﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormAPI.Models.RepositoryModel;
using RRPlatFormAPI.Models.RepositoryModel.Master;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace RRPlatFormAPI.BussinessLogic
{
    public class NotifyBL : AbBLRepository
    {
        public UserAccess userAccess;

        public async Task<ActionResultStatus> GetNotifyList(GetNotifyListRequest request)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            TrNotifyMsg trNotifyMsg = new TrNotifyMsg();
            TrNotifyUserRead trNotifyUserRead = new TrNotifyUserRead();
            List<GetNotifyListResponse> responseList = new List<GetNotifyListResponse>();
            try
            {
                trNotifyMsg.Datas = trNotifyMsg.GetDatasByEmpNo(this.userAccess.empNo).OrderByDescending(r => r.CREATE_DATE).Distinct().ToList();
                foreach (var trNotifyMsgData in trNotifyMsg.Datas)
                {
                    GetNotifyListResponse responseData = new GetNotifyListResponse();
                    responseData.createDate = trNotifyMsgData.CREATE_DATE;
                    responseData.imgUrl = trNotifyMsgData.IMG_URL;
                    responseData.notifyDesc = trNotifyMsgData.NOTIFY_DESC;
                    responseData.notifyHeader = trNotifyMsgData.NOTIFY_HEADER;
                    responseData.notifyMsgCode = trNotifyMsgData.NOTIFY_MSG_CODE;
                    responseData.url = trNotifyMsgData.URL;
                    responseData.readStatus = trNotifyUserRead.GetStatusUserRead(this.userAccess.empNo, trNotifyMsgData.NOTIFY_MSG_CODE);

                    responseList.Add(responseData);
                }


                actionResultStatus.data = responseList;
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> GetReadNotifyList(GetReadNotifyListRequest request)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            TrNotifyUserRead trNotifyUserRead = new TrNotifyUserRead();
            List<string> responseList = new List<string>();
            try
            {
                responseList = trNotifyUserRead.GetNotifyCodeListByEmpNo(this.userAccess.empNo);

                actionResultStatus.data = responseList;
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> ReadNotify(ReadNotifyRequest request)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            TrNotifyUserRead trNotifyUserRead = new TrNotifyUserRead();
            try
            {
                trNotifyUserRead.Data = trNotifyUserRead.GetDataUserRead(this.userAccess.empNo, request.notifyMsgCode);
                if (trNotifyUserRead.Data == null)
                {
                    trNotifyUserRead.Data = new RRPlatFormModel.Models.TR_NOTIFY_READ();
                    trNotifyUserRead.Data.CREATE_BY = this.userAccess.fullName;
                    trNotifyUserRead.Data.CREATE_DATE = DateTime.Now;
                    trNotifyUserRead.Data.EMP_NO = this.userAccess.empNo;
                    trNotifyUserRead.Data.IS_ACTIVE = StatusYesNo.Yes.Value();
                    trNotifyUserRead.Data.NOTIFY_MSG_CODE = request.notifyMsgCode;

                    trNotifyUserRead.Create(trNotifyUserRead.Data);
                }

                actionResultStatus.data = trNotifyUserRead.Data;
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> ReadNotifyList(ReadNotifyListRequest request)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            TrNotifyUserRead trNotifyUserRead = new TrNotifyUserRead();
            try
            {
                foreach (var item in request.notifyMsgCodeList)
                {
                    trNotifyUserRead.Data = trNotifyUserRead.GetDataUserRead(this.userAccess.empNo, item.notifyMsgCode);
                    if (trNotifyUserRead.Data == null)
                    {
                        trNotifyUserRead.Data = new RRPlatFormModel.Models.TR_NOTIFY_READ();
                        trNotifyUserRead.Data.CREATE_BY = this.userAccess.fullName;
                        trNotifyUserRead.Data.CREATE_DATE = DateTime.Now;
                        trNotifyUserRead.Data.EMP_NO = this.userAccess.empNo;
                        trNotifyUserRead.Data.IS_ACTIVE = StatusYesNo.Yes.Value();
                        trNotifyUserRead.Data.NOTIFY_MSG_CODE = item.notifyMsgCode;

                        trNotifyUserRead.Create(trNotifyUserRead.Data);
                    }

                }
                actionResultStatus.data = request;
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public bool SaveNotifyMsgForJob(string NotiTopicCode, string jobNo,string jobName, string jobDueDate, List<string> deptCode)
        {
            bool success = true;

            MtNotifyConfig mstNotiConfig = new MtNotifyConfig();
            MtNotifyGroup mstNotiGroup = new MtNotifyGroup();
            TrNotifyMsg trNotiMsg = new TrNotifyMsg();

            var noitConfig = mstNotiConfig.GetDataByCode(NotiTopicCode);
            if (noitConfig != null)
            {
                //1. User For Alert
                //MtOrgPosition mtOrgPosition = new MtOrgPosition();
                MtEmployeePosition mtEmployeePosition = new MtEmployeePosition();
        
                var empPositionList = new List<MT_EMPLOYEE_POSITION>();
                var notifyMsg = new SaveNotiMsgRequest();

                foreach (var itemOrg in deptCode)
                {
                    empPositionList = mtEmployeePosition.GetDatasByDeptCode(itemOrg, "DH");
                    //dataEmpList = dataEmpList.Concat(empItemList);
                    foreach (var itemEmp in empPositionList)
                    {
                        notifyMsg = new SaveNotiMsgRequest()
                        {
                            notifyMsgId = 0,
                            createBy = userAccess.userName,
                            empNo = itemEmp.EMP_NO,
                            notifyDesc = "Due date: " + jobDueDate + " : " + jobName,
                            refKey = "JOB_NO",
                            refTable =  nameof(TR_JOB),
                            refValue = jobNo,
                            notifyHeader = noitConfig.NOTIFY_CONFIG_HEADER_TEMPLATE + " " + jobNo,
                            isActive = "Y",
                            notifyMsgCode = trNotiMsg.getNotifyMsgRunning($"{noitConfig.NOTIFY_CONFIG_CODE}{DateTime.Now.ToString("yy")}{DateTime.Now.ToString("MM")}{DateTime.Now.ToString("dd")}", 3),
                            notifyTopicCode = noitConfig.NOTIFY_TOPIC_CODE,
                            url = $"{noitConfig.URL}{jobNo}",
                            notifyGroupCode = "NOG001"
                        };
                        var notiGsg = trNotiMsg.Save(notifyMsg, out success);
                    }
                }

                //2. Loop Save Noti
                
                
            }
            return success;
        }

        public bool SaveNotifyMsg(SaveNotiMsgRequest model)
        {
            TrNotifyMsg trNotiMsg = new TrNotifyMsg();
            bool processSuccess;
            var notiGsg = trNotiMsg.Save(model, out processSuccess);
            return processSuccess;
        }
    }
}