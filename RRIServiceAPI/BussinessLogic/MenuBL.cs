﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormAPI.Models.RepositoryModel;
using RRPlatFormModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RRPlatFormAPI.BussinessLogic
{
 
    public class MenuMudole : AbBLRepository
    {
        public static List<int> getProgramUser(int userId)
        {
            using (var db = new RRIServiceModelContext())
            {
                var getdata = db.MT_USER_MENU_PERMISSION.Where(r => r.USER_ID == userId && r.IS_ACTIVE.Equals("Y")).Select(r => r.MENU_ID).ToList();
                return getdata;
            }
        }

        public static List<int> getProgramGroup(int grpId)
        {
            using (var db = new RRIServiceModelContext())
            {
                var getdata = db.MT_MENU_GROUP_PERMISSION.Where(r => r.GROUP_ID == grpId && r.IS_ACTIVE.Equals("Y")).Select(r => r.MENU_ID).ToList();
                return getdata;
            }
        }

        public static List<int> getProgramType(int typeId)
        {
            using (var db = new RRIServiceModelContext())
            {
                var getdata = db.MT_MENU_TYPE_PERMISSION.Where(r => r.TYPE_ID == typeId && r.IS_ACTIVE.Equals("Y")).Select(r => r.MENU_ID).ToList();
                return getdata;
            }
        }

        public static List<int> getProgramRole(int roleId)
        {
            using (var db = new RRIServiceModelContext())
            {
                var getdata = db.MT_MENU_ROLE_PERMISSION.Where(r => r.ROLE_ID == roleId && r.IS_ACTIVE.Equals("Y")).Select(r => r.MENU_ID).ToList();
                return getdata;
            }
        }

        public static List<MenuItemResponse> getProgramPermission(List<int> listMenu)
        {
            ActionResultStatus result = new ActionResultStatus();
            using (var db = new RRIServiceModelContext())
            {
                var listMainMenu = db.MT_MENU.Where(a => a.MENU_TYPE_ID == 1 && listMenu.Contains(a.MENU_ID) && a.IS_ACTIVE.Equals("Y")).Select(r => new MenuItemResponse
                {
                    id = r.MENU_ID,
                    parentId = (int)r.PARENT_ID,
                    name = r.NAME,
                    icon = r.ICON,
                    path = r.URL,
                    sort = r.SORT,
                    isUse = r.IS_ACTIVE
                }).OrderBy(r => r.sort).ToList();

                List<MenuItemResponse> getMenuRecurs = new List<MenuItemResponse>();
                 getMenuRecurs = listMainMenu
                            .Where(c => c.parentId == 0)
                            .Select(c => new MenuItemResponse() 
                            {
                                id = c.id,
                                name = c.name,
                                icon = c.icon,
                                path = c.path,
                                sort = c.sort,
                                isUse = c.isUse,
                                children = GetChildren(listMainMenu, c.id)
                            }).ToList();

                return getMenuRecurs;
            }
        }
        public static List<MenuItemResponse> GetChildren(List<MenuItemResponse> comments, int parentId)
        {
            return comments
                    .Where(c => c.parentId == parentId && c.isUse == "Y")
                    .Select(c => new MenuItemResponse
                    {
                        id = c.id,
                        parentId = c.parentId,
                        name = c.name,
                        icon = c.icon,
                        path = c.path,
                        sort = c.sort,
                        isUse = c.isUse,
                        children = GetChildren(comments, c.id)
                    })
                    .ToList();
        }
        public static List<Children> GetChildren( int parentId)
        {
            using (var db = new RRIServiceModelContext())
            {
                var children = db.MT_MENU.Where(r => (int)r.PARENT_ID == parentId && r.IS_ACTIVE.Equals("Y")).Select(r => new Children
                {
                    id = r.MENU_ID,
                    name = r.NAME,
                    icon = r.ICON,
                    path = r.URL,
                    sort = r.SORT,
                    isUse = r.IS_ACTIVE,
                }).OrderBy(r => r.sort).ToList();

                return children;

            }
          

        }

        public static ActionResultStatus GetPromotionActionlist(PermittionListRequestInput input)
        {
            ActionResultStatus actionResult = new ActionResultStatus();
            int userId = 0;
            using (var db = new RRIServiceModelContext())
            {
                var permission = (from m in db.MT_MENU
                                  join um in db.MT_USER_MENU_PERMISSION on m.MENU_ID equals um.MENU_ID into um
                                  from _um in um.DefaultIfEmpty()
                                  where m.MENU_TYPE_ID == input.permiition_type && _um.USER_ID == userId
                                  select new outputPermission()
                                  {
                                      perId = m.MENU_ID.ToString(),
                                      perName = m.NAME,
                                      flg = (_um.IS_ACTIVE == "Y" ? "1" : "0")

                                  }).ToList();
                actionResult.data = permission;
                return actionResult;
            }
        }

        public static ActionResultStatus SetPermissionMenu(SetPermissionMenuRequest input)
        {
            ActionResultStatus result = new ActionResultStatus();

            bool userTypeSet = !input.usertypeId.IsNullOrEmptyWhiteSpace() && input.userid.IsNullOrEmptyWhiteSpace() ? true : false;
            bool userIdSet = !input.usertypeId.IsNullOrEmptyWhiteSpace() && !input.userid.IsNullOrEmptyWhiteSpace() ? true : false;
            string userName = "";

            using (var db = new RRIServiceModelContext())
            {
                if (userTypeSet)
                {
                    foreach (var menuData in input.menuList)
                    {
                        

                        MtMenuTypePermission mtMenuTypePermission = new MtMenuTypePermission();
                        mtMenuTypePermission.SaveById(input.usertypeId.ToInt32(), menuData.menuId, menuData.active, userName);
                    }
                }
                if (userIdSet)
                {
                    foreach (var menuData in input.menuList)
                    {
                        MtUserMenuPermission mtUserMenuPermission = new MtUserMenuPermission();
                        mtUserMenuPermission.SaveById(input.userid.ToInt32(), menuData.menuId, menuData.active, userName);
                    }
                }


                return result;
            }
        }

        public static ActionResultStatus GetMenu(UserAccess model)
        {
            ActionResultStatus result = new ActionResultStatus();

            try
            {
                var listMenu = new List<int>();
                var listMenuGroup = new List<int>();
                var listMenuType = new List<int>();
                var listMenuUser = new List<int>();

                listMenuGroup = MenuMudole.getProgramGroup(model.userGroupId.ToInt32());
                listMenuType = MenuMudole.getProgramType(model.userTypeId.ToInt32());
                listMenuUser = MenuMudole.getProgramUser(model.userId.ToInt32());
                listMenu = listMenuGroup.Union(listMenuUser).Union(listMenuType).ToList();
                result.data = MenuMudole.getProgramPermission(listMenu);
            }
            catch (Exception ex)
            {
                result.code = (int)StatusCode.Exception;
                result.message = StatusCode.Exception.ToString();
                result.description = ex.ErrorException();
                result.success = false;
            }

            return result;
            
        }


        public static ActionResultStatus GetMenuAll(GetMenuAllRequest input)
        {
            ActionResultStatus result = new ActionResultStatus();
            using (var db = new RRIServiceModelContext())
            {

                var listMainMenu = db.MT_MENU.Where(a => a.MENU_TYPE_ID == 1 && a.IS_ACTIVE == "Y").Select(r => new MenuItemResponse
                {
                    id = r.MENU_ID,
                    parentId = (int)r.PARENT_ID,
                    name = r.NAME,
                    icon = r.ICON,
                    path = r.URL,
                    sort = r.SORT,
                    isUse = r.IS_ACTIVE
                }).OrderBy(r => r.sort).ToList();

                List<MenuItemResponse> getMenuRecurs = new List<MenuItemResponse>();
                getMenuRecurs = listMainMenu
                           .Where(c => c.parentId == 0)
                           .Select(c => new MenuItemResponse()
                           {
                               id = c.id,
                               name = c.name,
                               icon = c.icon,
                               path = c.path,
                               sort = c.sort,
                               isUse = c.isUse,
                               children = GetChildren(listMainMenu, c.id)
                           }).ToList();

                GetMenuAllResponse getMenuAllResponse = new GetMenuAllResponse();
                getMenuAllResponse.menuList = getMenuRecurs;

                int userTypeId = 0;
                int userGroupId = 0;
                int userId = 0;

                if (!input.usertypeId.IsNullOrEmptyWhiteSpace())
                {
                    MtUserType mtUserType = new MtUserType();
                    mtUserType.Data = mtUserType.GetData(input.usertypeId);

                    if (mtUserType.Data != null)
                    {
                        UserTypeProfile userTypeProfile = new UserTypeProfile();
                        userTypeProfile.userTypeId = mtUserType.Data.USER_TYPE_ID.ToString();
                        userTypeProfile.userTypeName = mtUserType.Data.USER_TYPE_NAME.ToString();
                        userTypeProfile.isActive = mtUserType.Data.IS_ACTIVE;
                        getMenuAllResponse.userTypeProfile = userTypeProfile;

                        getMenuAllResponse.userTypeMenuList = (from mm in db.MT_MENU
                                                               join mpg in db.MT_MENU_TYPE_PERMISSION on mm.MENU_ID equals mpg.MENU_ID
                                                               where mm.MENU_TYPE_ID == 1
                                                               && mm.IS_ACTIVE == "Y"
                                                               && mpg.IS_ACTIVE == "Y"
                                                               && mpg.TYPE_ID == userTypeId
                                                               select new MenuDataShort()
                                                               {
                                                                   menuId = mm.MENU_ID.ToString(),
                                                                   name = mm.NAME,
                                                                   isUse = mm.IS_ACTIVE,
                                                                   parrentId = mm.PARENT_ID,
                                                                   countChildren = (from mmm in db.MT_MENU
                                                                                    where mmm.MENU_TYPE_ID == 1
                                                                                      && mmm.IS_ACTIVE == "Y"
                                                                                      && mmm.PARENT_ID == mm.MENU_ID
                                                                                    select mmm).Count()
                                                               }).ToList();
                    }

                }
                if (!input.userid.IsNullOrEmptyWhiteSpace())
                {
                    getMenuAllResponse.userMenuList = (from mm in db.MT_MENU
                                                       join um in db.MT_USER_MENU_PERMISSION on mm.MENU_ID equals um.MENU_ID
                                                       where mm.MENU_TYPE_ID == 1
                                                         && mm.IS_ACTIVE == "Y"
                                                         && um.IS_ACTIVE == "Y"
                                                         && um.USER_ID == userId
                                                       select new MenuDataShort()
                                                       {
                                                           menuId = mm.MENU_ID.ToString(),
                                                           name = mm.NAME,
                                                           isUse = mm.IS_ACTIVE,
                                                           parrentId = mm.PARENT_ID,
                                                           countChildren = (from mmm in db.MT_MENU
                                                                            where mmm.MENU_TYPE_ID == 1
                                                                              && mmm.IS_ACTIVE == "Y"
                                                                              && mmm.PARENT_ID == mm.MENU_ID
                                                                            select mmm).Count()
                                                       }).ToList();
                }
                result.data = getMenuAllResponse;

                return result;
            }
        }

        public static ActionResultStatus GetMenuButtomAll(GetMenuAllRequest input)
        {
            ActionResultStatus result = new ActionResultStatus();
            int userTypeId = 0;
            int userGroupId = 0;
            int userId = 0;
            GetMenuButtomAllResponse getMenuAllResponse = new GetMenuButtomAllResponse();


            using (var db = new RRIServiceModelContext())
            {

                var getMenuRecurs = (from groupMenu in db.MT_MENU_GROUP
                                     where groupMenu.IS_ACTIVE == "Y"
                                     select new MenuGroupButtonResponse()
                                     {
                                         menuId =  "000" + groupMenu.ID.ToString(),
                                         name = groupMenu.NAME,
                                         isUse = groupMenu.IS_ACTIVE,
                                         children = db.MT_MENU.Where(menu => menu.MENU_GROUP_ID == groupMenu.ID && menu.MENU_TYPE_ID == 2
                                                                        && menu.IS_ACTIVE == "Y")
                                                                    .Select(menu => new MenuButtonResponse()
                                                                    {
                                                                        menuId = menu.MENU_ID.ToString(),
                                                                        name = menu.NAME,
                                                                        menuGroupId = menu.MENU_GROUP_ID.ToString(),
                                                                        url = menu.URL,
                                                                        isUse = menu.IS_ACTIVE

                                                                    }).ToList()
                                     }
                                 ).ToList();


                getMenuAllResponse.menuButtonList = getMenuRecurs;


                if (!input.usertypeId.IsNullOrEmptyWhiteSpace())
                {
                    MtUserType mtUserType = new MtUserType();
                    mtUserType.Data = mtUserType.GetData(input.usertypeId);

                    if (mtUserType.Data != null)
                    {
                        UserTypeProfile userTypeProfile = new UserTypeProfile();
                        userTypeProfile.userTypeId = mtUserType.Data.USER_TYPE_ID.ToString();
                        userTypeProfile.userTypeName = mtUserType.Data.USER_TYPE_NAME.ToString();
                        userTypeProfile.isActive = mtUserType.Data.IS_ACTIVE ;

                        getMenuAllResponse.userTypeProfile = userTypeProfile;

                        var buttonTypeList = (from menu in db.MT_MENU
                                              join um in db.MT_MENU_TYPE_PERMISSION on menu.MENU_ID equals um.MENU_ID
                                              where menu.MENU_TYPE_ID == 2
                                               && menu.IS_ACTIVE == "Y"
                                               && um.IS_ACTIVE == "Y"
                                               && um.TYPE_ID == userTypeId
                                              select new MenuButtonResponse()
                                              {
                                                  menuId = menu.MENU_ID.ToString(),
                                                  name = menu.NAME,
                                                  menuGroupId = "000" + menu.MENU_GROUP_ID.ToString(),
                                                  url = menu.URL,
                                                  isUse = menu.IS_ACTIVE

                                              }).ToList();

                        getMenuAllResponse.userTypeMenuButtonList = buttonTypeList;
                    }
                }

                if (!input.userid.IsNullOrEmptyWhiteSpace())
                {
                  var  menuByUserButton = (from menu in db.MT_MENU
                                  join um in db.MT_USER_MENU_PERMISSION on menu.MENU_ID equals um.MENU_ID
                                  where menu.MENU_TYPE_ID == 2
                                   && menu.IS_ACTIVE == "Y"
                                   && um.IS_ACTIVE == "Y"
                                   && um.USER_ID == userId
                                  select new MenuButtonResponse()
                                  {
                                      menuId = menu.MENU_ID.ToString(),
                                      name = menu.NAME,
                                      menuGroupId = "000" + menu.MENU_GROUP_ID.ToString(),
                                      url = menu.URL,
                                      isUse = menu.IS_ACTIVE

                                  }).ToList();

                    getMenuAllResponse.userMenuButtonList = menuByUserButton;
                }

                result.data = getMenuAllResponse;

                return result;
            }
        }

        

        public static ActionResultStatus GetPromotionButtonPerPage(PermittionBottonPerPageRequestInput input)
        {
            ActionResultStatus result = new ActionResultStatus();
            using (var db = new RRIServiceModelContext())
            {
                int userGroupId = 0;
                int userTypeId = 0;
                int userId = 0;

                List<MenuDataListResponse> resultList = new List<MenuDataListResponse>();

                var menuByGroup = (from mm in db.MT_MENU
                                      join mpg in db.MT_MENU_GROUP_PERMISSION on mm.MENU_ID equals mpg.MENU_ID into um
                                      from mpg in um.DefaultIfEmpty()
                                      where mm.MENU_TYPE_ID == 2
                                      && mpg.GROUP_ID == userGroupId
                                      && mm.IS_ACTIVE == "Y"
                                      && mpg.IS_ACTIVE == "Y"
                                      && mm.URL == input.url
                                      select new MenuDataListResponse()
                                      {
                                          button_id = mm.MENU_ID.ToString(),
                                          button_name = mm.NAME,
                                          url = mm.URL

                                      }).ToList();


                var menuByType = (from mm in db.MT_MENU
                                   join mpg in db.MT_MENU_TYPE_PERMISSION on mm.MENU_ID equals mpg.MENU_ID into um
                                   from mpg in um.DefaultIfEmpty()
                                   where mm.MENU_TYPE_ID == 2
                                   && mpg.TYPE_ID == userTypeId
                                   && mm.IS_ACTIVE == "Y"
                                   && mpg.IS_ACTIVE == "Y"
                                   && mm.URL == input.url
                                   select new MenuDataListResponse()
                                   {
                                       button_id = mm.MENU_ID.ToString(),
                                       button_name = mm.NAME,
                                       url = mm.URL

                                   }).ToList();

                var menuByUser = (from mm in db.MT_MENU
                                  join um in db.MT_USER_MENU_PERMISSION on mm.MENU_ID equals um.MENU_ID into um
                                  from _um in um.DefaultIfEmpty()
                                  where mm.MENU_TYPE_ID == 2
                                      && _um.USER_ID == userId
                                      && mm.IS_ACTIVE == "Y"
                                      && _um.IS_ACTIVE == "Y"
                                      && mm.URL == input.url
                                  select new MenuDataListResponse()
                                  {
                                      button_id = mm.MENU_ID.ToString(),
                                      button_name = mm.NAME,
                                      url = mm.URL

                                  }).ToList();

                resultList = menuByGroup.Union(menuByUser).Union(menuByType).ToLookup(p => p.button_id).Select(g => g.Aggregate((p1, p2) => new MenuDataListResponse
                {
                    button_id = p1.button_id,
                    button_name = p1.button_name,
                    url = p1.url
                })).ToList();

                result.data = resultList;

                return result;
            }
        }

    }


}