﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormAPI.Models.RepositoryModel;
using RRPlatFormAPI.Utility;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;

namespace RRPlatFormAPI.BussinessLogic
{
    public class UploadBL : AbBLRepository
    {
        public UserAccess userAccess;
        public ActionResultStatus UploadProfileImage(HttpFileCollection files)
        {
            ActionResultStatus responseData = new ActionResultStatus();
            List<UploadResponse> resultList = new List<UploadResponse>();
            try
            {
                if (files.Count < 1)
                {
                    responseData.success = false;
                    responseData.code = (int)StatusCode.BlankFile;
                    responseData.message = StatusCode.BlankFile.Value();
                    responseData.description = StatusCode.BlankFile.Value();
                }
                else
                {
                    string dateTimeFolder = DateTime.Now.ToString("yyyy");
                    string dateTimePrefix = DateTime.Now.ToString("yyyyMMddHHmmss");
                    string dirNamePath = HttpContext.Current.Server.MapPath("~" + Commons.physicalServerPathFile + dateTimeFolder);

                    if (!Directory.Exists(dirNamePath))
                    {
                        System.IO.Directory.CreateDirectory(dirNamePath + dateTimeFolder);
                    }

                    foreach (string file in files)
                    {
                        UploadResponse resultData = new UploadResponse();

                        var postedFile = files[file];
                        var typeFileSplit = postedFile.FileName.Split('.');
                        string typeFile = "." + typeFileSplit[1];
                        string fileName = dateTimePrefix;
                        var filePathSave = HttpContext.Current.Server.MapPath("~" + Commons.physicalServerPathFile + dateTimeFolder + fileName + typeFile);
                        postedFile.SaveAs(filePathSave);

                        resultData.fileName = dateTimePrefix;
                        resultData.oldFileName = postedFile.FileName;
                        resultData.filePath = Commons.physicalServerPrefixRoute + Commons.physicalServerPathFile + dateTimeFolder + fileName + typeFile;
                        resultData.fileType = typeFileSplit[1];

                        resultList.Add(resultData);
                    }

                    responseData.data = resultList;
                }
            }
            catch (Exception ex)
            {
                responseData.success = false;
                responseData.code = (int)StatusCode.NotSave;
                responseData.data = null;
                responseData.message = ex.Message;
                responseData.description = ex.ErrorException();
            }

            return responseData;

        }

        public static void CompressImage(string SoucePath, string DestPath, int quality)
        {
            var FileName = Path.GetFileName(SoucePath);

            using (Bitmap bmp1 = new Bitmap(SoucePath))
            {
                ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);

                System.Drawing.Imaging.Encoder QualityEncoder = System.Drawing.Imaging.Encoder.Quality;

                EncoderParameters myEncoderParameters = new EncoderParameters(1);

                EncoderParameter myEncoderParameter = new EncoderParameter(QualityEncoder, quality);
                myEncoderParameters.Param[0] = myEncoderParameter;
                bmp1.Save(DestPath, jpgEncoder, myEncoderParameters);

            }

        }

        public static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }
    }

    public class UploadResponse
    {
        public string fileName { get; set; }
        public string oldFileName { get; set; }
        public string fileType { get; set; }
        public string filePath { get; set; }
    }
}