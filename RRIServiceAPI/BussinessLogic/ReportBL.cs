﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormAPI.Models.RepositoryModel;
using RRPlatFormAPI.Models.RepositoryModel.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace RRPlatFormAPI.BussinessLogic
{
    public class ReportBL : AbBLRepository
    {
        public UserAccess userAccess;

        public async Task<ActionResultStatus> ReportSummaryWorktime(ReportSummaryWorktimeRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtEmployee mtEmployee = new MtEmployee();
            MtEmployeePosition mtEmployeePosition = new MtEmployeePosition();
            MtDepartment mtDept = new MtDepartment();
            TrTimeSheet trTimeSheet = new TrTimeSheet();

            try
            {
                mtEmployeePosition.Datas = mtEmployeePosition.GetAll().Where(r => r.IS_ACTIVE == "Y").ToList();
                mtEmployee.Datas = mtEmployee.GetAll().Where(r => r.IS_ACTIVE == "Y").ToList();
                mtDept.Datas = mtDept.GetAll().Where(r => r.IS_ACTIVE == "Y").ToList();
                var dataEmpPosition = (from a in mtEmployeePosition.Datas
                                       group a by new { a.EMP_NO, a.DEPT_CODE } into g
                                       select new ReportSummaryWorktimeTemp
                                       {
                                           deptCode = g.Key.DEPT_CODE,
                                           empNo = g.Key.EMP_NO
                                       }).ToList();
                var dataEmp = (from a in dataEmpPosition
                               join b in mtEmployee.Datas on a.empNo equals b.EMP_NO
                               join c in mtDept.Datas on a.deptCode equals c.DEPT_CODE
                               select new ReportSummaryWorktimeTemp
                               {
                                   deptCode = a.deptCode,
                                   deptName = c.DEPT_NAME,
                                   empName = b.FIRST_NAME,
                                   empNo = a.empNo
                               }).ToList();

                var dataTimesheetList = trTimeSheet.GetDatasByWorkTime(model.startDate, model.endDate);
                //var aa = dataTimesheetList.Where(r => r.empNo == "1800008").ToList();
                //var sss = aa.Sum(r => r.workTime);

                //var datatotalTime = (from a in dataTimesheetList
                //                     group a by a.empNo into g
                //                     select new ReportSummaryWorkTimeModel { empNo = g.Key, totalTime = g.Sum(r => r.workTime) }).ToList();



                if (!model.empName.IsNullOrEmptyWhiteSpace())
                {
                    dataEmp = dataEmp.Where(r => r.empName.Contains(model.empName)).ToList();
                }
                if (model.deptCodeList.Count() > 0)
                {
                    dataEmp = dataEmp.Where(r => model.deptCodeList.Contains(r.deptCode)).ToList();
                }

                var result = (from a in dataEmp
                              //join b in datatotalTime on a.empNo equals b?.empNo into bb
                              //from b in bb.DefaultIfEmpty()
                              join c in dataTimesheetList on a.empNo equals c?.empNo into cc
                              from c in cc.DefaultIfEmpty()
                              select new ReportSummaryWorktimeTemp
                              {
                                  Date = c?.Date,
                                  empNo = a.empNo,
                                  //totalTime = SummaryHour(b?.totalTime ?? 0),
                                  workTime = SummaryHour(c?.workTime ?? 0),
                                  deptCode = a.deptCode,
                                  deptName = a.deptName,
                                  empName = a.empName
                              }).OrderBy(r => r.deptName).ThenBy(r => r.empName).ToList();


                var response = (from a in result
                                group a by new
                                {
                                    a.empNo,
                                    a.empName,
                                    a.deptName,
                                    a.deptCode
                                } into g
                                select new ReportSummaryWorkTimeResponse
                                {
                                    worktimeList = result.Where(r => r.empNo == g.Key.empNo && r.Date != null).ToList(),
                                    deptCode = g.Key.deptCode,
                                    deptName = g.Key.deptName,
                                    empName = g.Key.empName,
                                    empNo = g.Key.empNo,
                                    totalTime = g.Sum(r => r.workTime)
                                }).ToList();
                actionResultStatus.data = response;
            }
            catch (Exception ex)
            {
                actionResultStatus.success = false;
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public decimal SummaryHour(decimal minut)
        {
            //var totalMinutes = 125;
            //Console.WriteLine("{0}:{1:00}", totalMinutes / 60, totalMinutes % 60);
            var time = TimeSpan.FromMinutes((double)minut);
            var strHours = string.Format("{0}:{1:00}", (int)time.TotalHours, time.Minutes);
            strHours = strHours.Replace(":", ".");
            return strHours.ToDecimal();
        }
    }
}