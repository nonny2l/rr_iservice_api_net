﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormAPI.Models.RepositoryModel;
using System;
using System.Collections.Generic;

namespace RRPlatFormAPI.BussinessLogic
{
    public class UserBL : AbBLRepository
    {
        public UserAccess userAccess;

        public ActionResultStatus Login(LoginRequest request)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            MtUser mtUser = new MtUser();
            try
            {
                mReturn = mtUser.CheckLogIn(request.userName, request.passWord, request.userGroupId);
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }

        public ActionResultStatus LogOut(UserAccess request)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            MtUser mtUser = new MtUser();
            try
            {
                mReturn = mtUser.CheckLogOut(request.userId);
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }


        public ActionResultStatus GetUserList(GetUserListRequest request)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                List<GetMtUserListResponse> response = new List<GetMtUserListResponse>();

            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }

        public ActionResultStatus GetUserProfile(UserAccess request)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            try
            {
                MtUser mtUser = new MtUser();
                MtUserGroup mtUserGroup = new MtUserGroup();
                MtUserType mtUserType = new MtUserType();
                if (request != null)
                {
                    var data = mtUser.GetUserProfile(request.empNo);
                    data.tokenDateTime = request.tokenDateTime;
                    actionResultStatus.data = data;
                }
                else
                {
                    actionResultStatus.success = false;
                    actionResultStatus.code = (int)StatusCode.NoPermission;
                    actionResultStatus.message = StatusCode.NoPermission.Value();
                    actionResultStatus.description = "ไม่มีผู้ใช้งานนี้ในระบบ";
                }


            }
            catch (Exception ex)
            {
                actionResultStatus.success = false;
                actionResultStatus.message = ex.ErrorException();
                actionResultStatus.code = (int)StatusCode.NotSave;
            }

            return actionResultStatus;
        }

        public ActionResultStatus SavePassword(SavePasswordRequest request)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            try
            {
                MtUser mtUser = new MtUser();
                bool resultStatus = false;

                var data = mtUser.SavePassword(request, userAccess, out resultStatus);

                if (!resultStatus)
                {
                    actionResultStatus.success = resultStatus;
                    actionResultStatus.message = "รหัสผ่านเดิมไม่ถูกต้อง";
                }
            }
            catch (Exception ex)
            {
                actionResultStatus.success = false;
                actionResultStatus.message = ex.ErrorException();
                actionResultStatus.code = (int)StatusCode.NotSave;
            }

            return actionResultStatus;
        }
    }
}