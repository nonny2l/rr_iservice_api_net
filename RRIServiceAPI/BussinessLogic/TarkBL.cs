﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormAPI.Models.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using RRPlatFormAPI.HTTP.Request.Master;
using RRPlatFormAPI.Models.RepositoryModel.Master;
using RRPlatFormAPI.HTTP.Response.Master;
using System.Threading.Tasks;

namespace RRPlatFormAPI.BussinessLogic
{
    public class TarkBL : AbBLRepository
    {
        public UserAccess userAccess;

        public async Task<ActionResultStatus> GetTeamList(GetTeamListRequest request)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            List<GetTeamListResponse> responseList = new List<GetTeamListResponse>();
            TrJobTask trJobTask = new TrJobTask();
            TrTeam trTeam = new TrTeam();
            TrTeamRole teamRole = new TrTeamRole();
            List<string> jobTaskCodeList = new List<string>();
            try
            {
                jobTaskCodeList = trJobTask.GetTeamList(request);
                //trTeam.Datas = trTeam.FindBy(a => jobTaskCodeList.Contains(a.JOB_TARK_CODE)).ToList();

                //foreach (var trTeamData in trTeam.Datas)
                //{
                //    GetTeamListResponse response = new GetTeamListResponse();
                //    response.teamCode = trTeamData.TEAM_CODE;
                //    response.teamName = trTeamData.TEAM_NAME;
                //    response.jobTarkCode = trTeamData.JOB_TARK_CODE;

                //    teamRole.Datas = teamRole.GetDatas(trTeamData.TEAM_CODE);

                //    response.teamRoleList = new List<TeamRoleList>();
                //    foreach (var teamRoleData in teamRole.Datas)
                //    {
                //        TeamRoleList teamRoleDatas = new TeamRoleList();
                //        teamRoleDatas.empNo = teamRoleData.EMP_NO;
                //        teamRoleDatas.isActive = teamRoleData.IS_ACTIVE;
                //        teamRoleDatas.isMain = teamRoleData.IS_MAIN;

                //        response.teamRoleList.Add(teamRoleDatas);
                //    }

                //    responseList.Add(response);
                //}

                actionResultStatus.data = responseList;
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> AcceptJobTask(AcceptJobTaskRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtProjectD mtProjectD = new MtProjectD();
            TrJobTask trJobTask = new TrJobTask();

            try
            {
                trJobTask.Data = trJobTask.AcceptJobTask(model);
                if (trJobTask.Data != null)
                {

                    trJobTask.Data.ACCEPT_BY = this.userAccess.fullName;
                    trJobTask.Data.ACCEPT_DATE = DateTime.Now;
                    trJobTask.Data.ACCEPT_STATUS = model.acceptStatus;
                    trJobTask.Data.PLAN_END_DATE = model.planEndDate;
                    trJobTask.Data.PLAN_START_DATE = model.planStartDate;
                    trJobTask.Data.DUE_DATE = model.dueDate;

                    trJobTask.Edit("", trJobTask.Data);

                    if (model.team != null)
                    {
                        bool teamStatus = false;
                        TrTeam trTeam = new TrTeam();
                        model.team.jobNo = trJobTask.Data.JOB_NO;
                        model.team.deptCode = trJobTask.Data.DEPT_CODE;
                        model.team.jobTaskCode = trJobTask.Data.JOB_TASK_CODE;

                        var trTeamData = trTeam.Save(model.team, this.userAccess, out teamStatus);

                        foreach (var teamRoleData in model.team.teamRoleList)
                        {
                            TrTeamRole teamRole = new TrTeamRole();
                            teamRoleData.teamCode = trTeamData.TEAM_CODE;
                            teamRole.Save(teamRoleData, this.userAccess, out teamStatus);
                        }
                    }
                }

                actionResultStatus.data = model;
            }
            catch (Exception ex)
            {
                actionResultStatus.success = false;
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

    }
}