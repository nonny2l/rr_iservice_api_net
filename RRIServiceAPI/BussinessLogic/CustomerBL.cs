﻿using DocumentFormat.OpenXml.Drawing.ChartDrawing;
using Newtonsoft.Json;
using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Request.Customer;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormAPI.Models.RepositoryModel;
using RRPlatFormAPI.Models.RepositoryModel.Customer;
using RRPlatFormAPI.Models.RepositoryModel.Master;
using RRPlatFormAPI.Utility;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace RRPlatFormAPI.BussinessLogic
{
    public class CustomerBL : AbBLRepository
    {
        public UserAccess userAccess;
        public async Task<ActionResultStatus> GetClientList(GetClientListRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            List<GetClientListReponse> clientListReponse = new List<GetClientListReponse>();
            MtCompany mtCompany = new MtCompany();

            try
            {

                mtCompany.Datas = mtCompany.GetAll().ToList();
                foreach (var mtCompanyData in mtCompany.Datas)
                {
                    GetClientListReponse clientReponse = new GetClientListReponse();
                    clientReponse.compCode = mtCompanyData.COMP_CODE;
                    clientReponse.compName = mtCompanyData.COMP_NAME;
                    clientReponse.businessTypeId = mtCompanyData.BUSINESS_TYPE.ToInt32();
                    clientReponse.businessTypeName = "";
                    clientReponse.subBusinessTypeId = mtCompanyData.SUB_BUSINESS_TYPE.ToInt32();
                    clientReponse.subBusinessTypeName = "";
                    clientReponse.stratDate = mtCompanyData.START_DATE;
                    clientReponse.endDate = mtCompanyData.END_DATE;
                    clientReponse.taxId = mtCompanyData.TAX_ID;
                    clientReponse.telNo = mtCompanyData.TEL_NO;
                    clientReponse.faxNo = mtCompanyData.FAX_NO;
                    clientReponse.termOfPayment = (int)mtCompanyData.TERM_OF_PAYMENT;
                    clientReponse.agencyFee = mtCompanyData.AGENCY_FEE != null ? (int)mtCompanyData.AGENCY_FEE : (int?)null;
                    clientReponse.remark = mtCompanyData.REMARK;
                    clientReponse.fackbookId = mtCompanyData.FACEBOOK_ID;
                    clientReponse.website = mtCompanyData.WEBSITE;
                    clientReponse.isCompany = mtCompanyData.IS_COMPANY;
                    clientReponse.isActive = mtCompanyData.IS_ACTIVE;

                    clientListReponse.Add(clientReponse);
                }

                actionResultStatus.data = clientListReponse.OrderBy(r => r.compName).ToList();
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }


        public async Task<ActionResultStatus> GetProjectList(GetProjectListRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtProjectH mtProjectH = new MtProjectH();
            List<GetProjectListResponse> responseList = new List<GetProjectListResponse>();
            try
            {
                mtProjectH.Datas = mtProjectH.GetProjectList(model);
                foreach (var mtProjectHData in mtProjectH.Datas)
                {
                    GetProjectListResponse dataResponse = new GetProjectListResponse();

                    dataResponse.projectHId = mtProjectHData.PROJECT_ID;
                    dataResponse.projectCode = mtProjectHData.PROJECT_CODE;
                    dataResponse.compCode = mtProjectHData.COMP_CODE;
                    dataResponse.projectName = mtProjectHData.PROJECT_NAME;
                    dataResponse.projectDesc = mtProjectHData.PROJECT_DESC;
                    dataResponse.budgetValue = mtProjectHData.BUDGET_VALUE;
                    dataResponse.budgetUnitTypeCode = mtProjectHData.BUDGET_UNIT_CODE;
                    dataResponse.budgetUnitTypeName = "";
                    dataResponse.isActive = mtProjectHData.IS_ACTIVE;

                    responseList.Add(dataResponse);
                }

                actionResultStatus.data = responseList.OrderBy(r=>r.projectName).ToList();
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> GetServiceList(GetServiceListRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtService mtService = new MtService();
            List<GetServiceListResponse> responseList = new List<GetServiceListResponse>();
            try
            {
                mtService.Datas = mtService.GetAll().ToList();
                foreach (var mtProjectHData in mtService.Datas)
                {
                    GetServiceListResponse dataResponse = new GetServiceListResponse();
                    dataResponse.srvcId = mtProjectHData.SERVICE_ID;
                    dataResponse.srvcCode = mtProjectHData.SERVICE_CODE;
                    dataResponse.srvcName = mtProjectHData.SERVICE_NAME;
                    dataResponse.isActive = mtProjectHData.IS_ACTIVE;
                    dataResponse.seq = mtProjectHData.SEQ;

                    responseList.Add(dataResponse);
                }

                actionResultStatus.data = responseList.OrderBy(a => a.seq).ToList();
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> GetSubServiceList(GetSubServiceListRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtService mtService = new MtService();
            MtSubService mtSubService = new MtSubService();
            List<GetSubServiceListResponse> responseList = new List<GetSubServiceListResponse>();
            try
            {

                mtSubService.Datas = mtSubService.GetSubServiceListFilter(model);
                foreach (var mtSubServiceData in mtSubService.Datas)
                {
                    GetSubServiceListResponse dataResponse = new GetSubServiceListResponse();
                    dataResponse.srvcCode = mtSubServiceData.SERVICE_CODE;
                    dataResponse.srvcName = mtService.GetDataByServiceCode(mtSubServiceData.SERVICE_CODE).SERVICE_NAME;
                    dataResponse.srvcSubId = mtSubServiceData.SUB_SERVICE_ID;
                    dataResponse.srvcSubCode = mtSubServiceData.SUB_SERVICE_CODE;
                    dataResponse.srvcSubName = mtSubServiceData.SUB_SERVICE_NAME;
                    dataResponse.isActive = mtSubServiceData.IS_ACTIVE;
                    responseList.Add(dataResponse);
                }

                actionResultStatus.data = responseList.OrderBy(r => r.srvcSubCode).ToList();
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> GetStatusJobList(GetStatusJobListRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtConfigD mtJobStatus = new MtConfigD();
            List<GetStatusJobListResponse> responseList = new List<GetStatusJobListResponse>();
            try
            {
                var dataMasterStatusList = mtJobStatus.GetDatasActiveByConfigHCode("JOB_STATUS");
                foreach (var mtStatusData in dataMasterStatusList)
                {
                    GetStatusJobListResponse dataResponse = new GetStatusJobListResponse();
                    dataResponse.statusId = (int)mtStatusData.CONFIG_D_ID;
                    dataResponse.statusCode = mtStatusData.CONFIG_D_CODE;
                    dataResponse.statusName = mtStatusData.CONFIG_D_TNAME;
                    dataResponse.seq = mtStatusData.CONFIG_D_SEQ;
                    dataResponse.isActive = mtStatusData.IS_ACTIVE;
                    responseList.Add(dataResponse);
                }

                actionResultStatus.data = responseList.OrderBy(a => a.seq).ToList();
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> GetEmployeeAllList(GetEmployeeAllListRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtEmployee mtEmployee = new MtEmployee();
            MtUser mtUser = new MtUser();
            List<GetEmployeeListResponse> responseList = new List<GetEmployeeListResponse>();
            try
            {
                mtEmployee.Datas = mtEmployee.GetAll().ToList();
                var empNoList = mtEmployee.Datas.Select(e => e.EMP_NO).ToList();
                var dataUer = mtUser.GetAll().ToList().Where(r => empNoList.Contains(r.EMP_NO)).ToList();

                responseList = (from a in mtEmployee.Datas
                                join b in dataUer on a.EMP_NO equals b.EMP_NO into bb
                                from b in bb.DefaultIfEmpty()
                                select new GetEmployeeListResponse
                                {
                                    empId = a.EMP_ID,
                                    empNo = a.EMP_NO,
                                    title = a.PRE_NAME_CODE,
                                    firstName = a.FIRST_NAME,
                                    lastName = a.LAST_NAME,
                                    startDate = a.START_DATE,
                                    endDate = a.END_DATE,
                                    isActive = a.IS_ACTIVE,
                                    avatar = b?.AVATAR,
                                    email = b?.EMAIL,
                                    userId = b?.USER_ID.ToString()
                                }).ToList();

                actionResultStatus.data = responseList.OrderBy(r => r.firstName).ToList();
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> GetEmployeeList(GetEmployeeListRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtEmployee mtEmployee = new MtEmployee();
            MtUser mtUser = new MtUser();
            List<GetEmployeeListResponse> responseList = new List<GetEmployeeListResponse>();
            try
            {
                mtEmployee.Datas = mtEmployee.GetDataByUserId(this.userAccess.empNo);
                var empNoList = mtEmployee.Datas.Select(e => e.EMP_NO).ToList();
                var dataUer = mtUser.GetAll().ToList().Where(r => empNoList.Contains(r.EMP_NO)).ToList();

                responseList = (from a in mtEmployee.Datas
                                join b in dataUer on a.EMP_NO equals b.EMP_NO into bb
                                from b in bb.DefaultIfEmpty()
                                select new GetEmployeeListResponse
                                {
                                    empId = a.EMP_ID,
                                    empNo = a.EMP_NO,
                                    title = a.PRE_NAME_CODE,
                                    firstName = a.FIRST_NAME,
                                    lastName = a.LAST_NAME,
                                    startDate = a.START_DATE,
                                    endDate = a.END_DATE,
                                    isActive = a.IS_ACTIVE,
                                    avatar = b.AVATAR,
                                    email = b.EMAIL,
                                    userId = b.USER_ID.ToString()
                                }).ToList();

                actionResultStatus.data = responseList.OrderBy(r => r.firstName).ToList();
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> GetEmployeeByTeamList(GetEmployeeByTeamListRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtEmployee mtEmployee = new MtEmployee();
            MtUser mtUser = new MtUser();
            TrTeam trTeam = new TrTeam();
            TrTeamRole trTeamRole = new TrTeamRole();
            List<GetEmployeeByTeamListResponse> responseList = new List<GetEmployeeByTeamListResponse>();
            try
            {
                List<string> deptCodeList = this.userAccess.teamRoleList.Select(a => a.deptCode).Distinct().ToList();
                List<string> teamCodeList = trTeam.GetDataByJobNoAndDeptCodeList(model.jobNo, deptCodeList).Select(a => a.TEAM_CODE).Distinct().ToList();
                if (teamCodeList.Count != 0)
                {
                    List<string> empNoList = trTeamRole.FindBy(a => teamCodeList.Contains(a.TEAM_CODE)).Select(a => a.EMP_NO).Distinct().ToList();
                    mtEmployee.Datas = mtEmployee.FindBy(a => empNoList.Contains(a.EMP_NO)).ToList();
                    foreach (var mtEmployeeData in mtEmployee.Datas)
                    {
                        GetEmployeeByTeamListResponse responseData = new GetEmployeeByTeamListResponse();
                        responseData.empId = mtEmployeeData.EMP_ID;
                        responseData.empNo = mtEmployeeData.EMP_NO;
                        responseData.title = mtEmployeeData.PRE_NAME_CODE;
                        responseData.firstName = mtEmployeeData.FIRST_NAME;
                        responseData.lastName = mtEmployeeData.LAST_NAME;
                        responseData.startDate = mtEmployeeData.START_DATE;
                        responseData.endDate = mtEmployeeData.END_DATE;
                        responseData.isActive = mtEmployeeData.IS_ACTIVE;
                        responseData.jobNo = model.jobNo;
                        mtUser.Data = mtUser.GetDataByEmpNo(mtEmployeeData.EMP_NO);
                        if (mtUser.Data != null)
                        {
                            responseData.avatar = mtUser.Data.AVATAR;
                            responseData.email = mtUser.Data.EMAIL;
                            responseData.userId = mtUser.Data.USER_ID.ToString();
                        }

                        responseList.Add(responseData);
                    }

                    actionResultStatus.data = responseList.OrderBy(r => r.firstName).ToList();
                }
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }


        public async Task<ActionResultStatus> GetContactList(GetContactListRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtPerson mtPerson = new MtPerson();
            TrCompanyPerson trCompanyPerson = new TrCompanyPerson();
            List<GetContactListResponse> responseList = new List<GetContactListResponse>();
            try
            {

                List<string> personCodeList = trCompanyPerson.FindBy(a => model.compCodeList.Contains(a.COMP_CODE)).Select(a => a.PERSON_CODE).Distinct().ToList();
                mtPerson.Datas = mtPerson.FindBy(a => personCodeList.Contains(a.PERSON_CODE)).ToList();
                foreach (var mtPersonData in mtPerson.Datas)
                {
                    GetContactListResponse dataResponse = new GetContactListResponse();

                    dataResponse.citizenId = mtPersonData.CITIZEN_ID;
                    dataResponse.email = mtPersonData.EMAIL;
                    dataResponse.firstName = mtPersonData.FIRST_NAME;
                    dataResponse.lastName = mtPersonData.LAST_NAME;
                    dataResponse.lineId = mtPersonData.LINE_ID;
                    dataResponse.mobile = mtPersonData.MOBILE;
                    dataResponse.passport = mtPersonData.PASSPORT;
                    dataResponse.personCode = mtPersonData.PERSON_CODE;
                    dataResponse.personId = mtPersonData.PERSON_ID;
                    dataResponse.preNameCode = mtPersonData.PRE_NAME_CODE;
                    dataResponse.preNameName = MtConficD.GetTName(mtPersonData.PRE_NAME_CODE);
                    dataResponse.telephone = mtPersonData.TELEPHONE;

                    trCompanyPerson.Data = trCompanyPerson.GetData(mtPersonData.PERSON_ID.ToString());
                    if (trCompanyPerson.Data != null)
                    {
                        dataResponse.contact = new ContactData();
                        dataResponse.contact.compCode = trCompanyPerson.Data.COMP_CODE;
                        dataResponse.contact.compName = MtCompany.GetName(trCompanyPerson.Data.COMP_CODE);
                        dataResponse.contact.compPersonId = trCompanyPerson.Data.COMP_PERSON_ID;
                        dataResponse.contact.email = trCompanyPerson.Data.EMAIL;
                        dataResponse.contact.isMain = trCompanyPerson.Data.IS_MAIN;
                        dataResponse.contact.jobPositionName = trCompanyPerson.Data.JOB_POSITION;
                    }

                    responseList.Add(dataResponse);
                }

                actionResultStatus.data = responseList;
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> SaveContact(SaveContactRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtPerson mtPerson = new MtPerson();
            TrCompanyPerson trCompanyPerson = new TrCompanyPerson();
            try
            {
                bool mtPersonStatus = false;
                var mtPersonData = mtPerson.Save(model, this.userAccess, out mtPersonStatus);
                if (mtPersonStatus)
                {
                    if (model.contact != null)
                    {
                        bool trCompanyPersonStatus = false;
                        model.personId = mtPersonData.PERSON_ID;
                        model.personCode = mtPersonData.PERSON_CODE;
                        model.contact.personCode = mtPersonData.PERSON_CODE;
                        var trCompanyPersonData = trCompanyPerson.Save(model.contact, this.userAccess, out trCompanyPersonStatus);
                        if (trCompanyPersonStatus)
                        {
                            model.contact.compPersonId = trCompanyPersonData.COMP_PERSON_ID;
                        }
                    }
                }
                actionResultStatus.data = model;
            }
            catch (Exception ex)
            {
                actionResultStatus.success = false;
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> GetJobList(GetJobListRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtProjectH mtProjectH = new MtProjectH();
            MtProjectD mtProjectD = new MtProjectD();
            TrTask trTask = new TrTask();
            TrAsignTo trAsignTo = new TrAsignTo();
            TrJobTask trJobTask = new TrJobTask();
            List<MtProjectDDTO> responseList = new List<MtProjectDDTO>();
            MtConfigD mtJobStatus = new MtConfigD();
            MtService mtService = new MtService();
            MtSubService mtSubService = new MtSubService();

            TrTeam trTeam = new TrTeam();
            TrTeamRole trTeamAssing = new TrTeamRole();



            try
            {
                mtProjectD.Datas = mtProjectD.GetJobListFilter(model, this.userAccess);

                var dataTrTeam = trTeam.GetDataByJobNo(mtProjectD.Datas.Select(r => r.JOB_NO).ToList());
                var dataTrTeamAssign = trTeamAssing.GetDatasByTeamCode(dataTrTeam.Select(r => r.TEAM_CODE).ToList());

                var JobNoList = new List<string>();
                
                if (userAccess.positionList.Any(r => r.positionCode == "DH"))
                {
                    var teamList = userAccess.teamRoleList.Select(r => r.empNo).ToList();
                    teamList = teamList.Where(r => r != userAccess.empNo).ToList();
                    JobNoList = (from a in dataTrTeam
                                 join b in dataTrTeamAssign on a.TEAM_CODE equals b.TEAM_CODE
                                 where mtProjectD.Datas.Select(r => r.JOB_NO).ToList().Contains(a.JOB_NO)
                                 && teamList.Contains(b.EMP_NO)
                                 //&& userAccess.positionList.Any(r => r.positionCode == "DH")
                                 select a.JOB_NO).Distinct().ToList();
                }

                //var JobNoList = (from a in dataTrTeam
                //                 join b in dataTrTeamAssign on a.TEAM_CODE equals b.TEAM_CODE
                //                 where mtProjectD.Datas.Select(r => r.JOB_NO).ToList().Contains(a.JOB_NO)
                //                 && userAccess.teamRoleList.Select(r => r.empNo).ToList().Contains(b.EMP_NO)
                //                 //&& userAccess.positionList.Any(r => r.positionCode == "DH")
                //                 select a.JOB_NO).Distinct().ToList();

                //step 1
                foreach (var mtProjectDData in mtProjectD.Datas)
                {
                    MtProjectDDTO dataResponse = new MtProjectDDTO();
                    dataResponse.projectDId = mtProjectDData.JOB_ID;
                    dataResponse.jobNo = mtProjectDData.JOB_NO;
                    dataResponse.jobName = mtProjectDData.JOB_NAME;
                    dataResponse.personCode = mtProjectDData.PERSON_CODE;
                    dataResponse.jobDesc = mtProjectDData.JOB_DESC;
                    dataResponse.workBudget = mtProjectDData.WORK_BUDGET;
                    dataResponse.issueBy = mtProjectDData.ISSUE_BY;
                    dataResponse.issueDate = mtProjectDData.ISSUE_DATE;
                    dataResponse.isActive = mtProjectDData.IS_ACTIVE;
                    dataResponse.isAssigned = JobNoList.Any(r => r == mtProjectDData.JOB_NO) ? "Y" : "N";
                    dataResponse.briefDate = mtProjectDData.BRIEF_DATE;
                    dataResponse.dueDate = mtProjectDData.DUE_DATE;
                    dataResponse.jobYear = mtProjectDData.JOB_YEAR;

                    dataResponse.projectCode = mtProjectDData.PROJECT_CODE;
                    dataResponse.projectName = MtProjectH.GetName(mtProjectDData.PROJECT_CODE, mtProjectDData.COMP_CODE);
                    dataResponse.compCode = mtProjectDData.COMP_CODE;
                    dataResponse.compName = MtCompany.GetName(mtProjectDData.COMP_CODE);
                    dataResponse.jobStatusCode = mtProjectDData.JOB_STATUS_CODE;
                    dataResponse.jobStatusName = MtConficD.GetEName(mtProjectDData.JOB_STATUS_CODE);
                    dataResponse.srvcCode = mtProjectDData.SERVICE_CODE;
                    dataResponse.srvcName = MtService.GetName(mtProjectDData.SERVICE_CODE);
                    dataResponse.subSrvcCode = mtProjectDData.SUB_SERVICE_CODE;
                    dataResponse.subSrvcName = MtSubService.GetName(mtProjectDData.SUB_SERVICE_CODE);

                    dataResponse.trJobTask = new List<TrJobTaskDto>();
                    List<string> deptCodeListByUserAccess = this.userAccess.teamRoleList.Select(a => a.deptCode).ToList();
                    trJobTask.Datas = trJobTask.GetJobList(mtProjectDData.JOB_NO);
                    foreach (var trTaskData in trJobTask.Datas)
                    {

                        TrJobTaskDto trTaskDataResponse = new TrJobTaskDto();
                        trTaskDataResponse.jobTaskId = trTaskData.JOB_TASK_ID;
                        trTaskDataResponse.jobTaskCode = trTaskData.JOB_TASK_CODE;
                        trTaskDataResponse.jobTaskName = trTaskData.JOB_TASK_NAME;
                        trTaskDataResponse.jobNo = trTaskData.JOB_NO;
                        trTaskDataResponse.dueDate = trTaskData.DUE_DATE;
                        trTaskDataResponse.planStartDate = trTaskData.PLAN_START_DATE;
                        trTaskDataResponse.planEndDate = trTaskData.PLAN_END_DATE;
                        trTaskDataResponse.dueDate = trTaskData.DUE_DATE;
                        trTaskDataResponse.note = trTaskData.NOTE;
                        trTaskDataResponse.priorityCode = trTaskData.PRIORITY_CODE;
                        trTaskDataResponse.priorityName = MtConficD.GetEName(trTaskData.PRIORITY_CODE);
                        trTaskDataResponse.deptCode = trTaskData.DEPT_CODE;
                        trTaskDataResponse.deptName = MtDepartment.GetName(trTaskData.DEPT_CODE);
                        trTaskDataResponse.jobTaskStatusCode = trTaskData.JOB_TASK_STATUS_CODE;
                        trTaskDataResponse.jobTaskStatusName = MtConficD.GetEName(trTaskData.JOB_TASK_STATUS_CODE);

                        dataResponse.trJobTask.Add(trTaskDataResponse);
                    }

                    responseList.Add(dataResponse);
                }

                // ดึงรายการที่ พนักงานคนนี้ ที่เป็น CS under บ้าง และมี job no ไหนบ้าง
                MtTeamRole mtTeamRole = new MtTeamRole();
                TrCompanyHeadCs trClientHeader = new TrCompanyHeadCs();
                var jobHeaderCs = new List<string>();

                mtTeamRole.Datas = mtTeamRole.GetTeamRoleByEmpNo(userAccess.empNo)
                    .Where(r => r.DEPT_CODE == "CS" && r.PARENT_EMP_NO != "0" && !r.PARENT_EMP_NO.IsNullOrEmptyWhiteSpace()).ToList();

                trClientHeader.Datas = trClientHeader.GetAll().Where(r => r.IS_ACTIVE == "Y").ToList()
                    .Where(r => mtTeamRole.Datas.Select(e => e.PARENT_EMP_NO).ToList().Contains(r.EMP_NO)).ToList();


                //step 2
                var clientCareList = GetClientCareList(model.deptCodeList);
                var myClientCareList = clientCareList.Where(r => r.empNo == userAccess.empNo).FirstOrDefault();
                if (myClientCareList != null)
                {
                    if (myClientCareList.deptCode == "CS" && myClientCareList.deptCode != "DH")
                    {
                        responseList = responseList.Where(r => trClientHeader.Datas.Select(e => e.COMP_CODE).ToList().Contains(r.compCode)).ToList();
                    }
                    responseList = responseList.Where(r => myClientCareList.companyList.Select(e => e.compCode).ToList().Contains(r.compCode)).ToList();
                    //responseList = responseList.Where(r => trClientHeader.Datas.Select(e => e.COMP_CODE).ToList().Contains(r.compCode)).ToList();
                }
                else
                {
                    //เลข job ที่ได้จากหัวหน้าอื่นๆ
                    jobHeaderCs = mtProjectD.Datas.Where(r => trClientHeader.Datas.Select(e => e.COMP_CODE).ToList().Contains(r.COMP_CODE)).Select(r => r.JOB_NO).Distinct().ToList();
                    


                    //step 3
                    var jonNoFromTeamWithTeamAssign = (from a in dataTrTeam
                                                       join b in dataTrTeamAssign on a.TEAM_CODE equals b.TEAM_CODE
                                                       where b.EMP_NO == userAccess.empNo
                                                       select a.JOB_NO).Distinct().ToList();

                    jonNoFromTeamWithTeamAssign = jonNoFromTeamWithTeamAssign.Concat(jobHeaderCs).Distinct().ToList();

                    responseList = responseList.Where(r => jonNoFromTeamWithTeamAssign.Contains(r.jobNo)).ToList();
                }
                //return 
                actionResultStatus.data = responseList.OrderByDescending(r => r.jobNo);
            }
            catch (Exception ex)
            {
                actionResultStatus.success = false;
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> GetJobDetail(GetJobDetailRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtProjectH mtProjectH = new MtProjectH();
            MtProjectD mtProjectD = new MtProjectD();
            TrTask trTask = new TrTask();
            TrAsignTo trAsignTo = new TrAsignTo();
            TrJobTask trJobTask = new TrJobTask();
            MtConfigD mtJobStatus = new MtConfigD();
            MtService mtService = new MtService();
            MtSubService mtSubService = new MtSubService();
            MtProjectDDTO dataResponse = new MtProjectDDTO();
            TrTeam trTeam = new TrTeam();
            TrTeamRole trTeamRole = new TrTeamRole();
            MtEmployee mtEmp = new MtEmployee();
            TrJobAttachment trJobAttachment = new TrJobAttachment();

            try
            {
                var mtProjectDData = mtProjectD.GetDataByJobNo(model.jobNo);
                var dataMtEmployee = mtEmp.GetAll().ToList();
                if (mtProjectDData != null)
                {
                    var dataEmp = dataMtEmployee.Where(r => r.EMP_NO == mtProjectDData.ISSUE_BY).FirstOrDefault();

                    dataResponse.projectDId = mtProjectDData.JOB_ID;
                    dataResponse.projectCode = mtProjectDData.PROJECT_CODE;
                    dataResponse.jobNo = mtProjectDData.JOB_NO;
                    dataResponse.jobName = mtProjectDData.JOB_NAME;
                    dataResponse.srvcCode = mtProjectDData.SERVICE_CODE;
                    dataResponse.srvcName = MtService.GetName(mtProjectDData.SERVICE_CODE);
                    dataResponse.subSrvcCode = mtProjectDData.SUB_SERVICE_CODE;
                    dataResponse.subSrvcName = MtSubService.GetName(mtProjectDData.SUB_SERVICE_CODE);
                    dataResponse.personCode = mtProjectDData.PERSON_CODE;
                    dataResponse.jobDesc = mtProjectDData.JOB_DESC;
                    dataResponse.workBudget = mtProjectDData.WORK_BUDGET;
                    dataResponse.issueBy = mtProjectDData.ISSUE_BY;
                    dataResponse.issueByName = dataEmp != null ? dataEmp.FIRST_NAME + " " + dataEmp.LAST_NAME : "";
                    dataResponse.issueDate = mtProjectDData.ISSUE_DATE;
                    dataResponse.isActive = mtProjectDData.IS_ACTIVE;
                    dataResponse.dueDate = mtProjectDData.DUE_DATE;
                    dataResponse.briefDate = mtProjectDData.BRIEF_DATE;
                    dataResponse.compCode = mtProjectDData.COMP_CODE;
                    dataResponse.projectCode = mtProjectDData.PROJECT_CODE;
                    dataResponse.projectName = MtProjectH.GetName(mtProjectDData.PROJECT_CODE);
                    dataResponse.compName = MtCompany.GetName(mtProjectDData.COMP_CODE);
                    dataResponse.jobStatusCode = mtProjectDData.JOB_STATUS_CODE;
                    dataResponse.jobStatusName = MtConficD.GetEName(mtProjectDData.JOB_STATUS_CODE);
                    dataResponse.jobYear = mtProjectDData.JOB_YEAR;


                    dataResponse.trJobTask = new List<TrJobTaskDto>();
                    trJobTask.Datas = trJobTask.GetDataByJobNo(mtProjectDData.JOB_NO);
                    foreach (var trTaskData in trJobTask.Datas)
                    {
                        TrJobTaskDto trTaskDataResponse = new TrJobTaskDto();
                        trTaskDataResponse.jobTaskId = trTaskData.JOB_TASK_ID;
                        trTaskDataResponse.jobTaskCode = trTaskData.JOB_TASK_CODE;
                        trTaskDataResponse.jobTaskName = trTaskData.JOB_TASK_NAME;
                        trTaskDataResponse.jobNo = trTaskData.JOB_NO;
                        trTaskDataResponse.dueDate = trTaskData.DUE_DATE;
                        trTaskDataResponse.planStartDate = trTaskData.PLAN_START_DATE;
                        trTaskDataResponse.planEndDate = trTaskData.PLAN_END_DATE;
                        trTaskDataResponse.dueDate = trTaskData.DUE_DATE;
                        trTaskDataResponse.note = trTaskData.NOTE;
                        trTaskDataResponse.priorityCode = trTaskData.PRIORITY_CODE;
                        trTaskDataResponse.priorityName = MtConficD.GetEName(trTaskData.PRIORITY_CODE);
                        trTaskDataResponse.deptCode = trTaskData.DEPT_CODE;
                        trTaskDataResponse.deptName = MtDepartment.GetName(trTaskData.DEPT_CODE);
                        trTaskDataResponse.jobTaskStatusCode = trTaskData.JOB_TASK_STATUS_CODE;
                        trTaskDataResponse.jobTaskStatusName = MtConficD.GetEName(trTaskData.JOB_TASK_STATUS_CODE);

                        dataResponse.trJobTask.Add(trTaskDataResponse);
                        trTaskDataResponse.team = new TeamDatas();
                        trTeam.Data = trTeam.GetDataByJobTaskCode(trTaskData.JOB_TASK_CODE);
                       if(trTeam.Data != null)
                        {

                            trTaskDataResponse.team.teamCode = trTeam.Data.TEAM_CODE;
                            trTaskDataResponse.team.teamName = trTeam.Data.TEAM_NAME;
                            trTaskDataResponse.team.jobTaskCode = trTeam.Data.JOB_TASK_CODE;


                            trTeamRole.Datas = trTeamRole.GetDatas(trTeam.Data.TEAM_CODE);
                            trTaskDataResponse.team.teamRoleList = new List<RRPlatFormAPI.HTTP.Response.TeamRoleDatas>();

                            foreach (var trTeamRoleData in trTeamRole.Datas)
                            {
                                MtEmployeePosition mtEmployeePosition = new MtEmployeePosition();
                                var dataPositionList = mtEmployeePosition.GetDatasByEmpNo(trTeamRoleData.EMP_NO);
                                RRPlatFormAPI.HTTP.Response.TeamRoleDatas teamRoleDatas = new RRPlatFormAPI.HTTP.Response.TeamRoleDatas();

                                var empTeam = dataMtEmployee.Where(r => r.EMP_NO == trTeamRoleData.EMP_NO).FirstOrDefault();

                                teamRoleDatas.empNo = trTeamRoleData.EMP_NO;
                                teamRoleDatas.empName = empTeam != null ? empTeam.FIRST_NAME + " " + empTeam.LAST_NAME : "";
                                teamRoleDatas.positionCode = new List<string>();
                                foreach (var itemPosition in dataPositionList)
                                {
                                    teamRoleDatas.positionCode.Add(itemPosition.POSITION_CODE);
                                }
                                teamRoleDatas.isActive = trTeamRoleData.IS_ACTIVE;
                                teamRoleDatas.isMain = trTeamRoleData.IS_MAIN;
                                trTaskDataResponse.team.teamRoleList.Add(teamRoleDatas);
                            }
                        }
                    }

                    dataResponse.jobAttactList = new List<SaveJobAttactFileData>();
                    trJobAttachment.Datas = trJobAttachment.GetDataByJobNo(mtProjectDData.JOB_NO);
                    foreach (var trJobAttachmentData in trJobAttachment.Datas)
                    {
                        SaveJobAttactFileData trJobAttachmentDto = new SaveJobAttactFileData();
                        trJobAttachmentDto.fileName = trJobAttachmentData.FILE_NAME;
                        trJobAttachmentDto.filePath = trJobAttachmentData.FILE_PATH;
                        trJobAttachmentDto.fileType = trJobAttachmentData.FILE_TYPE;
                        trJobAttachmentDto.isActive = trJobAttachmentData.IS_ACTIVE;
                        trJobAttachmentDto.jobNo = trJobAttachmentData.JOB_NO;
                        trJobAttachmentDto.oldFileName = trJobAttachmentData.OLD_FILE_NAME;
                        trJobAttachmentDto.seq = trJobAttachmentData.SEQ;
                        trJobAttachmentDto.jobAttachId = trJobAttachmentData.JOB_ATTACHMENT_ID;
                        dataResponse.jobAttactList.Add(trJobAttachmentDto);
                    }
                }
                else
                {
                    dataResponse = null;
                }


                actionResultStatus.data = dataResponse;
            }
            catch (Exception ex)
            {
                actionResultStatus.success = false;
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> SaveJob(SaveJobRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtProjectD mtProjectD = new MtProjectD();
            TrJobTask trJobTask = new TrJobTask();
            TrCompanyHeadCs trCompanyHeadCs = new TrCompanyHeadCs();
            TrJobAttachment trJobAttachment = new TrJobAttachment();
            // save notifyMsg
            NotifyBL notifyBL = new NotifyBL();
            MtNotifyConfig mstNotiConfig = new MtNotifyConfig();
            TrNotifyMsg trNotiMsg = new TrNotifyMsg();
            try
            {
                bool isSandNotifyToHeadDetp = mtProjectD.checkCanSandNotify(model.jobNo, model.jobStatusCode);
                bool mtProjectDStatus = false;
                bool vacationJobStatus = false;
                var mtProjectDData = mtProjectD.Save(model, this.userAccess, out mtProjectDStatus,out vacationJobStatus);
                List<string> deptCodeList = new List<string>();

                if (vacationJobStatus)
                {
                    deptCodeList = model.jobTaskList.Select(a => a.deptCode).ToList();
                    MtEmployeePosition mtEmployeePosition = new MtEmployeePosition();
                    // save task
                    foreach (var deptCode in deptCodeList)
                    {
                        SaveTaskRequest saveTaskRequest = new SaveTaskRequest();
                        saveTaskRequest.compCode = mtProjectDData.COMP_CODE;
                        saveTaskRequest.dueDate = mtProjectDData.DUE_DATE;
                        saveTaskRequest.estimated = 0;
                        saveTaskRequest.jobNo = mtProjectDData.JOB_NO;
                        saveTaskRequest.note = null;
                        saveTaskRequest.planEndDate = null;
                        saveTaskRequest.planStartDate = null;
                        saveTaskRequest.priorityCode = "LW";
                        saveTaskRequest.projectCode = mtProjectDData.PROJECT_CODE;
                        saveTaskRequest.taskCode = "0";
                        saveTaskRequest.taskGroupCode = mtProjectDData.SUB_SERVICE_CODE == "36" ? "T36" : "T35";
                        saveTaskRequest.taskName = mtProjectDData.JOB_NAME ;
                        saveTaskRequest.taskStatusCode = "OP";
                        saveTaskRequest.assignList = new List<SaveTaskAssignDatas>();

                        mtEmployeePosition.Datas = mtEmployeePosition.GetDatasByDeptCode(deptCode);
                        foreach (var mtEmployeePositionData in mtEmployeePosition.Datas)
                        {
                            SaveTaskAssignDatas saveTaskAssignDatas = new SaveTaskAssignDatas();
                            saveTaskAssignDatas.empNo = mtEmployeePositionData.EMP_NO;
                            saveTaskAssignDatas.isActive = StatusYesNo.Yes.Value();
                            saveTaskAssignDatas.taskCode = "0";
                            saveTaskAssignDatas.isSendNotify = StatusYesNo.Yes.Value();
                            saveTaskRequest.assignList.Add(saveTaskAssignDatas);
                        }

                        this.SaveTask(saveTaskRequest);
                    }
                   
                }
                else
                { 
                    if (mtProjectDStatus)
                    {

                        foreach (var trJobTaskData in model.jobTaskList)
                        {
                            bool trTaskStatus = false;
                            trJobTaskData.jobNo = mtProjectDData.JOB_NO;
                            trJobTaskData.compCode = mtProjectDData.COMP_CODE;
                            trJobTaskData.projectCode = model.projectCode;
                            trJobTask.Save(trJobTaskData, this.userAccess, out trTaskStatus);
                            deptCodeList.Add(trJobTaskData.deptCode);
                            List<string> dept = new List<string>();
                            dept.Add(trJobTaskData.deptCode);
                            //save Team 
                            TrTeam trTeam = new TrTeam();
                            trTeam.SaveTeamJobAssign(dept, model.jobNo, this.userAccess, trJobTask.Data.JOB_TASK_CODE);
                        }


                        foreach (var jobAttactData in model.jobAttactList)
                        {
                            bool jobAttactStatus = false;
                            trJobAttachment.Data = new TR_JOB_ATTACHMENT();
                            TrJobAttachmentDto trJobAttachmentDto = new TrJobAttachmentDto();
                            trJobAttachmentDto.fileName = jobAttactData.fileName;
                            trJobAttachmentDto.filePath = jobAttactData.filePath;
                            trJobAttachmentDto.fileType = jobAttactData.fileType;
                            trJobAttachmentDto.isActive = jobAttactData.isActive;
                            trJobAttachmentDto.jobNo = mtProjectDData.JOB_NO;
                            trJobAttachmentDto.oldFileName = jobAttactData.oldFileName;
                            trJobAttachmentDto.seq = jobAttactData.seq;
                            trJobAttachmentDto.jobAttachId = jobAttactData.jobAttachId;

                            trJobAttachment.Save(trJobAttachmentDto, this.userAccess, out jobAttactStatus);

                        }



                    }

                    var dataClientCareHeadList = trCompanyHeadCs.GetDatasByCompCode(model.compCode);

                    var notifyMsg = new SaveNotiMsgRequest();
                    notifyBL.userAccess = this.userAccess;
                    var noitConfig = mstNotiConfig.GetDataByCode("NOT002");
                    if (model.jobNo == "")
                    {
                        foreach (var itemEmp in dataClientCareHeadList)
                        {
                            notifyMsg = new SaveNotiMsgRequest()
                            {
                                notifyMsgId = 0,
                                createBy = userAccess.userName,
                                empNo = itemEmp.EMP_NO,
                                notifyDesc = "Issue By: " + model.issueBy + " Due date: " + model.dueDate + " : " + model.jobName,
                                refKey = "JOB_NO",
                                refTable = nameof(TR_JOB),
                                refValue = mtProjectDData.JOB_NO,
                                notifyHeader = noitConfig.NOTIFY_CONFIG_HEADER_TEMPLATE + " " + mtProjectDData.JOB_NO,
                                isActive = "Y",
                                notifyMsgCode = trNotiMsg.getNotifyMsgRunning($"{noitConfig.NOTIFY_CONFIG_CODE}{DateTime.Now.ToString("yy")}{DateTime.Now.ToString("MM")}{DateTime.Now.ToString("dd")}", 3),
                                notifyTopicCode = noitConfig.NOTIFY_TOPIC_CODE,
                                url = $"{noitConfig.URL}{mtProjectDData.JOB_NO}",
                                notifyGroupCode = "NOG002"
                            };
                            notifyBL.SaveNotifyMsg(notifyMsg);
                        }

                    }
                    model.jobNo = mtProjectDData.JOB_NO;
                    if (isSandNotifyToHeadDetp)
                    {
                        notifyBL.SaveNotifyMsgForJob("NOT001", model.jobNo, mtProjectDData.JOB_NAME, mtProjectDData.DUE_DATE.ToString("dd/MM/yyyy"), deptCodeList);
                    }
                }
                

                actionResultStatus.data = model;
            }
            catch (Exception ex)
            {
                actionResultStatus.success = false;
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> SaveJobTask(SaveJobTarkRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtProjectD mtProjectD = new MtProjectD();
            TrJobTask trJobTask = new TrJobTask();
            TrCompanyHeadCs trCompanyHeadCs = new TrCompanyHeadCs();
            try
            {
                bool mtrJobTaskStatus = false;
                trJobTask.Save(model, this.userAccess, out mtrJobTaskStatus);
                if (model.teamList.Count() == 0)
                {
                    TrTeam trTeam = new TrTeam();
                    List<string> deptCode = new List<string>();
                    deptCode.Add(model.deptCode);
                    trTeam.SaveTeamJobAssign(deptCode, model.jobNo, this.userAccess, trJobTask.Data.JOB_TASK_CODE);
                }
                else
                {
                    NotifyBL notifyBL = new NotifyBL();
                    MtNotifyConfig mstNotiConfig = new MtNotifyConfig();
                    TrNotifyMsg trNotiMsg = new TrNotifyMsg();

                    var notifyMsg = new SaveNotiMsgRequest();
                    notifyBL.userAccess = this.userAccess;
                    var noitConfig = mstNotiConfig.GetDataByCode("NOT003");


                    foreach (var teamData in model.teamList)
                    {
                        bool teamStatus = false;
                        TrTeam trTeam = new TrTeam();
                        teamData.jobNo = trJobTask.Data.JOB_NO;
                        teamData.deptCode = trJobTask.Data.DEPT_CODE;
                        teamData.jobTaskCode = model.jobTaskCode;
                        teamData.teamCode = teamData.teamCode;
                        var trTeamData = trTeam.Save(teamData, this.userAccess, out teamStatus);

                        foreach (var teamRoleData in teamData.teamRoleList)
                        {
                            TrTeamRole teamRole = new TrTeamRole();
                            teamRoleData.teamCode = trTeamData.TEAM_CODE;
                            teamRole.Save(teamRoleData, this.userAccess, out teamStatus);

                            if (teamRoleData.isSendNotify == "Y") {
                                // save notifyMsg
                                notifyMsg = new SaveNotiMsgRequest()
                                {
                                    notifyMsgId = 0,
                                    createBy = userAccess.userName,
                                    empNo = teamRoleData.empNo,
                                    notifyDesc = "คุณถูกเชิญเข้าทีม " + trJobTask.Data.JOB_NO + " Due date: " + model.dueDate + " : " + model.jobTaskName,
                                    refKey = "JOB_TASK_CODE",
                                    refTable = nameof(TR_JOB),
                                    refValue = trJobTask.Data.JOB_TASK_CODE,
                                    notifyHeader = noitConfig.NOTIFY_CONFIG_HEADER_TEMPLATE + " " + trJobTask.Data.JOB_NO,
                                    isActive = "Y",
                                    notifyMsgCode = trNotiMsg.getNotifyMsgRunning($"{noitConfig.NOTIFY_CONFIG_CODE}{DateTime.Now.ToString("yy")}{DateTime.Now.ToString("MM")}{DateTime.Now.ToString("dd")}", 3),
                                    notifyTopicCode = noitConfig.NOTIFY_TOPIC_CODE,
                                    url = $"{noitConfig.URL}{trJobTask.Data.JOB_NO}",
                                    notifyGroupCode = "NOG003"
                                };
                                notifyBL.SaveNotifyMsg(notifyMsg);
                            }
                        }
                    }
                }


                actionResultStatus.data = model;
            }
            catch (Exception ex)
            {
                actionResultStatus.success = false;
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }


        public async Task<ActionResultStatus> GetDataTimeSheet(GetDataTimeSheetRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtProjectD mtProjectD = new MtProjectD();
            TrTask trTask = new TrTask();
            TrAsignTo trAsignTo = new TrAsignTo();
            TrTimeSheet trTimeSheet = new TrTimeSheet();
            MtProjectH mtProjectH = new MtProjectH();
            MtTaskGroup mtTaskGroup = new MtTaskGroup();
            MtUser mtUser = new MtUser();
            //MtTaskStatus mtTaskStatus = new MtTaskStatus();

            List<GetDataTimeSheetResponse> responseList = new List<GetDataTimeSheetResponse>();
            try
            {
                trTask.Datas = trTask.GetDataByProjectCodeList(model.projectCodeList);
                if (model.compCodeList.Count > 0)
                {
                    trTask.Datas = trTask.Datas.Where(r => model.compCodeList.Contains(r.COMP_CODE)).ToList();
                }
                //trTask.Datas = trTask.Datas.Where(r => r.PLAN_END_DATE.Value.Date >= model.endDate.Value.Date).ToList();
                List<string> taskCodeList = trTask.Datas.Select(a => a.TASK_CODE).ToList();

                if (model.empNoList.Count == 0)
                {
                    model.empNoList = new List<string>();
                    MtEmployee mtEmployee = new MtEmployee();
                    mtEmployee.Datas = mtEmployee.GetDataByUserId(this.userAccess.empNo);
                    model.empNoList = mtEmployee.Datas.Select(a => a.EMP_NO).Distinct().ToList();
                }

                trAsignTo.Datas = trAsignTo.GetDatasByUserIdList(model.empNoList, taskCodeList);
                mtProjectH.Datas = mtProjectH.GetDataByProjectCodeAndCompCodeList(trTask.Datas.Select(r => r.PROJECT_CODE).ToList(), trTask.Datas.Select(r => r.COMP_CODE).ToList());
                var empList = trAsignTo.Datas.Select(r => r.EMP_NO).ToList();
                mtUser.Datas = mtUser.FindBy(a => empList.Contains( a.EMP_NO)).ToList();
                var tastGroupCodeList = trTask.Datas.Select(e => e.TASK_GROUP_CODE).ToList();
                mtTaskGroup.Datas = mtTaskGroup.FindBy(r => tastGroupCodeList.Contains(r.TASK_GROUP_CODE)).ToList();
                trTimeSheet.Datas = trTimeSheet.GetDatasByTaskCodeActive(trTask.Datas.Select(e => e.TASK_CODE).ToList());
                foreach (var trAsignToData in trAsignTo.Datas)
                {
                    GetDataTimeSheetResponse dataResponse = new GetDataTimeSheetResponse();

                    trTask.Data = trTask.Datas.Where(a => a.TASK_CODE == trAsignToData.TASK_CODE).FirstOrDefault();
                    mtProjectH.Data = mtProjectH.Datas.Where(r => r.PROJECT_CODE == trTask.Data.PROJECT_CODE && r.COMP_CODE == trTask.Data.COMP_CODE).FirstOrDefault();
                    mtUser.Data = mtUser.Datas.Where(a => a.EMP_NO == trAsignToData.EMP_NO).FirstOrDefault();
                    mtTaskGroup.Data = mtTaskGroup.Datas.Where(r => r.TASK_GROUP_CODE == trTask.Data.TASK_GROUP_CODE).FirstOrDefault();
                    //mtTaskStatus.Data = mtTaskStatus.GetDataByCode(trTask.Data.PROGRESS_STATUS_CODE);

                    dataResponse.assingToId = trAsignToData.ASSIGN_TO_ID;
                    dataResponse.empNo = trAsignToData.EMP_NO;
                    dataResponse.userFullName = mtUser.Data.NAME;
                    dataResponse.email = trAsignToData.EMAIL;
                    dataResponse.taskCode = trAsignToData.TASK_CODE;
                    dataResponse.taskId = trTask.Data.TASK_ID;
                    dataResponse.jobNo = trTask.Data.JOB_NO;
                    dataResponse.taskName = trTask.Data.TASK_NAME;
                    dataResponse.projectCode = mtProjectH?.Data?.PROJECT_CODE;
                    dataResponse.projectName = mtProjectH?.Data?.PROJECT_NAME;
                    dataResponse.taskGroupCode = trTask.Data.TASK_GROUP_CODE;
                    dataResponse.taskGroupName = mtTaskGroup.Data == null ? "" : mtTaskGroup.Data.TASK_GROUP_NAME;
                    dataResponse.priorityCode = trTask.Data.PRIORITY_CODE;
                    dataResponse.priorityName = MtConficD.GetEName(trTask.Data.PRIORITY_CODE, "PRIORITY");
                    dataResponse.planStartDate = trTask.Data.PLAN_START_DATE;
                    dataResponse.planEndDate = trTask.Data.PLAN_END_DATE;
                    dataResponse.dueDate = trTask.Data.DUE_DATE;
                    dataResponse.note = trTask.Data.NOTE;
                    dataResponse.compCode = trTask.Data.COMP_CODE;
                    dataResponse.taskStatusCode = trTask.Data.TASK_STATUS_CODE;
                    dataResponse.taskStatusName = MtConficD.GetEName(trTask.Data.TASK_STATUS_CODE, "TASK_STATUS");
                    dataResponse.estimated = trTask.Data.ESTIMATED;
                    dataResponse.taskActiveOnTime = new TaskActiveOntime();
                    dataResponse.summaryTimeList = trTimeSheet.GetDatasByTaskCode(trAsignToData.TASK_CODE, model.startDate, model.endDate, trAsignToData.EMP_NO);
                    dataResponse.totalDuration = dataResponse.summaryTimeList.Sum(a => a.workTime);
                    dataResponse.createDate = trTask.Data.CREATE_DATE;

                    trTimeSheet.Data = trTimeSheet.Datas.Where(r => r.TASK_CODE == trAsignToData.TASK_CODE && r.EMP_NO == trAsignToData.EMP_NO).FirstOrDefault();
                    if (trTimeSheet.Data != null)
                    {
                        dataResponse.taskActiveOnTime.startDate = trTimeSheet.Data.START_DATE;
                        dataResponse.taskActiveOnTime.endDate = trTimeSheet.Data.END_DATE;
                        dataResponse.taskActiveOnTime.timeSheetId = trTimeSheet.Data.TIME_SHEET_ID;
                        dataResponse.taskActiveOnTime.empNo = trTimeSheet.Data.EMP_NO;
                        dataResponse.taskActiveOnTime.workTime = trTimeSheet.Data.WORK_TIME;
                        dataResponse.taskActiveOnTime.isActive = trTimeSheet.Data.IS_ACTIVE;
                    }

                    dataResponse.jobData = new JobDatas();

                    mtProjectD.Data = mtProjectD.GetDataByJobNo(trTask.Data.JOB_NO);
                    if (mtProjectD.Data != null)
                    {
                        dataResponse.jobData.jobNo = trTask.Data.JOB_NO;
                        dataResponse.jobData.jobName = mtProjectD.Data.JOB_NAME;
                        dataResponse.jobData.briefDate = mtProjectD.Data.BRIEF_DATE;
                        dataResponse.jobData.contactName =  MtPerson.GetName(mtProjectD.Data.PERSON_CODE);
                        dataResponse.jobData.dueDate = mtProjectD.Data.DUE_DATE;
                        dataResponse.jobData.issueByName = MtEmployee.GetName(mtProjectD.Data.ISSUE_BY);
                        dataResponse.jobData.issueDate = mtProjectD.Data.ISSUE_DATE;
                        dataResponse.jobData.serviceName = MtService.GetName( mtProjectD.Data.SERVICE_CODE);
                        dataResponse.jobData.subServiceName = MtSubService.GetName(mtProjectD.Data.SUB_SERVICE_CODE);
                    }
                    responseList.Add(dataResponse);
                }
                actionResultStatus.data = responseList.OrderBy(a => a.empNo == this.userAccess.empNo ? 0 : 1).ThenBy(a => a.userFullName).OrderByDescending(b => b.createDate).ToList();
            }
            catch (Exception ex)
            {
                actionResultStatus.success = false;
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> GetTaskCerrentTimeSheet(GetTaskCerrentTimeSheetRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtProjectD mtProjectD = new MtProjectD();
            TrTask trTask = new TrTask();
            TrAsignTo trAsignTo = new TrAsignTo();
            TrTimeSheet trTimeSheet = new TrTimeSheet();
            MtProjectH mtProjectH = new MtProjectH();
            MtTaskGroup mtTaskGroup = new MtTaskGroup();
            MtUser mtUser = new MtUser();
            //MtTaskStatus mtTaskStatus = new MtTaskStatus();
            GetTaskCerrentTimeSheetResponse dataResponse = new GetTaskCerrentTimeSheetResponse();

            try
            {
                string activeSatus = TimeSheetStatus.Active.Value();

                var trTimeSheetData = trTimeSheet.FindBy(a => a.EMP_NO.ToString() == this.userAccess.empNo
                                                         && a.TIME_SHEET_ACTIVE == activeSatus
                                                         && a.WORK_TIME == 0).FirstOrDefault();
                if (trTimeSheetData != null)
                {
                    dataResponse.timeSheetId = trTimeSheetData.TIME_SHEET_ID;
                    dataResponse.taskCode = trTimeSheetData.TASK_CODE;
                    dataResponse.startDate = trTimeSheetData.START_DATE;
                    dataResponse.endDate = trTimeSheetData.END_DATE;

                    trTask.Data = trTask.GetDataByTaskCode(trTimeSheetData.TASK_CODE);
                    if (trTask.Data != null)
                    {
                        dataResponse.taskName = trTask.Data.TASK_NAME;
                        dataResponse.priorityCode = trTask.Data.PRIORITY_CODE;

                        MtPriority mtPriority = new MtPriority();
                        var mtPriorityData = mtPriority.GetDataByCode(trTask.Data.PRIORITY_CODE);
                        if (mtPriorityData != null)
                            dataResponse.priorityName = mtPriorityData.PRIORITY_NAME;


                        var mtProjectHData = mtProjectH.GetDataByProjectCode(trTask.Data.PROJECT_CODE);
                        if (mtProjectHData != null)
                        {
                            MtCompany mtCompany = new MtCompany();
                            dataResponse.projectCode = mtProjectHData.PROJECT_CODE;
                            dataResponse.projectName = mtProjectHData.PROJECT_NAME;
                            dataResponse.compCode = mtProjectHData.COMP_CODE;
                            dataResponse.compName = mtCompany.FindBy(a => a.COMP_CODE == mtProjectHData.COMP_CODE).FirstOrDefault().COMP_NAME;
                        }

                        var mtProjectDData = mtProjectD.GetDataByJobNo(trTask.Data.JOB_NO);
                        if (mtProjectHData != null)
                        {
                            dataResponse.jobCode = trTask.Data.JOB_NO;
                            //dataResponse.jobName = mtProjectDData.JOB_NAME;
                        }
                    }

                    var summaryTimeList = trTimeSheet.GetDatasByTaskCode(trTimeSheetData.TASK_CODE);
                    dataResponse.totalWorkingTime = summaryTimeList.Sum(a => a.workTime);

                    actionResultStatus.data = dataResponse;
                }
                else
                {
                    actionResultStatus.data = null;

                }

            }
            catch (Exception ex)
            {
                actionResultStatus.success = false;
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> GetTimeSheetHistoryByTaskCode(GetTimeSheetHistoryByTaskCode model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            List<GetTimeSheetActiveByUserIdResponse> response = new List<GetTimeSheetActiveByUserIdResponse>();
            TrTimeSheet trTimeSheet = new TrTimeSheet();
            try
            {
                trTimeSheet.Datas = trTimeSheet.GetDataByTaskCode(model.taskCode, this.userAccess.empNo);
                foreach (var item in trTimeSheet.Datas)
                {
                    GetTimeSheetActiveByUserIdResponse timeSheet = new GetTimeSheetActiveByUserIdResponse();
                    timeSheet.taskCode = item.TASK_CODE;
                    timeSheet.startDate = item.START_DATE;
                    timeSheet.endDate = item.END_DATE;
                    timeSheet.empNo = item.EMP_NO;
                    timeSheet.timeSheetId = item.TIME_SHEET_ID;
                    timeSheet.workTime = item.WORK_TIME;
                    timeSheet.timeSheetActiveStatusCode = item.TIME_SHEET_ACTIVE;
                    timeSheet.createDate = item.CREATE_DATE;
                    string timeSheetActiveStatusName = string.Empty;
                    switch (item.TIME_SHEET_ACTIVE)
                    {
                        case "C":
                            timeSheetActiveStatusName = TimeSheetStatus.Cancel.Description();
                            break;
                        case "M":
                            timeSheetActiveStatusName = TimeSheetStatus.Manual.Description();
                            break;
                        case "S":
                            timeSheetActiveStatusName = TimeSheetStatus.Success.Description();
                            break;
                        case "A":
                            timeSheetActiveStatusName = TimeSheetStatus.Active.Description();
                            break;
                    }

                    timeSheet.timeSheetActiveStatusName = timeSheetActiveStatusName;
                    response.Add(timeSheet);
                }
                ;
                
                actionResultStatus.data = response;
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }
        public async Task<ActionResultStatus> SaveTimeSheet(SaveTimeSheetRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            TrTimeSheet trTimeSheet = new TrTimeSheet();
            try
            {
                bool result = false;

                var beforeActive = trTimeSheet.FindBy(a => a.EMP_NO == this.userAccess.empNo && a.TASK_CODE != model.taskCode && a.END_DATE == null).FirstOrDefault();
                if (beforeActive != null)
                {
                    beforeActive.END_DATE = DateTime.Now;
                    decimal hourTime = Math.Ceiling(((decimal)(DateTime.Now - beforeActive.START_DATE).TotalMinutes));
                    beforeActive.WORK_TIME = hourTime;
                    beforeActive.UPDATE_BY = this.userAccess.fullName;
                    beforeActive.UPDATE_DATE = DateTime.Now;
                    beforeActive.TIME_SHEET_ACTIVE = TimeSheetStatus.Success.Value();
                    trTimeSheet.Edit("", beforeActive);
                }

                var trTimeSheetData = trTimeSheet.Save(model, this.userAccess, out result);
                model.timeSheetId = trTimeSheetData.TIME_SHEET_ID;
                actionResultStatus.data = model;
            }
            catch (Exception ex)
            {
                actionResultStatus.success = false;
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> GetTimeSheetActiveByUserId()
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            GetTimeSheetActiveByUserIdResponse response = new GetTimeSheetActiveByUserIdResponse();
            TrTimeSheet trTimeSheet = new TrTimeSheet();
            try
            {
                trTimeSheet.Data = trTimeSheet.GetDataActive(this.userAccess.empNo);
                if (trTimeSheet.Data != null)
                {
                    response.taskCode = trTimeSheet.Data.TASK_CODE;
                    response.timeSheetId = trTimeSheet.Data.TIME_SHEET_ID;
                    response.empNo = trTimeSheet.Data.EMP_NO;
                    response.startDate = trTimeSheet.Data.START_DATE;
                    response.endDate = trTimeSheet.Data.END_DATE;
                    response.workTime = trTimeSheet.Data.WORK_TIME;
                }

                actionResultStatus.data = response;
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> SaveTask(SaveTaskRequest request)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            TrTask trTask = new TrTask();
            TrAsignTo trAsignTo = new TrAsignTo();
            
            //TrTaskDocument trTaskDocument = new TrTaskDocument();
            //TrTaskDocumentComment trTaskDocumentComment = new TrTaskDocumentComment();
            try
            {

                bool resultStatusTrTask = false;
                var trTaskData = trTask.Save(request, this.userAccess, out resultStatusTrTask);
                if (resultStatusTrTask)
                {
                    foreach (var trAsignToData in request.assignList)
                    {
                        bool resultStatusTrAsignTo = false;
                        trAsignToData.taskCode = trTaskData.TASK_CODE;
                        trAsignTo.Save(trAsignToData, this.userAccess, out resultStatusTrAsignTo);



                        if (trAsignToData.isSendNotify == StatusYesNo.Yes.Value())
                        {
                            SaveNotiMsgRequest notifyMsg = new SaveNotiMsgRequest();
                            NotifyBL notifyBL = new NotifyBL();
                            MtNotifyConfig mstNotiConfig = new MtNotifyConfig();
                            TrNotifyMsg trNotiMsg = new TrNotifyMsg();
                            MT_NOTIFY_CONFIG noitConfig = mstNotiConfig.GetDataByCode("NOT003");
                            // save notifyMsg

                            notifyMsg.notifyMsgId = 0;
                            notifyMsg.createBy = userAccess.userName;
                            notifyMsg.empNo = trAsignToData.empNo;
                            notifyMsg.notifyDesc = "คุณถูกมอบหมายให้ทำงานภายใต้ Job : " + trTaskData.JOB_NO + " Due date : " + trTaskData.DUE_DATE + " โดย : " + this.userAccess.fullName;
                            notifyMsg.refKey = "JOB_TASK_CODE";
                            notifyMsg.refTable = nameof(TR_TASK);
                            notifyMsg.refValue = trTaskData.TASK_CODE;
                            notifyMsg.notifyHeader = noitConfig.NOTIFY_CONFIG_HEADER_TEMPLATE + " " + trTaskData.JOB_NO;
                            notifyMsg.isActive = "Y";
                            notifyMsg.notifyMsgCode = trNotiMsg.getNotifyMsgRunning($"{noitConfig.NOTIFY_CONFIG_CODE}{DateTime.Now.ToString("yy")}{DateTime.Now.ToString("MM")}{DateTime.Now.ToString("dd")}", 3);
                            notifyMsg.notifyTopicCode = noitConfig.NOTIFY_TOPIC_CODE;
                            notifyMsg.url = $"{noitConfig.URL}{trTaskData.JOB_NO}";
                            notifyMsg.notifyGroupCode = "NOG003";
                         
                            notifyBL.SaveNotifyMsg(notifyMsg);
                        }
                    }

                }
                request.taskCode = trTaskData.TASK_CODE;
                actionResultStatus.data = request;
            }
            catch (Exception ex)
            {
                actionResultStatus.success = false;
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> SaveTaskList(List<SaveTaskRequest> request)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            TrTask trTask = new TrTask();
            TrAsignTo trAsignTo = new TrAsignTo();
            //TrTaskDocument trTaskDocument = new TrTaskDocument();
            //TrTaskDocumentComment trTaskDocumentComment = new TrTaskDocumentComment();
            try
            {
                foreach (var item in request)
                {
                    bool resultStatusTrTask = false;
                    var trTaskData = trTask.Save(item, this.userAccess, out resultStatusTrTask);
                    if (resultStatusTrTask)
                    {
                        foreach (var trAsignToData in item.assignList)
                        {
                            bool resultStatusTrAsignTo = false;
                            trAsignToData.taskCode = trTaskData.TASK_CODE;
                            trAsignTo.Save(trAsignToData, this.userAccess, out resultStatusTrAsignTo);
                        }

                    }
                    item.taskCode = trTaskData.TASK_CODE;
                }
                
                actionResultStatus.data = request;
            }
            catch (Exception ex)
            {
                actionResultStatus.success = false;
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> GetTaskDetails(GetTaskDetailsRequest request)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            SaveTaskRequest response = new SaveTaskRequest();
            TrTask trTask = new TrTask();
            TrAsignTo trAsignTo = new TrAsignTo();

            try
            {
                trTask.Data = trTask.GetDataByTaskCode(request.taskCode);
                if (trTask.Data != null)
                {

                    response.compCode = trTask.Data.COMP_CODE;
                    response.dueDate = trTask.Data.DUE_DATE;
                    response.estimated = (int)trTask.Data.ESTIMATED;
                    response.jobNo = trTask.Data.JOB_NO;
                    response.note = trTask.Data.NOTE;
                    response.planEndDate = trTask.Data.PLAN_END_DATE;
                    response.planStartDate = trTask.Data.PLAN_START_DATE;
                    response.priorityCode = trTask.Data.PRIORITY_CODE;
                    response.projectCode = trTask.Data.PROJECT_CODE;
                    response.taskCode = trTask.Data.TASK_CODE;
                    response.taskName = trTask.Data.TASK_NAME;
                    response.taskGroupCode = trTask.Data.TASK_GROUP_CODE;
                    response.taskStatusCode = trTask.Data.TASK_STATUS_CODE;
                    response.assignList = new List<SaveTaskAssignDatas>();

                    trAsignTo.Datas = trAsignTo.GetDatas(request.taskCode);
                    foreach (var trAsignToData in trAsignTo.Datas)
                    {
                        SaveTaskAssignDatas saveTaskAssignDatas = new SaveTaskAssignDatas();
                        saveTaskAssignDatas.empNo = trAsignToData.EMP_NO;
                        saveTaskAssignDatas.taskCode = trAsignToData.TASK_CODE;
                        saveTaskAssignDatas.isActive = trAsignToData.IS_ACTIVE;

                        response.assignList.Add(saveTaskAssignDatas);
                    }

                    actionResultStatus.data = response;
                }


            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> GetSummaryTaskList(GetSummaryTaskListRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtCompany mtComp = new MtCompany();
            MtProjectH mtProject = new MtProjectH();
            MtProjectD trJob = new MtProjectD();
            TrTask trTask = new TrTask();
            TrTimeSheet trTimesheet = new TrTimeSheet();
            MtConfigD mtConfigD = new MtConfigD();
            MtEmployee mtEmp = new MtEmployee();
            TrAsignTo trAssginTo = new TrAsignTo();



            try
            {
                var dataJob = trJob.GetTrJob(model);
                var dataCompany = mtComp.GetAll().ToList().Where(r => dataJob.Select(e => e.COMP_CODE).Distinct().ToList().Contains(r.COMP_CODE)).ToList();
                var dataProject = mtProject.GetAll().ToList().Where(r => dataJob.Select(e => e.PROJECT_CODE).Distinct().ToList().Contains(r.PROJECT_CODE)).ToList();

                var dataTask = trTask.GetDataByJobNo(dataJob.Select(e => e.JOB_NO).Distinct().ToList());
                var dataTimesheet = trTimesheet.GetDataByTaskCodesAsync(dataTask.Select(r => r.TASK_CODE).ToList());

                var dataConfigD = mtConfigD.GetAll().ToList().Where(e => dataTask.Select(r => r.TASK_STATUS_CODE).Distinct().ToList().Contains(e.CONFIG_D_CODE) && e.CONFIG_H_CODE == "TASK_STATUS").Distinct().ToList();
                var dataAssginTo = trAssginTo.GetDatasByUserIdList(new List<string>(), dataTask.Select(r => r.TASK_CODE).ToList());
                var dataEmp = mtEmp.GetAll().ToList().Where(r => dataAssginTo.Select(e => e.EMP_NO).ToList().Contains(r.EMP_NO)).ToList();

                var dataTaskTimesheet = (from a in dataTask
                                         join b in dataTimesheet on a.TASK_CODE equals b.TASK_CODE into bb
                                         from b in bb.DefaultIfEmpty()
                                         join c in dataAssginTo on a.TASK_CODE equals c.TASK_CODE
                                         join g in dataEmp on c.EMP_NO equals g.EMP_NO
                                         //where b.EMP_NO == userAccess.empNo || userAccess.teamRoleList.Select(r => r.empNo).ToList().Contains(b.EMP_NO)
                                         group new { a, b, g } by new { a.TASK_CODE, a.JOB_NO, a.PLAN_END_DATE, a.PLAN_START_DATE, a.ESTIMATED, a.TASK_NAME, a.TASK_STATUS_CODE, c.EMP_NO, g.FIRST_NAME, g.LAST_NAME } into grp
                                         select new GetSummaryTaskListResponse
                                         {
                                             jobNo = grp?.Key?.JOB_NO,
                                             empNo = grp?.Key?.EMP_NO,

                                             empFullName = grp?.Key?.FIRST_NAME + ' ' + grp?.Key?.LAST_NAME,
                                             endDate = grp.Key?.PLAN_END_DATE,
                                             estimated = grp?.Key?.ESTIMATED ?? 0,
                                             startDate = grp?.Key?.PLAN_START_DATE,
                                             taskCode = grp?.Key?.TASK_CODE,
                                             taskName = grp?.Key?.TASK_NAME,
                                             taskStatusCode = grp?.Key?.TASK_STATUS_CODE,
                                             taskStatusName = dataConfigD.Where(r => r.CONFIG_D_CODE == grp?.Key?.TASK_STATUS_CODE).Select(r => r.CONFIG_D_ENAME).FirstOrDefault(),
                                             totalWorkingTime = grp != null ? grp?.Sum(r => r?.b != null ? r?.b?.WORK_TIME : 0) : 0
                                         }).ToList();

                var dataTaskTimeSheetGroup = (from a in dataTaskTimesheet
                                              group a by a.taskCode into g
                                              select new GetSummaryTaskListResponse
                                              {
                                                  taskCode = g.Key,
                                                  totalWorkingTime = g.Select(r => r.totalWorkingTime).Sum()
                                              }).ToList();



                //var dataTempTimesheet = (from a in dataTask
                //                         join b in dataTimesheet on a.TASK_CODE equals b.TASK_CODE into bb
                //                         from b in bb.DefaultIfEmpty()
                //                         join g in dataEmp on b.EMP_NO equals g.EMP_NO into emp
                //                         from g in emp.DefaultIfEmpty()
                //                         where b.EMP_NO == userAccess.empNo || userAccess.teamRoleList.Select(r => r.empNo).ToList().Contains(b.EMP_NO)
                //                         group new { a, b, g } by new { a.TASK_CODE } into grp
                //                         select new GetSummaryTaskListResponse
                //                         {
                //                             //compCode = grp.FirstOrDefault().d?.COMP_CODE,
                //                             //compName = grp.FirstOrDefault().d?.COMP_NAME,
                //                             //empNo =  grp.FirstOrDefault() != null ? grp.FirstOrDefault()?.b?.EMP_NO : null,
                //                             //empFullName =  grp.FirstOrDefault().g?.FIRST_NAME + " " + grp.FirstOrDefault().g?.LAST_NAME,

                //                             jobNo = grp?.FirstOrDefault()?.a?.JOB_NO,

                //                             //empNo = grp?.FirstOrDefault() != null ? grp?.FirstOrDefault()?.b?.EMP_NO : null,
                //                             empFullName = grp?.Select(r => r?.g?.FIRST_NAME + " " + r?.g?.LAST_NAME)?.FirstOrDefault(),
                //                             endDate = grp?.FirstOrDefault()?.a?.PLAN_END_DATE,
                //                             estimated = grp?.FirstOrDefault()?.a?.ESTIMATED ?? 0,
                //                             startDate = grp?.FirstOrDefault()?.a?.PLAN_START_DATE,
                //                             taskCode = grp?.FirstOrDefault()?.a?.TASK_CODE,
                //                             taskName = grp?.FirstOrDefault()?.a?.TASK_NAME,
                //                             taskStatusCode = grp?.FirstOrDefault()?.a?.TASK_STATUS_CODE,
                //                             totalWorkingTime = grp?.Sum(r => r?.b != null ? r?.b?.WORK_TIME : 0)
                //                         }).ToList();
                dataConfigD = mtConfigD.GetAll().ToList().Where(e => dataJob.Select(r => r.JOB_STATUS_CODE).Distinct().ToList().Contains(e.CONFIG_D_CODE) && e.CONFIG_H_CODE == "JOB_STATUS").Distinct().ToList();
                var result = (from a in dataTaskTimesheet
                              join b in dataTaskTimeSheetGroup on a.taskCode equals b.taskCode
                              join c in dataJob on a.jobNo equals c.JOB_NO into cc
                              from c in cc.DefaultIfEmpty()
                              join d in dataCompany on c.COMP_CODE equals d.COMP_CODE into dd
                              from d in dd.DefaultIfEmpty()
                              join e in dataProject on c.PROJECT_CODE equals e.PROJECT_CODE into ee
                              from e in ee.DefaultIfEmpty()
                              join f in dataConfigD on a.taskStatusCode equals f.CONFIG_D_CODE into ff
                              from f in ff.DefaultIfEmpty()
                              group new { a, b, c, d, e, f } by new
                              {
                                  a.jobNo,
                                  a.taskCode,
                                  a.taskName,
                                  a.taskStatusCode,
                                  a.startDate,
                                  a.endDate,
                                  a.estimated,
                                  b.totalWorkingTime,
                                  //c.JOB_NAME,
                                  //c.JOB_STATUS_CODE,
                                  //c.JOB_YEAR,
                                  //d.COMP_NAME,
                                  //d.COMP_CODE,
                                  //e.PROJECT_CODE,
                                  //e.PROJECT_NAME,
                                  //f.CONFIG_D_ENAME

                              } into g
                              select new GetSummaryTaskListResponse
                              {
                                  compCode = g?.Select(r => r.d?.COMP_CODE).FirstOrDefault(),
                                  compName = g?.Select(r => r.d?.COMP_NAME).FirstOrDefault(),
                                  empFullName = string.Join(", ", g?.Select(r => r?.a?.empFullName).Distinct().ToList()),
                                  empNo = string.Join(", ", g?.Select(r => r?.a?.empNo).Distinct().ToList()),
                                  empFullNameList = g?.Select(r => r?.a?.empFullName).ToList(),
                                  empNoList = g?.Select(r => r?.a?.empNo).ToList(),
                                  endDate = g?.Key?.endDate,
                                  estimated = g?.Key?.estimated,
                                  jobName = g?.Select(r => r.c.JOB_NAME).FirstOrDefault(),
                                  jobNo = g?.Key?.jobNo,
                                  jobStatusCode = g?.Select(r => r.c?.JOB_STATUS_CODE).FirstOrDefault(),
                                  jobStatusName = dataConfigD.Where(r => r.CONFIG_D_CODE == g?.Select(e => e.c?.JOB_STATUS_CODE).FirstOrDefault()).Select(r => r.CONFIG_D_ENAME).FirstOrDefault(),
                                  jobYear = g?.Select(r => r.c?.JOB_YEAR).FirstOrDefault(),
                                  projectCode = g?.Select(r => r.e?.PROJECT_CODE).FirstOrDefault(),
                                  projectName = g?.Select(r => r.e?.PROJECT_NAME).FirstOrDefault(),
                                  startDate = g?.Key?.startDate,
                                  taskCode = g?.Key?.taskCode,//g?.Select(r => r?.a?.taskCode).FirstOrDefault(),
                                  taskStatusName = g?.Select(r => r?.a?.taskStatusName).FirstOrDefault(),
                                  taskName = g?.Key?.taskName,
                                  taskStatusCode = g?.Key?.taskStatusCode,
                                  totalWorkingTime = g?.Key?.totalWorkingTime
                              }).ToList();



                //var result = (from a in dataTaskTimesheet
                //              join c in dataJob on a.jobNo equals c.JOB_NO into cc
                //              from c in cc.DefaultIfEmpty()
                //              join d in dataCompany on c.COMP_CODE equals d.COMP_CODE into dd
                //              from d in dd.DefaultIfEmpty()
                //              join e in dataProject on c.PROJECT_CODE equals e.PROJECT_CODE into ee
                //              from e in ee.DefaultIfEmpty()
                //              join f in dataConfigD on a.taskStatusCode equals f.CONFIG_D_CODE into ff
                //              from f in ff.DefaultIfEmpty()
                //                  //join g in dataEmp on a.empNo equals g.EMP_NO into emp
                //                  //from g in emp.DefaultIfEmpty()
                //                  //where a.empNo == userAccess.empNo || userAccess.teamRoleList.Select(r => r.empNo).ToList().Contains(a.empNo)
                //              select new GetSummaryTaskListResponse
                //              {
                //                  compCode = d?.COMP_CODE,
                //                  compName = d?.COMP_NAME,
                //                  //empNo = a?.empNo,
                //                  empFullName = a?.empFullName,
                //                  endDate = a?.endDate,
                //                  estimated = a?.estimated ?? 0,
                //                  jobNo = c?.JOB_NO,
                //                  jobName = c?.JOB_NAME,
                //                  jobStatusCode = c?.JOB_STATUS_CODE,
                //                  jobStatusName = f?.CONFIG_D_ENAME,
                //                  jobYear = c?.JOB_YEAR,
                //                  projectCode = e?.PROJECT_CODE,
                //                  projectName = e?.PROJECT_NAME,
                //                  startDate = a?.startDate,
                //                  taskCode = a?.taskCode,
                //                  taskName = a?.taskName,
                //                  taskStatusCode = a?.taskStatusCode,
                //                  totalWorkingTime = a?.totalWorkingTime
                //              }).Distinct().ToList();
                actionResultStatus.data = result;
            }
            catch (Exception ex)
            {
                actionResultStatus.success = false;
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public async Task<ActionResultStatus> GetClientCareList(GetClientCareListRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            var clientCareListReponse = new List<GetClientCareListResponse>();

            try
            {
                MtEmployee mtEmployee = new MtEmployee();
                MtEmployeePosition mtEmployeePosition = new MtEmployeePosition();
                MtDepartment mtDepartment = new MtDepartment();
                //MtOrgPosition mtOrgPosition = new MtOrgPosition();
                MtPosition mtPosition = new MtPosition();
                MtCompany mtCompany = new MtCompany();
                TrCompanyHeadCs trCompanyHeadCs = new TrCompanyHeadCs();
                // MtTeamRole mtTeamRole = new MtTeamRole();

                var dataCompanyList = mtCompany.GetAll().Where(r => r.IS_ACTIVE == "Y").Select(r => new GetClientList { compCode = r.COMP_CODE, compName = r.COMP_NAME }).ToList();
                mtDepartment.Datas = mtDepartment.GetAll().ToList();
                mtPosition.Datas = mtPosition.GetAll().ToList();


                List<MT_EMPLOYEE> dataEmployee = new List<MT_EMPLOYEE>();

                //var dataOrgPosition = mtOrgPosition.GetDatasByDeptCode(model.deptCodeList);
                var dataEmployeePosition = mtEmployeePosition.GetDatasByDeptCodeList(model.deptCodeList).ToList();


                dataEmployee = mtEmployee.GetDatasByEmpNoList(dataEmployeePosition.Select(r => r.EMP_NO).ToList());
                var tempTrCompanyHeadCs = trCompanyHeadCs.GetDatasActive();

                dataEmployee = (from a in dataEmployee
                                join b in tempTrCompanyHeadCs on a.EMP_NO equals b.EMP_NO into bb
                                from b in bb.DefaultIfEmpty()
                                where b == null
                                select a
                             ).ToList();

                clientCareListReponse = (from a in dataEmployee
                                         join b in dataEmployeePosition on a.EMP_NO equals b.EMP_NO
                                         join c in mtDepartment.Datas on b.DEPT_CODE equals c.DEPT_CODE
                                         join d in mtPosition.Datas on b.POSITION_CODE equals d.POSITION_CODE
                                         select new GetClientCareListResponse
                                         {
                                             deptCode = b.DEPT_CODE,
                                             deptName = c.DEPT_NAME,
                                             empName = a.FIRST_NAME + " " + a.LAST_NAME,
                                             empNo = a.EMP_NO,
                                             positionCode = b.POSITION_CODE,
                                             positionName = d.POSITION_NAME,
                                             companyList = dataCompanyList
                                         }).ToList();



                var dataTrCompanyHeadCs = (from a in tempTrCompanyHeadCs
                                           group a by a.EMP_NO into g
                                           select new TrCompanyHeadCsGroupEmp
                                           {
                                               empNo = g.Key,
                                               companyCodeList = g.Select(r => r.COMP_CODE).ToList()
                                           }).ToList();
                var clientCareListReponse2 = new List<GetClientCareListResponse>();

                //var data2 = (from a in dataTrCompanyHeadCs
                //             join b in clientCareListReponse on a.empNo equals b.empNo into bb
                //             from b in bb.DefaultIfEmpty()
                //             where b?.empNo == null
                //             select b).ToList();
                var tempList = new List<GetClientCareListResponse>();
                foreach (var item in dataTrCompanyHeadCs)
                {
                    var employeeList = mtEmployee.GetDataByUserId(item.empNo);

                    var companyList = dataCompanyList.Where(r => item.companyCodeList.Contains(r.compCode)).Select(r => new GetClientList
                    { compCode = r.compCode, compName = r.compName }).ToList();

                    var data = (from a in employeeList
                                    //join b in dataEmployeePosition on a.EMP_NO equals b.EMP_NO into bb
                                    //from b in bb.DefaultIfEmpty()
                                    //join c in mtDepartment.Datas on b.DEPT_CODE equals c.DEPT_CODE into cc
                                    //from c in cc.DefaultIfEmpty()
                                    //join d in mtPosition.Datas on b.POSITION_CODE equals d.POSITION_CODE into dd
                                    //from d in dd.DefaultIfEmpty()
                                select new GetClientCareListResponse
                                {
                                    //deptCode = b.DEPT_CODE,
                                    //deptName = c.DEPT_NAME,
                                    empName = a.FIRST_NAME + " " + a.LAST_NAME,
                                    empNo = a.EMP_NO,
                                    //positionCode = b.POSITION_CODE,
                                    //positionName = d.POSITION_NAME,
                                    companyList = companyList
                                }).Distinct().ToList();

                    tempList.AddRange(data);
                }
                clientCareListReponse.AddRange(tempList);
                actionResultStatus.data = clientCareListReponse;
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public List<GetClientCareListResponse> GetClientCareList(List<string> deptCodeList)
        {
            var clientCareListReponse = new List<GetClientCareListResponse>();

            MtEmployee mtEmployee = new MtEmployee();
            MtEmployeePosition mtEmployeePosition = new MtEmployeePosition();
            MtDepartment mtDepartment = new MtDepartment();
            //MtOrgPosition mtOrgPosition = new MtOrgPosition();
            MtPosition mtPosition = new MtPosition();
            MtCompany mtCompany = new MtCompany();
            TrCompanyHeadCs trCompanyHeadCs = new TrCompanyHeadCs();
            // MtTeamRole mtTeamRole = new MtTeamRole();

            var dataCompanyList = mtCompany.GetAll().Where(r => r.IS_ACTIVE == "Y").Select(r => new GetClientList { compCode = r.COMP_CODE, compName = r.COMP_NAME }).ToList();
            mtDepartment.Datas = mtDepartment.GetAll().ToList();
            mtPosition.Datas = mtPosition.GetAll().ToList();


            List<MT_EMPLOYEE> dataEmployee = new List<MT_EMPLOYEE>();

            //var dataOrgPosition = mtOrgPosition.GetDatasByDeptCode(model.deptCodeList);
            var dataEmployeePosition = mtEmployeePosition.GetDatasByDeptCodeList(deptCodeList).ToList();


            dataEmployee = mtEmployee.GetDatasByEmpNoList(dataEmployeePosition.Select(r => r.EMP_NO).ToList());
            var tempTrCompanyHeadCs = trCompanyHeadCs.GetDatasActive();

            dataEmployee = (from a in dataEmployee
                            join b in tempTrCompanyHeadCs on a.EMP_NO equals b.EMP_NO into bb
                            from b in bb.DefaultIfEmpty()
                            where b == null
                            select a
                         ).ToList();

            clientCareListReponse = (from a in dataEmployee
                                     join b in dataEmployeePosition on a.EMP_NO equals b.EMP_NO
                                     join c in mtDepartment.Datas on b.DEPT_CODE equals c.DEPT_CODE
                                     join d in mtPosition.Datas on b.POSITION_CODE equals d.POSITION_CODE
                                     select new GetClientCareListResponse
                                     {
                                         deptCode = b.DEPT_CODE,
                                         deptName = c.DEPT_NAME,
                                         empName = a.FIRST_NAME + " " + a.LAST_NAME,
                                         empNo = a.EMP_NO,
                                         positionCode = b.POSITION_CODE,
                                         positionName = d.POSITION_NAME,
                                         companyList = dataCompanyList
                                     }).ToList();



            var dataTrCompanyHeadCs = (from a in tempTrCompanyHeadCs
                                       group a by a.EMP_NO into g
                                       select new TrCompanyHeadCsGroupEmp
                                       {
                                           empNo = g.Key,
                                           companyCodeList = g.Select(r => r.COMP_CODE).ToList()
                                       }).ToList();
            var clientCareListReponse2 = new List<GetClientCareListResponse>();

            //var data2 = (from a in dataTrCompanyHeadCs
            //             join b in clientCareListReponse on a.empNo equals b.empNo into bb
            //             from b in bb.DefaultIfEmpty()
            //             where b?.empNo == null
            //             select b).ToList();
            var tempList = new List<GetClientCareListResponse>();
            foreach (var item in dataTrCompanyHeadCs)
            {
                var employeeList = mtEmployee.GetDataByUserId(item.empNo);

                var companyList = dataCompanyList.Where(r => item.companyCodeList.Contains(r.compCode)).Select(r => new GetClientList
                { compCode = r.compCode, compName = r.compName }).ToList();

                var data = (from a in employeeList
                                //join b in dataEmployeePosition on a.EMP_NO equals b.EMP_NO into bb
                                //from b in bb.DefaultIfEmpty()
                                //join c in mtDepartment.Datas on b.DEPT_CODE equals c.DEPT_CODE into cc
                                //from c in cc.DefaultIfEmpty()
                                //join d in mtPosition.Datas on b.POSITION_CODE equals d.POSITION_CODE into dd
                                //from d in dd.DefaultIfEmpty()
                            select new GetClientCareListResponse
                            {
                                //deptCode = b.DEPT_CODE,
                                //deptName = c.DEPT_NAME,
                                empName = a.FIRST_NAME + " " + a.LAST_NAME,
                                empNo = a.EMP_NO,
                                //positionCode = b.POSITION_CODE,
                                //positionName = d.POSITION_NAME,
                                companyList = companyList
                            }).Distinct().ToList();

                tempList.AddRange(data);
            }
            clientCareListReponse.AddRange(tempList);
            return clientCareListReponse;
        }
    }
}