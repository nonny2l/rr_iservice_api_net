﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormAPI.Models.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using RRPlatFormAPI.HTTP.Request.Master;
using RRPlatFormAPI.Models.RepositoryModel.Master;
using RRPlatFormAPI.HTTP.Response.Master;

namespace RRPlatFormAPI.BussinessLogic
{
    public class MasterBL : AbBLRepository
    {
        public UserAccess userAccess;
        public ActionResultStatus GetMasterList(GetMasterListRequest request)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            MtConficH mtConficH = new MtConficH();
            MtConficD mtConficD = new MtConficD();
            try
            {
                List<GetMasterListResponse> responseRecursive = new List<GetMasterListResponse>();
                List<GetMasterListResponse> responseResponse = new List<GetMasterListResponse>();
                mtConficH.Data = mtConficH.GetDataByCode(request.configHCode);
                if (mtConficH.Data != null)
                {
                    mtConficD.Datas = mtConficD.GetDatas(mtConficH.Data.CONFIG_H_ID.ToString());
                    foreach (var mtConficDData in mtConficD.Datas)
                    {
                        MtConficDDto dataDto = mtConficD.BindDataDto(mtConficDData);
                        string dataDtoString = JsonConvert.SerializeObject(dataDto);
                        GetMasterListResponse responseData = JsonConvert.DeserializeObject<GetMasterListResponse>(dataDtoString);
                        responseRecursive.Add(responseData);
                    }

                    var dataChild = mtConficD.GetAll().ToList().Where(r => responseRecursive.Select(e => e.mtConfigDId).Contains(r.PARENT_D_ID.ToString())).Select(r => new GetMasterListResponse
                    {
                        mtConfigDId = r.CONFIG_D_ID.ToString(),
                        mtConfigDCode = r.CONFIG_D_CODE,
                        mtConfigHCode = r.CONFIG_H_CODE,
                        mtConfigDSeq = r.CONFIG_D_SEQ,
                        mtConfigDTName = r.CONFIG_D_TNAME,
                        mtConfigDEName = r.CONFIG_D_ENAME,
                        parentDId = r.PARENT_D_ID.ToString(),
                        isActive = r.IS_ACTIVE,
                        createBy = r.CREATE_BY,
                        createDate = r.CREATE_DATE,
                        updateBy = r.UPDATE_BY,
                        updateDate = r.UPDATE_DATE,
                    }).ToList();
                    //var dataChild = mtConficD.BindDataDto(tempDataChild);
                    responseResponse = responseRecursive.Where(c => c.isActive == "Y")
                    .Select(dataBind => new GetMasterListResponse
                    {
                        mtConfigDId = dataBind.mtConfigDId,
                        mtConfigDCode = dataBind.mtConfigDCode,
                        mtConfigHCode = dataBind.mtConfigHCode,
                        mtConfigDSeq = dataBind.mtConfigDSeq,
                        mtConfigDTName = dataBind.mtConfigDTName,
                        mtConfigDEName = dataBind.mtConfigDEName,
                        parentDId = dataBind.parentDId,
                        isActive = dataBind.isActive,
                        createBy = dataBind.createBy,
                        createDate = dataBind.createDate,
                        updateBy = dataBind.updateBy,
                        updateDate = dataBind.updateDate,
                        child = mtConficD.GetChildParent(dataChild, dataBind.mtConfigDId)

                    }).ToList();

                    mReturn.data = responseResponse;

                }
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }
        public ActionResultStatus GetMstProvinceList(ProvinceRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            try
            {
                MtProvince prov = new MtProvince();
                var data = prov.GetDatas(string.Empty);
                actionResultStatus.data = data.Select(r => new ProvinceResponse
                {
                    provCode = r.PROV_CODE,
                    provEName = r.PROV_ENAME,
                    provTName = r.PROV_TNAME,
                    provRegion = r.PROV_REGION,
                    isActive = r.IS_ACTIVE
                }).ToList();

            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.Exception;
                actionResultStatus.success = false;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.ErrorException();
            }
            return actionResultStatus;
        }
        public ActionResultStatus GetMstProvinceGroupRegionList(ProvinceRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            try
            {
                MtProvince prov = new MtProvince();
                actionResultStatus.data = prov.GetAllGrouped();
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.Exception;
                actionResultStatus.success = false;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.ErrorException();
            }
            return actionResultStatus;
        }
        public ActionResultStatus GetMstDistrictList(DistrictRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            try
            {
                MtDistrict district = new MtDistrict();
                var data = district.GetDatasByProvinceCode(model.provCode);
                actionResultStatus.data = data.Select(r => new DistrictResponse
                {
                    districtCode = r.DISTRICT_CODE,
                    districtEName = r.DISTRICT_ENAME,
                    districtTName = r.DISTRICT_TNAME,
                    isActive = r.IS_ACTIVE,
                    reffProvCode = r.REF_PROV_CODE
                }).ToList();

            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.Exception;
                actionResultStatus.success = false;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.ErrorException();
            }
            return actionResultStatus;
        }
        public ActionResultStatus GetMstSubDistrictList(SubDistrictRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            try
            {
                MtSubDistrict district = new MtSubDistrict();
                var data = district.GetDatasByDistrictCode(model.districtCode);
                actionResultStatus.data = data.Select(r => new SubDistrictResponse
                {
                    subDistrictCode = r.SUB_DISTRICT_CODE,
                    subDdistrictEName = r.SUB_DISTRICT_ENAME,
                    subDistrictTName = r.SUB_DISTRICT_TNAME,
                    isActive = r.IS_ACTIVE,
                    reffDistrictCode = r.REF_DISTRICT_CODE,
                    zipcode = r.ZIPCODE
                }).ToList();

            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.Exception;
                actionResultStatus.success = false;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.ErrorException();
            }
            return actionResultStatus;
        }
        
        public ActionResultStatus GeDepartmentList(GeDepartmentListRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtDepartment mtDepartment = new MtDepartment();
            List<GeDepartmentListResponse> responseList = new List<GeDepartmentListResponse>();
            try
            {
                mtDepartment.Datas = mtDepartment.GetAll().ToList();
                foreach (var mtDepartmentData in mtDepartment.Datas)
                {
                    GeDepartmentListResponse dataResponse = new GeDepartmentListResponse();
                    dataResponse.deptId = mtDepartmentData.DEPT_ID;
                    dataResponse.deptCode = mtDepartmentData.DEPT_CODE;
                    dataResponse.deptName = mtDepartmentData.DEPT_NAME;
                    dataResponse.isActive = mtDepartmentData.IS_ACTIVE;
                    responseList.Add(dataResponse);
                }

                actionResultStatus.data = responseList.OrderBy(a => a.deptName).ToList();
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public ActionResultStatus GetEstimationTimeList(GetEstimationTimeListRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtEstimateTime mtEstimateTime = new MtEstimateTime();
            List<GetEstimationTimeListResponse> responseList = new List<GetEstimationTimeListResponse>();
            try
            {
                mtEstimateTime.Datas = mtEstimateTime.GetAll().ToList();
                foreach (var mtDepartmentData in mtEstimateTime.Datas)
                {
                    GetEstimationTimeListResponse dataResponse = new GetEstimationTimeListResponse();
                    dataResponse.estimatedId = mtDepartmentData.ESTIMATED_ID;
                    dataResponse.estimatedCode = mtDepartmentData.ESTIMATED_CODE;
                    dataResponse.estimatedName = mtDepartmentData.ESTIMATED_NAME;
                    dataResponse.isActive = mtDepartmentData.IS_ACTIVE;
                    responseList.Add(dataResponse);
                }

                actionResultStatus.data = responseList;
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public ActionResultStatus GetPriorityList(GetPriorityListRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtPriority mtPriority = new MtPriority();
            List<GetPriorityListResponse> responseList = new List<GetPriorityListResponse>();
            try
            {
                mtPriority.Datas = mtPriority.GetAll().ToList();
                foreach (var mtPriorityData in mtPriority.Datas)
                {
                    GetPriorityListResponse dataResponse = new GetPriorityListResponse();
                    dataResponse.priorityId = mtPriorityData.PRIORITY_ID;
                    dataResponse.priorityCode = mtPriorityData.PRIORITY_CODE;
                    dataResponse.priorityName = mtPriorityData.PRIORITY_NAME;
                    dataResponse.isActive = mtPriorityData.IS_ACTIVE;

                    responseList.Add(dataResponse);
                }

                actionResultStatus.data = responseList;
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public ActionResultStatus GetTaskGroupList(GetTaskGroupListRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtTaskGroup mtTaskGroup = new MtTaskGroup();
            List<GetTaskGroupListesponse> responseList = new List<GetTaskGroupListesponse>();
            try
            {
                mtTaskGroup.Datas = mtTaskGroup.GetAll().ToList();
                foreach (var mtTaskGroupData in mtTaskGroup.Datas)
                {
                    GetTaskGroupListesponse dataResponse = new GetTaskGroupListesponse();
                    dataResponse.taskGroupId = mtTaskGroupData.TASK_GROUP_ID;
                    dataResponse.taskGroupName = mtTaskGroupData.TASK_GROUP_NAME;
                    dataResponse.taskGroupCode = mtTaskGroupData.TASK_GROUP_CODE;
                    dataResponse.isActive = mtTaskGroupData.IS_ACTIVE;
                    dataResponse.seq = mtTaskGroupData.SEQ;
                    responseList.Add(dataResponse);
                }

                actionResultStatus.data = responseList.OrderBy(a => a.seq ).ToList();
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public ActionResultStatus GetMasterTaskStatusList(GetMasterTaskStatusListRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            //MtTaskStatus mtTaskStatus = new MtTaskStatus();
            List<GetMasterTaskStatusListResponse> responseList = new List<GetMasterTaskStatusListResponse>();
            try
            {
                //mtTaskStatus.Datas = mtTaskStatus.GetAll().ToList();
                //foreach (var mtTaskStatusData in mtTaskStatus.Datas)
                //{
                //    GetMasterTaskStatusListResponse dataResponse = new GetMasterTaskStatusListResponse();
                //    dataResponse.taskStatusId = mtTaskStatusData.STATUS_ID;
                //    dataResponse.taskStatusName = mtTaskStatusData.STATUS_NAME;
                //    dataResponse.taskStatusCode = mtTaskStatusData.STATUS_CODE;
                //    dataResponse.isActive = mtTaskStatusData.IS_ACTIVE;
                //    dataResponse.seq = mtTaskStatusData.SEQ;

                //    responseList.Add(dataResponse);
                //}

                actionResultStatus.data = responseList.OrderBy(a => a.seq).ToList(); 
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public ActionResultStatus GetOrgPositionList(GetOrgPositionListRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtOrgPosition mtOrgPosition = new MtOrgPosition();
            List<GetOrgPositionListResponse> responseList = new List<GetOrgPositionListResponse>();
            try
            {
                if (model.deptCodeList.Count != 0)
                    mtOrgPosition.Datas = mtOrgPosition.GetAll().Where(a => model.deptCodeList.Contains(a.DEPT_CODE)).ToList();
                else
                    mtOrgPosition.Datas = mtOrgPosition.GetAll().ToList();


                foreach (var mtOrgPositionData in mtOrgPosition.Datas)
                {
                    GetOrgPositionListResponse dataResponse = new GetOrgPositionListResponse();
                    dataResponse.deptCode = mtOrgPositionData.DEPT_CODE;
                    dataResponse.deptName = MtDepartment.GetName(mtOrgPositionData.DEPT_CODE);
                    dataResponse.positionCode = mtOrgPositionData.POSITION_CODE;
                    //dataResponse.positionName = mtOrgPositionData.POSITION_NAME;
                    dataResponse.seq = mtOrgPositionData.SEQ;

                    responseList.Add(dataResponse);
                }

                actionResultStatus.data = responseList.OrderBy(a => a.seq).ToList();
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }

        public ActionResultStatus GetMasterConfigList(GetMasterConfigListRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            MtConficD mtConficD = new MtConficD();
            GetMasterConfigListResponse reponse = new GetMasterConfigListResponse();
            List<object> listResponse = new List<object>();
            try
            {
                mtConficD.Datas = mtConficD.GetDatasByHCode(model.configHCode);
                foreach (var mtConficDData in mtConficD.Datas)
                {
                    IDictionary<string, string> dictResponse = new Dictionary<string, string>();
                    string headerCode = mtConficDData.CONFIG_H_CODE.ToPascalFormat().ToLowerFirstChar();

                    dictResponse.Add(headerCode + nameof(reponse.HeaderCode),mtConficDData.CONFIG_H_CODE);
                    dictResponse.Add(headerCode + nameof(reponse.HeaderName),MtConficH.GetName(mtConficDData.CONFIG_H_CODE));
                    dictResponse.Add(headerCode + nameof(reponse.Code), mtConficDData.CONFIG_D_CODE);
                    dictResponse.Add(headerCode + nameof(reponse.TName), mtConficDData.CONFIG_D_TNAME);
                    dictResponse.Add(headerCode + nameof(reponse.EName), mtConficDData.CONFIG_D_ENAME);
                    dictResponse.Add(headerCode + nameof(reponse.Seq), mtConficDData.CONFIG_D_SEQ.ToString());

                    listResponse.Add(dictResponse);

                }
                actionResultStatus.data = listResponse ;
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = null;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }
            return actionResultStatus;
        }


        

    }
}