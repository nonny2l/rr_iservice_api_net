﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

[assembly: OwinStartup(typeof(RRPlatFormAPI.SignalR.Startup))]

namespace RRPlatFormAPI.SignalR
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Any connection or hub wire up and configuration should go here
            var hubConfig = new Microsoft.AspNet.SignalR.HubConfiguration();
            //hubConfig.EnableDetailedErrors = true;
            //hubConfig.EnableJavaScriptProxies = false;
            //hubConfig.EnableJSONP = true;
            app.MapSignalR("/signalr", hubConfig);
            //app.Map("/signalr", map =>
            //{
            //    map.UseCors(CorsOptions.AllowAll);
            //    map.RunSignalR(hubConfig);
            //});
        }
    }
}