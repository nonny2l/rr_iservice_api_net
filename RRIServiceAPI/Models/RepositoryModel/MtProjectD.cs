﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request.Customer;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormAPI.Models.Enum;
using RRPlatFormAPI.Models.RepositoryModel.Master;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class MtProjectD : AbRepository<TR_JOB>
    {
        public override TR_JOB GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.JOB_ID == idInt).FirstOrDefault();
        }

        public bool checkCanSandNotify(string jobNo, string jobStatusCode) {
            return this.FindBy(a => a.JOB_NO == jobNo && a.JOB_STATUS_CODE == "OP" && jobStatusCode == "AP").Any();
        }

        public TR_JOB GetDataByJobNo(string jobNo)
        {
            return this.FindBy(a => a.JOB_NO == jobNo).FirstOrDefault();
        }

        public List<TR_JOB> GetJobListFilter(GetJobListRequest request, UserAccess userAccess)
        {
            List<TR_JOB> result = new List<TR_JOB>();
            result = this.FindBy(a => a.IS_ACTIVE == "Y").ToList();

            if (request.compCodeList.Count != 0)
            {
                result = result.Where(a => request.compCodeList.Contains(a.COMP_CODE)).ToList();
            }

            if (request.projectCodeList.Count != 0)
            {
                result = result.Where(a => request.projectCodeList.Contains(a.PROJECT_CODE)).ToList();
            }

            if (!request.jobNo.IsNullOrEmptyWhiteSpace())
            {
                result = result.Where(a => a.JOB_NO.Contains(request.jobNo)).ToList();
            }

            if (request.jobStatusCodeList.Count != 0)
            {
                result = result.Where(a => request.jobStatusCodeList.Contains(a.JOB_STATUS_CODE)).ToList();
            }

            if (request.briefDateFrom != null && request.briefDateTo == null)
            {
                result = result.Where(a => a.BRIEF_DATE.Date >= ((DateTime)request.briefDateFrom).Date).ToList();
            }

            if (request.briefDateFrom == null && request.briefDateTo != null)
            {
                result = result.Where(a => a.BRIEF_DATE.Date <= ((DateTime)request.briefDateTo).Date).ToList();
            }

            if (request.briefDateFrom != null && request.briefDateTo != null)
            {
                result = result.Where(a => a.BRIEF_DATE.Date >= ((DateTime)request.briefDateFrom).Date
                                        && a.BRIEF_DATE.Date <= ((DateTime)request.briefDateTo).Date).ToList();
            }

            if (!request.jobYear.IsNullOrEmptyWhiteSpace())
            {
                result = result.Where(a => a.JOB_YEAR == request.jobYear).ToList();
            }

            List<string> deptCodeList = userAccess.teamRoleList.Select(a => a.deptCode).Distinct().ToList();
            MtConfigD mtConfigD = new MtConfigD();
            mtConfigD.Datas = mtConfigD.GetDatasActiveByConfigHCode(ConfigurationMaster.SpecialDepartment.Value());
            int countDept = mtConfigD.Datas.Where(a => deptCodeList.Contains(a.CONFIG_D_CODE)).ToList().Count;
            if (countDept == 0)
            {
                TrTeamRole trTeamRole = new TrTeamRole();
                List<string> teamCodeList = trTeamRole.GetDatasByEmpNo(userAccess.empNo).Select(a => a.TEAM_CODE).ToList();
                if (teamCodeList.Count != 0)
                {
                    TrTeam trTeam = new TrTeam();
                    List<string> jobCodeList = trTeam.FindBy(a => teamCodeList.Contains(a.TEAM_CODE)).Select(a => a.JOB_NO).ToList();
                    result = result.Where(a => jobCodeList.Contains(a.JOB_NO)).OrderByDescending(a => a.CREATE_DATE).ToList();
                }
                else
                {
                    result = new List<TR_JOB>();
                }
            }
            else
            {
                result.OrderByDescending(a => a.CREATE_DATE).ToList();
            }

            return result;

        }

        public TR_JOB GetDataCheckDupicate(int projectDId, string projectCode, string jobNo, string srvcCode, string subSrvcCode, string prodTypeCode)
        {
            return this.FindBy(a => a.JOB_ID == projectDId
                                 && a.PROJECT_CODE == projectCode
                                 && a.JOB_NO == jobNo
                                 && a.SERVICE_CODE == srvcCode
                                 && a.SUB_SERVICE_CODE == subSrvcCode
                                 ).FirstOrDefault();
        }

        public override List<TR_JOB> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public TR_JOB BindData(SaveJobRequest mtProjectDData, TR_JOB data, UserAccess userAccess, ActionType actionType)
        {
            if (data == null)
                data = new TR_JOB();

            data.COMP_CODE = mtProjectDData.compCode;
            data.PROJECT_CODE = mtProjectDData.projectCode;
            data.JOB_NAME = mtProjectDData.jobName;
            data.PERSON_CODE = mtProjectDData.personCode;
            data.SERVICE_CODE = mtProjectDData.srvcCode;
            data.SUB_SERVICE_CODE = mtProjectDData.subSrvcCode;
            data.JOB_DESC = mtProjectDData.jobDesc;
            data.JOB_STATUS_CODE = mtProjectDData.jobStatusCode;
            data.WORK_BUDGET = mtProjectDData.workBudget;
            data.BRIEF_DATE = mtProjectDData.briefDate;
            data.DUE_DATE = mtProjectDData.dueDate;
            data.ISSUE_BY = mtProjectDData.issueBy;
            data.ISSUE_DATE = mtProjectDData.issueDate;
            data.JOB_YEAR = DateTime.Now.ToString("yyyy");

            if (actionType == ActionType.Add)
            {
                TrDocRunning trDocRunning = new TrDocRunning();
                data.JOB_NO = trDocRunning.GetDocumentRunning(mtProjectDData.compCode, "", mtProjectDData.compCode, int.Parse(DateTime.Now.ToString("yy")), int.Parse(DateTime.Now.ToString("MM")), 3);
                data.CREATE_BY = userAccess.fullName;
                data.CREATE_DATE = DateTime.Now;
                data.IS_ACTIVE = StatusYesNo.Yes.Value();
            }
            else
            {
                data.UPDATE_BY = userAccess.fullName;
                data.UPDATE_DATE = DateTime.Now;

            }

            return data;
        }

        public TR_JOB Save(SaveJobRequest data, UserAccess user, out bool resultStatus, out bool vacationJobStatus)
        {
            resultStatus = false;
            vacationJobStatus = false;
            if (data.jobNo != null)
            {
                this.Data = this.GetDataByJobNo(data.jobNo);
                if (this.Data != null)
                {
                    if (this.Data.JOB_STATUS_CODE == "OP" && data.jobStatusCode == "AP" && this.Data.SERVICE_CODE == "LAV")
                    {
                        vacationJobStatus = true;
                    }
                }
            }
            if (this.Data == null)
            {
                this.Data = this.BindData(data, this.Data, user, ActionType.Add);
                this.Create(this.Data);
                resultStatus = true;
            }
            else
            {
                this.Data = this.BindData(data, this.Data, user, ActionType.Edit);
                this.Edit("", this.Data);
                resultStatus = true;
            }

            return this.Data;

        }


        public IEnumerable<TR_JOB> GetTrJob(GetSummaryTaskListRequest model)
        {
            var data = dbContext.TR_JOB.AsQueryable();
            if (!model.jobNo.IsNullOrEmptyWhiteSpace())
            {
                data = data.Where(r => r.JOB_NO == model.jobNo).AsQueryable();
            }
            if (!model.compCode.IsNullOrEmptyWhiteSpace())
            {
                data = data.Where(r => r.COMP_CODE == model.compCode).AsQueryable();
            }
            if (model.jobStatusCodeList != null && model.jobStatusCodeList.Count > 0)
            {
                data = data.Where(r => model.jobStatusCodeList.Contains(r.JOB_STATUS_CODE)).AsQueryable();
            }
            if (!model.year.IsNullOrEmptyWhiteSpace())
            {
                data = data.Where(r => r.JOB_YEAR == model.year).AsQueryable();
            }
            if (!model.projectCode.IsNullOrEmptyWhiteSpace())
            {
                data = data.Where(r => r.PROJECT_CODE == model.projectCode).AsQueryable();
            }
            var a = data.ToList();
            return a;
        }
    }

}