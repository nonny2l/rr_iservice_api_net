﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrNotifyUserRead : AbRepository<TR_NOTIFY_READ>
    {
        public override TR_NOTIFY_READ GetData(string id)
        {
            return this.FindBy(a => a.TR_NOTIFY__READ_ID.ToString() == id).FirstOrDefault();
        }

        public TR_NOTIFY_READ GetDataUserRead(string empNo, string notifyCode)
        {
            var data = this.FindBy(a => a.EMP_NO == empNo && a.NOTIFY_MSG_CODE == notifyCode).FirstOrDefault();

            return data;
        }
        public string GetStatusUserRead(string empNo,string notifyCode)
        {
            string result = string.Empty;

           var data = this.FindBy(a => a.EMP_NO == empNo && a.NOTIFY_MSG_CODE == notifyCode).FirstOrDefault();
            if (data != null)
                result = StatusYesNo.Yes.Value();
            else
                result = StatusYesNo.No.Value();

            return result;
        }
        public List<string>  GetNotifyCodeListByEmpNo(string empNo)
        {
            return this.FindBy(a => a.EMP_NO == empNo && a.IS_ACTIVE == "Y").Select(a => a.NOTIFY_MSG_CODE).ToList();
        }

        public override List<TR_NOTIFY_READ> GetDatas(string id)
        {
            return this.FindBy(a => a.NOTIFY_MSG_CODE == id && a.IS_ACTIVE == "Y").ToList();
        }

   
    }
   
}