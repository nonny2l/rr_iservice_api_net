﻿using RRApiFramework.Model;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel.Master
{
    public class MtStatus : AbRepository<MT_STATUS>
    {
        public override MT_STATUS GetData(string id)
        {
            return FindBy(a => a.ID.ToString() == id).FirstOrDefault();
        }

        public override List<MT_STATUS> GetDatas(string id)
        {
            return FindBy(r => r.IS_ACTIVE == "Y" && r.STATUS_CODE == id).ToList();
        }
       
    }
}