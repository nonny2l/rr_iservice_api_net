﻿using RRApiFramework.Model;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class MtNotifyConfig : AbRepository<MT_NOTIFY_CONFIG>
    {
        public override MT_NOTIFY_CONFIG GetData(string id)
        {
            return dbContext.MT_NOTIFY_CONFIG.Find(int.Parse(id));
        }

        public override List<MT_NOTIFY_CONFIG> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public MT_NOTIFY_CONFIG GetDataByCode(string notiTopicCode)
        {
            return dbContext.MT_NOTIFY_CONFIG.Where(r => r.NOTIFY_TOPIC_CODE == notiTopicCode).FirstOrDefault();
        }
    }
}