﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrCompanyCa : AbRepository<TR_COMPANY_CA>
    {
        public override TR_COMPANY_CA GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.CA_ID == idInt).FirstOrDefault();
        }
     
      
        public override List<TR_COMPANY_CA> GetDatas(string id)
        {
            throw new NotImplementedException();
        }
    }
  
}