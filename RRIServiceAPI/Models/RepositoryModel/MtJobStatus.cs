﻿using RRApiFramework.Model;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel.Master
{
    public class MtJobStatus : AbRepository<MT_JOB_STATUS>
    {
        public override MT_JOB_STATUS GetData(string id)
        {
            return FindBy(a => a.JOB_STATUS_CODE == id).FirstOrDefault();
        }

        public override List<MT_JOB_STATUS> GetDatas(string id)
        {
            return FindBy(r => r.IS_ACTIVE == "Y" && r.JOB_STATUS_CODE == id).ToList();
        }
       
    }
}