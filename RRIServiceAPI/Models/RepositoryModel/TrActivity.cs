﻿using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrActivity : AbRepository<TR_ACTIVITY>
    {
        public override TR_ACTIVITY GetData(string id)
        {
            return FindBy(r => r.ACTIVITY_ID.ToString() == id).FirstOrDefault();
        }

        public override List<TR_ACTIVITY> GetDatas(string id)
        {
            return this.GetAll().ToList();
        }

        public async Task<TR_ACTIVITY> Save(ActivityRequest request)
        {
            this.Data = this.GetData(request.activityId.ToString());
            if (this.Data == null)
            {
                this.Data = await this.BindData(request, this.Data, ActionType.Add);
                this.Create(this.Data);
            }
            else
            {
                this.Data = await this.BindData(request, this.Data, ActionType.Edit);
                this.Edit(request.activityId.ToString(), this.Data);
            }

            return this.Data;
        }

        internal async Task<TR_ACTIVITY> BindData(ActivityRequest model, TR_ACTIVITY data, ActionType actionType)
        {
            if (data == null)
                data = new TR_ACTIVITY();

            data.IS_ACTIVE = model.isActive;
            data.CA_ID = model.caId;
            data.ACTIVITY_DATE = model.activityDate != null ? model.activityDate : DateTime.Now;
            data.ACTIVITY_STATUS = model.activityStatus;
            data.ACTIVITY_TYPE_CFID = model.activityTypeId;
            data.REF_CALL_H_ID = model.refCallHId;
            data.REF_VENDOR_ID = model.refVendorId;
            data.REMARK = model.remark;

            if (model.activityStatus.ToLower() == "c")
            {
                data.CANCEL_DATE = DateTime.Now;
                data.IS_ACTIVE = "N";
            }

            if (actionType == ActionType.Add)
            {
                data.CREATE_BY = model.createBy;
                data.CREATE_DATE = DateTime.Now;
            }
            else
            {
                data.UPDATE_BY = model.updateBy;
                data.CREATE_DATE = DateTime.Now;
            }
            return data;
        }

        public async Task<List<TR_ACTIVITY>> getActivityList(GetActivityRequest model)
        {
            var data = this.GetAll();
            var _result = await filterDataActivity(data, model);
            return _result.ToList();
        }

        private async Task<IQueryable<TR_ACTIVITY>> filterDataActivity(IQueryable<TR_ACTIVITY> data, GetActivityRequest model)
        {
            if (model.caId != null && model.caId != 0)
            {
                data = data.Where(r => r.CA_ID == model.caId).AsQueryable();
            }
            if (model.activityId != null && model.activityId != 0)
            {
                data = data.Where(r => r.ACTIVITY_ID == model.activityId).AsQueryable();
            }
            if (model.activityTypeId != null && model.activityTypeId != 0)
            {
                data = data.Where(r => r.ACTIVITY_TYPE_CFID == model.activityTypeId).AsQueryable();
            }
            if (model.refCallHId != null && model.refCallHId != 0)
            {
                data = data.Where(r => r.REF_CALL_H_ID == model.refCallHId).AsQueryable();
            }

            return data;
        }
    }
}