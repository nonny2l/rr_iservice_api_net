﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormAPI.Models.RepositoryModel.Master;
using RRPlatFormModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrNotifyMsg : AbRepository<TR_NOTIFY_MSG>
    {
        public override TR_NOTIFY_MSG GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.NOTIFY_MSG_ID == idInt).FirstOrDefault();
        }
        public TR_NOTIFY_MSG GetDataByHCode(string code)
        {
            return this.FindBy(a => a.NOTIFY_MSG_CODE == code && a.IS_ACTIVE == "Y").FirstOrDefault();
        }

        public List<TR_NOTIFY_MSG> GetDatasByEmpNo(string empNo)
        {
            return this.FindBy(a => a.EMP_NO == empNo && a.IS_ACTIVE == "Y").ToList();
        }
        

        public static string GetName(string code)
        {
            string result = string.Empty;
            using (var rRIServiceModelContext = new RRIServiceModelContext())
            {
                var data = rRIServiceModelContext.TR_NOTIFY_MSG.Where(a => a.NOTIFY_MSG_CODE == code).FirstOrDefault();
                if (data != null)
                    result = data.NOTIFY_DESC;
            }
            return result;
        }
      
        public override List<TR_NOTIFY_MSG> GetDatas(string id)
        {
            return this.FindBy(a => a.NOTIFY_TOPIC_CODE == id && a.IS_ACTIVE == "Y").ToList();
        }

        public TR_NOTIFY_MSG Save(SaveNotiMsgRequest request, out bool isSsuccess)
        {
            isSsuccess = false;
            this.Data = this.GetData(request.notifyMsgId.ToString());
            if (this.Data == null)
            {
                this.Data = this.BindData(request, this.Data, ActionType.Add);
                this.Create(this.Data);
                isSsuccess = true;
            }
            //else
            //{
            //    this.Data = this.BindData(request, this.Data, ActionType.Edit);
            //    this.Edit(request.NOTIFY_MSG_ID.ToString(), this.Data);
            //}

            return this.Data;
        }
        public TR_NOTIFY_MSG BindData(SaveNotiMsgRequest model, TR_NOTIFY_MSG data, ActionType actionType)
        {
            if (data == null)
                data = new TR_NOTIFY_MSG();

            data.NOTIFY_MSG_CODE = model.notifyMsgCode;

            data.EMP_NO = model.empNo;
            data.IS_ACTIVE = model.isActive;
            data.NOTIFY_DESC = model.notifyDesc;
            data.NOTIFY_GROUP_CODE = model.notifyGroupCode;
            data.NOTIFY_HEADER = model.notifyHeader;
            data.NOTIFY_TOPIC_CODE = model.notifyTopicCode;


            data.REF_KEY = model.refKey;
            data.REF_TABLE = model.refTable;
            data.REF_VALUE = model.refValue;

            data.URL = model.url;

            if (actionType == ActionType.Add)
            {
                data.CREATE_BY = model.createBy;
                data.CREATE_DATE = DateTime.Now;
            }
            else
            {
                data.UPDATE_BY = model.updateBy;
                data.CREATE_DATE = DateTime.Now;
            }
            return data;
        }
        public string getNotifyMsgRunning(string prefix, int runningDigit)
        {
            var _runningCurrent = dbContext.TR_NOTIFY_MSG.Where(r => r.NOTIFY_MSG_CODE.Contains(prefix)).OrderByDescending(r => r.CREATE_DATE).FirstOrDefault();
            var newRunning = 1;
            var runningFormat = "";
            var runningNext = "";
            if (_runningCurrent != null)
            {
                if (!_runningCurrent.NOTIFY_MSG_CODE.IsNullOrEmptyWhiteSpace())
                {
                    newRunning = int.Parse(_runningCurrent.NOTIFY_MSG_CODE.Substring(_runningCurrent.NOTIFY_MSG_CODE.Length - runningDigit, runningDigit)) + 1;
                }
               
            }
            for (int i = 0; i < runningDigit; i++)
            {
                runningFormat += "0";
            }
            runningNext = $"{prefix}{newRunning.ToString(runningFormat)}";
            return runningNext;

        }
    }
   
}