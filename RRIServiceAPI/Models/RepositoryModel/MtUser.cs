﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormAPI.Models.RepositoryModel.Master;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class MtUser : AbRepository<MT_USER>
    {
        public override MT_USER GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.USER_ID == idInt).FirstOrDefault();
        }

        public MT_USER GetDataByEmpNo(string empNo)
        {
            return this.FindBy(a => a.EMP_NO == empNo).FirstOrDefault();
        }

        public ActionResultStatus CheckLogIn(string userName, string password, int userGroup)
        {
            ActionResultStatus result = new ActionResultStatus();

            this.Data = new MT_USER();
            this.Data = this.FindBy(a => a.USERNAME == userName).FirstOrDefault();
            if (this.Data != null)
            {
                if (password != this.Data.PASSWORD)
                {
                    result.success = false;
                    result.code = (int)StatusCode.PasswordInCorrect;
                    result.message = StatusCode.PasswordInCorrect.Value();
                    result.description = "รหัสผ่านของคุณไม่ถูกต้อง";
                    return result;
                }
                else if (userGroup != this.Data.USER_GROUP_ID)
                {
                    result.success = false;
                    result.code = (int)StatusCode.NoPermission;
                    result.message = StatusCode.NoPermission.Value();
                    result.description = "กลุ่มผู้ใช้งานของคุณไม่ถูกต้อง";
                }
                else
                {
                    UserAccess userData = BindData(this.Data);
                    result.data = userData;
                    result.key = JwtManagement.GenerateJasonWebToken(userData);
                }
            }
            else
            {
                result.success = false;
                result.code = (int)StatusCode.NoPermission;
                result.message = StatusCode.NoPermission.Value();
                result.description = "ไม่มีผู้ใช้งานนี้ในระบบ";
            }


            return result;
        }

        public ActionResultStatus CheckLogOut(string userId)
        {
            ActionResultStatus result = new ActionResultStatus();

            this.Data = new MT_USER();
            this.Data = this.GetData(userId);
            if (this.Data == null)
            {
                result.success = false;
                result.code = (int)StatusCode.NoPermission;
                result.message = StatusCode.NoPermission.Value();
                result.description = "ไม่มีผู้ใช้งานนี้ในระบบ";
            }
            else
            {
                result.key = null;
            }
            return result;
        }

        public UserAccess BindData(MT_USER dataBind)
        {
            UserAccess data = new UserAccess();
            //TrStaffTeam trStaffTeam = new TrStaffTeam();
            MtTeamRole mtTeamRole = new MtTeamRole();

            data.userId = dataBind.USER_ID.ToString();
            data.email = dataBind.EMAIL;
            data.userName = dataBind.USERNAME;
            data.userGroupId = dataBind.USER_GROUP_ID.ToString();
            data.userTypeId = dataBind.USER_TYPE_ID.ToString();
            data.isActive = dataBind.IS_ACTIVE;
            data.empNo = dataBind.EMP_NO;
            data.teamRoleList = mtTeamRole.GetUserIdList(dataBind.EMP_NO);
            data.fullName = dataBind.NAME;

            MtEmployeePosition mtEmployeePosition = new MtEmployeePosition();
            var dataEmpPositionList = mtEmployeePosition.GetDatasByEmpNo(data.empNo);
            data.positionList = new List<PositionData>();
            foreach (var item in dataEmpPositionList)
            {
                PositionData positionData = new PositionData();
                positionData.deptCode = item.DEPT_CODE;
                positionData.deptName = MtDepartment.GetName(item.DEPT_CODE);
                positionData.positionCode = item.POSITION_CODE;
                positionData.positionName = MtPosition.GetName(item.POSITION_CODE);

                data.positionList.Add(positionData);
            }

            return data;
        }

        public override List<MT_USER> GetDatas(string id)
        {
            return dbContext.MT_USER.ToList();
        }

        public GetUserProfileResponse GetUserProfile(string empNo)
        {
            MtTeamRole mtTeamRole = new MtTeamRole();
            MtEmployeePosition mtEmployeePosition = new MtEmployeePosition();

            var data = (from a in dbContext.MT_USER
                        join b in dbContext.MT_USER_TYPE on a.USER_TYPE_ID equals b.USER_TYPE_ID into bb
                        from b in bb.DefaultIfEmpty()
                        join c in dbContext.MT_USER_GROUP on a.USER_GROUP_ID equals c.USER_GROUP_ID into cc
                        from c in cc.DefaultIfEmpty()
                        where a.EMP_NO == empNo
                        select new GetUserProfileResponse
                        {
                            avatar = a.AVATAR,
                            email = a.EMAIL,
                            empNo = a.EMP_NO,
                            isActive = a.IS_ACTIVE,
                            name = a.NAME,
                            remark = a.REMARK,
                            tokenDateTime = 0,
                            userGroupId = a.USER_GROUP_ID.ToString(),
                            userGroupName = c.NAME,
                            userId = a.USER_ID.ToString(),
                            userName = a.USERNAME,
                            userTypeId = a.USER_TYPE_ID.ToString(),
                            userTypeName = b.USER_TYPE_NAME,
                        }).FirstOrDefault();

            data.teamRoleList = mtTeamRole.GetUserIdList(data.empNo);
            var dataEmpPositionList = mtEmployeePosition.GetDatasByEmpNo(data.empNo);

            data.positionList = new List<PositionData>();
            foreach (var item in dataEmpPositionList)
            {
                PositionData positionData = new PositionData();
                positionData.deptCode = item.DEPT_CODE;
                positionData.deptName = MtDepartment.GetName(item.DEPT_CODE);
                positionData.positionCode = item.POSITION_CODE;
                positionData.positionName = MtPosition.GetName(item.POSITION_CODE);
                data.positionList.Add(positionData);
            }
            return data;
        }

        public List<MT_USER> GetDatas(List<string> usernames)
        {
            return dbContext.MT_USER.Where(r => usernames.Contains(r.USERNAME)).ToList();
        }

        public MT_USER SavePassword(SavePasswordRequest request, UserAccess userAccess, out bool resultStatus)
        {
            resultStatus = false;
            this.Data = this.FindBy(r => r.USERNAME == userAccess.userName && r.PASSWORD == request.currentPassword).FirstOrDefault();
            if (this.Data != null)
            {
                request.fullName = userAccess.fullName;
                this.Data = this.BindDataPassword(request, this.Data, ActionType.Edit);
                this.Edit(userAccess.userId, this.Data);

                resultStatus = true;
            }
            return this.Data;
        }

        public MT_USER BindDataPassword(SavePasswordRequest model, MT_USER data, ActionType actionType)
        {
            if (data == null)
                data = new MT_USER();

            data.PASSWORD = model.newPassword;
            data.UPDATE_BY = model.fullName;
            data.UPDATE_DATE = DateTime.Now;

            return data;
        }
    }
}