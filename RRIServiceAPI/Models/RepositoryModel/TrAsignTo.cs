﻿using RRApiFramework;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Request.Customer;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrAsignTo : AbRepository<TR_ASSIGN_TO>
    {
        public override TR_ASSIGN_TO GetData(string id)
        {
            return FindBy(r => r.ASSIGN_TO_ID.ToString() == id).FirstOrDefault();
        }

        public override List<TR_ASSIGN_TO> GetDatas(string id)
        {
            return FindBy(r => r.TASK_CODE == id && r.IS_ACTIVE == "Y").ToList();
        }

        public TR_ASSIGN_TO CheckDupicate(string taskCode,string empNo)
        {
            return FindBy(r => r.TASK_CODE == taskCode && r.EMP_NO == empNo && r.IS_ACTIVE == "Y").FirstOrDefault();
        }

        public  List<TR_ASSIGN_TO> GetDatasByUserIdList(List<string> empNoList, List<string> taskCodeList)
        {
            List<TR_ASSIGN_TO> result = new List<TR_ASSIGN_TO>();

            if (empNoList.Count != 0)
            {
                result = this.FindBy(a => empNoList.Contains(a.EMP_NO) && taskCodeList.Contains(a.TASK_CODE) && a.IS_ACTIVE == "Y").ToList();
            }
            else
            {
                result = this.FindBy(a =>  taskCodeList.Contains(a.TASK_CODE) && a.IS_ACTIVE == "Y").ToList();
            }

            return result;
        }

        public TR_ASSIGN_TO BindData(SaveTaskAssignDatas trTaskData, TR_ASSIGN_TO data, UserAccess userAccess, ActionType actionType)
        {
            if (data == null)
                data = new TR_ASSIGN_TO();

            data.TASK_CODE = trTaskData.taskCode;
            data.EMP_NO = trTaskData.empNo;
           // data.EMAIL = trTaskData.email;
            data.IS_ACTIVE = trTaskData.isActive;

            if (actionType == ActionType.Add)
            {
                data.CREATE_BY = userAccess.fullName;
                data.CREATE_DATE = DateTime.Now;
            }
            else
            {
                data.UPDATE_BY = userAccess.fullName;
                data.UPDATE_DATE = DateTime.Now;

            }

            return data;
        }

        public TR_ASSIGN_TO Save(SaveTaskAssignDatas data, UserAccess userAccess, out bool resultStatus)
        {
            resultStatus = false;
            this.Data = this.CheckDupicate(data.taskCode,data.empNo);
            if (this.Data == null)
            {
                this.Data = this.BindData(data, this.Data, userAccess, ActionType.Add);
                this.Create(this.Data);
                resultStatus = true;
            }
            else
            {
                this.Data = this.BindData(data, this.Data, userAccess, ActionType.Edit);
                this.Edit("", this.Data);
                resultStatus = true;
            }

            return this.Data;
        }

    }
    public class TrAssingToDto
    {
        public int assingToId { get; set; }
        public string taskCode { get; set; }
        public int userId { get; set; }
        public string email { get; set; }
        public string isActive { get; set; }
    }
}