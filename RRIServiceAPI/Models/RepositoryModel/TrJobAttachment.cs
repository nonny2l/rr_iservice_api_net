﻿using RRApiFramework;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Request.Customer;
using RRPlatFormAPI.Models.RepositoryModel.Master;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrJobAttachment : AbRepository<TR_JOB_ATTACHMENT>
    {
        public override TR_JOB_ATTACHMENT GetData(string id)
        {
            return FindBy(r => r.JOB_ATTACHMENT_ID.ToString() == id).FirstOrDefault();
        }


        public List<TR_JOB_ATTACHMENT> GetDatasByJobNo( string jobNo)
        {
            return FindBy(r => r.JOB_NO == jobNo && r.IS_ACTIVE == "Y").ToList();
        }

        public List<TR_JOB_ATTACHMENT> GetDataByJobNo(string jobNo)
        {
            return FindBy(r => r.JOB_NO == jobNo && r.IS_ACTIVE == "Y").ToList();
        }


        public override List<TR_JOB_ATTACHMENT> GetDatas(string id)
        {
            return FindBy(r => r.JOB_NO == id &&  r.IS_ACTIVE == "Y").ToList();
        }

        public TR_JOB_ATTACHMENT BindData(TrJobAttachmentDto teamData, TR_JOB_ATTACHMENT data, UserAccess userAccess, ActionType actionType)
        {
            if (data == null)
                data = new TR_JOB_ATTACHMENT();

            data.FILE_NAME = teamData.fileName;
            data.FILE_PATH = teamData.filePath;
            data.FILE_TYPE = teamData.fileType;
            data.IS_ACTIVE = teamData.isActive;
            data.JOB_NO = teamData.jobNo;
            data.OLD_FILE_NAME = teamData.oldFileName;
            data.SEQ = teamData.seq == null ? 0 : (int)teamData.seq;
            
            if (actionType == ActionType.Add)
            {
                data.CREATE_BY = userAccess.fullName;
                data.CREATE_DATE = DateTime.Now;
            }
            else
            {
                data.UPDATE_BY = userAccess.fullName;
                data.UPDATE_DATE = DateTime.Now;

            }

            return data;
        }

        public TR_JOB_ATTACHMENT Save(TrJobAttachmentDto data, UserAccess userAccess, out bool resultStatus)
        {
            resultStatus = false;
            this.Data = this.GetData(data.jobAttachId.ToString());
            if (this.Data == null)
            {
                this.Data = this.BindData(data, this.Data, userAccess, ActionType.Add);
                this.Create(this.Data);
                resultStatus = true;
            }
            else
            {
                this.Data = this.BindData(data, this.Data, userAccess, ActionType.Edit);
                this.Edit("", this.Data);
                resultStatus = true;
            }

            return this.Data;
        }



    }
    public class TrJobAttachmentDto : MasterDto
    {
        public int jobAttachId { get; set; }
        public int? seq { get; set; }
        public string jobNo { get; set; }
        public string filePath { get; set; }
        public string fileName { get; set; }
        public string oldFileName { get; set; }
        public string fileType { get; set; }
    }


}