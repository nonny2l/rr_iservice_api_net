﻿using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request.Customer;
using RRPlatFormAPI.Utility;
using RRPlatFormModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel.Customer
{
    public class MtCompany : AbRepository<MT_COMPANY>
    {
        public override MT_COMPANY GetData(string id)
        {
            return dbContext.MT_COMPANY.Where(r => r.COMP_ID.ToString() == id).FirstOrDefault();
        }

        public override List<MT_COMPANY> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public static string GetName(string comCode)
        {
            string result = string.Empty;
            using (var rRIServiceModelContext = new RRIServiceModelContext())
            {
                var data = rRIServiceModelContext.MT_COMPANY.Where(a => a.COMP_CODE == comCode).FirstOrDefault();
                if (data != null)
                    result = data.COMP_NAME;
            }
            return result;
        }

        public List<MT_COMPANY> getCompany(GetCustomerListFilterRequest model)
        {
            var data = dbContext.MT_COMPANY.Where(r => r.IS_ACTIVE == "Y").AsQueryable();
            var _result = filterDataCompany(data, model);
            return _result.ToList();
        }

        private IQueryable<MT_COMPANY> filterDataCompany(IQueryable<MT_COMPANY> data, GetCustomerListFilterRequest model)
        {
            if (model.compId != null && model.compId != 0)
            {
                data = data.Where(r => r.COMP_ID == model.compId).AsQueryable();
            }
           
            if (!model.compName.IsNullOrEmptyWhiteSpace())
            {
                var valueTrimed = model.compName.Trim(Commons.trimChars);
                data = data.Where(r => r.COMP_NAME.Contains(valueTrimed)).AsQueryable();
            }
            if (!model.businessType.IsNullOrEmptyWhiteSpace())
            {
                var valueTrimed = model.businessType.Trim(Commons.trimChars);
                data = data.Where(r => r.BUSINESS_TYPE == valueTrimed).AsQueryable();
            }
            if (!model.subBusinessType.IsNullOrEmptyWhiteSpace())
            {
                var valueTrimed = model.subBusinessType.Trim(Commons.trimChars);
                data = data.Where(r => r.SUB_BUSINESS_TYPE == valueTrimed).AsQueryable();
            }
            return data;
        }

        public MT_COMPANY Save(MtCompanyRequest request)
        {
            this.Data = this.GetData(request.compId.ToString());
            if (this.Data == null)
            {
                this.Data = this.BindData(request, this.Data, ActionType.Add);
                this.Create(this.Data);
            }
            else
            {
                this.Data = this.BindData(request, this.Data, ActionType.Edit);
                this.Edit(request.compId.ToString(), this.Data);
            }

            return this.Data;
        }

        internal MT_COMPANY BindData(MtCompanyRequest model, MT_COMPANY data, ActionType actionType)
        {
            if (data == null)
                data = new MT_COMPANY();

            data.IS_ACTIVE = model.isActive;
            data.BUSINESS_TYPE = model.businessType;
            data.COMP_NAME = model.compName;
            data.SUB_BUSINESS_TYPE = model.subBusinessType;
            data.WEBSITE = model.website;
            data.IS_COMPANY = model.isCompany;
            
            if (actionType == ActionType.Add)
            {
                data.CREATE_BY = model.createBy;
                data.CREATE_DATE = DateTime.Now;
            }
            else
            {
                data.UPDATE_BY = model.updateBy;
                data.CREATE_DATE = DateTime.Now;
            }
            return data;
        }
    }
}