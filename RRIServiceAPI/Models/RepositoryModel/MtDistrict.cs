﻿using RRApiFramework.Model;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel.Master
{
    public class MtDistrict : AbRepository<MT_DISTRICT>
    {
        public override MT_DISTRICT GetData(string id)
        {
            return FindBy(a => a.DISTRICT_CODE == id).FirstOrDefault();
        }

        public override List<MT_DISTRICT> GetDatas(string id)
        {
            return dbContext.MT_DISTRICT.Where(r => r.IS_ACTIVE == "Y" || (r.DISTRICT_CODE == "" || r.DISTRICT_CODE == id)).ToList();
        }
        public List<MT_DISTRICT> GetDatasByProvinceCode(string provCode)
        {
            return dbContext.MT_DISTRICT.Where(r => r.IS_ACTIVE == "Y" && r.REF_PROV_CODE == provCode).ToList();
        }
    }
}