﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrTaskDocumentComment : AbRepository<TR_TASK_DOCUMENT_COMMENT>
    {
        public override TR_TASK_DOCUMENT_COMMENT GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.COMMENT_ID == idInt).FirstOrDefault();
        }

        public override List<TR_TASK_DOCUMENT_COMMENT> GetDatas(string id)
        {
            return this.FindBy(a => a.TASK_CODE == id && a.IS_ACTIVE == "Y").ToList();
        }

        public TR_TASK_DOCUMENT_COMMENT BindData(trTaskCommentDto trTaskData, TR_TASK_DOCUMENT_COMMENT data, UserAccess userAccess, ActionType actionType)
        {
            if (data == null)
                data = new TR_TASK_DOCUMENT_COMMENT();

            data.TASK_CODE = trTaskData.taskCode;
            data.COMMENT_ID = trTaskData.commentId;
            data.COMMENT_DESC = trTaskData.commentDesc;
            data.USER_ID = trTaskData.userId;
            data.IS_ACTIVE = trTaskData.isActive;

            if (actionType == ActionType.Add)
            {
                data.CREATE_BY = userAccess.fullName;
                data.CREATE_DATE = DateTime.Now;
            }
            else
            {
                data.UPDATE_BY = userAccess.fullName;
                data.UPDATE_DATE = DateTime.Now;

            }

            return data;
        }

        public TR_TASK_DOCUMENT_COMMENT Save(trTaskCommentDto data, UserAccess userAccess, out bool resultStatus)
        {
            resultStatus = false;
            this.Data = this.GetData(data.commentId.ToString());
            if (this.Data == null)
            {
                this.Data = this.BindData(data, this.Data, userAccess, ActionType.Add);
                this.Create(this.Data);
                resultStatus = true;
            }
            else
            {
                this.Data = this.BindData(data, this.Data, userAccess, ActionType.Edit);
                this.Edit("", this.Data);
                resultStatus = true;
            }

            return this.Data;
        }


    }
    public class trTaskCommentDto
    {
        public int commentId { get; set; }
        public string commentDesc { get; set; }
        public string taskCode { get; set; }
        public int userId { get; set; }
        public string isActive { get; set; }
    }

}