﻿using RRApiFramework;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request.Customer;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormAPI.Utility;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel.Customer
{
    public class TrCompanyPerson : AbRepository<TR_COMPANY_PERSON>
    {
        public override TR_COMPANY_PERSON GetData(string id)
        {
            return FindBy(r => r.COMP_PERSON_ID.ToString() == id).FirstOrDefault();
        }

        public override List<TR_COMPANY_PERSON> GetDatas(string id)
        {
            return this.Datas;
        }

        public List<TR_COMPANY_PERSON> GetDatasActiveByCompId(string id)
        {
            return dbContext.TR_COMPANY_PERSON.Where(r =>  r.IS_ACTIVE == "Y").ToList();
        }


        public List<TR_COMPANY_PERSON> GetDatasByMainStatus(int caId,string mainStatus)
        {
            return dbContext.TR_COMPANY_PERSON.Where(r =>  r.IS_MAIN == mainStatus && r.IS_ACTIVE == "Y").ToList();
        }

        public TR_COMPANY_PERSON Save(SaveContactDataRequest request ,UserAccess userAccess,out bool statusResult)
        {
            statusResult = false;
            this.Data = this.GetData(request.compPersonId.ToString());
            if (this.Data == null)
            {
                this.Data = this.BindData(request, this.Data, userAccess, ActionType.Add);
                this.Create(this.Data);
                statusResult = true;
            }
            else
            {
                this.Data = this.BindData(request, this.Data, userAccess, ActionType.Edit);
                this.Edit(request.compPersonId.ToString(), this.Data);
                statusResult = true;
            }

            return this.Data;
        }

        internal TR_COMPANY_PERSON BindData(SaveContactDataRequest model, TR_COMPANY_PERSON data,UserAccess userAccess, ActionType actionType)
        {
            if (data == null)
                data = new TR_COMPANY_PERSON();

            data.IS_ACTIVE = model.isActive;
            data.EMAIL = model.email;
            data.IS_MAIN = model.isMain;
            data.COMP_CODE = model.compCode;
            data.PERSON_CODE = model.personCode;
            data.MOBILE = model.mobile;
            data.JOB_POSITION = model.jobPositionName;

            if (actionType == ActionType.Add)
            {
                data.CREATE_BY = userAccess.fullName;
                data.CREATE_DATE = DateTime.Now;
            }
            else
            {
                data.UPDATE_BY = userAccess.fullName;
                data.CREATE_DATE = DateTime.Now;
            }
            return data;
        }

        private IQueryable<CompanyPersonResponse> filterCompanyPersonList(IQueryable<CompanyPersonResponse> data, GetCustomerListFilterRequest model)
        {
            if (model.compId != null && model.compId != 0)
            {
                data = data.Where(r => r.compId == model.compId).AsQueryable();
            }
            if (!model.contactTypeCfCode.IsNullOrEmptyWhiteSpace())
            {
                var valueTrimed = model.contactTypeCfCode.Trim(Commons.trimChars);
                data = data.Where(r => r.contactTypeCfCode == valueTrimed).AsQueryable();
            }
            if (!model.email.IsNullOrEmptyWhiteSpace())
            {
                var valueTrimed = model.email.Trim(Commons.trimChars);
                data = data.Where(r => r.email.Contains(valueTrimed)).AsQueryable();
            }
            if (!model.firstName.IsNullOrEmptyWhiteSpace())
            {
                var valueTrimed = model.firstName.Trim(Commons.trimChars);
                data = data.Where(r => r.firstName.Contains(valueTrimed)).AsQueryable();
            }
            if (!model.lastName.IsNullOrEmptyWhiteSpace())
            {
                var valueTrimed = model.lastName.Trim(Commons.trimChars);
                data = data.Where(r => r.lastName.Contains(valueTrimed)).AsQueryable();
            }
            if (!model.mobile.IsNullOrEmptyWhiteSpace())
            {
                var valueTrimed = model.mobile.Trim(Commons.trimChars);
                data = data.Where(r => r.mobile.Contains(valueTrimed)).AsQueryable();
            }
            if (model.personId != null && model.personId != 0)
            {
                data = data.Where(r => r.personId == model.personId).AsQueryable();
            }
            if (!model.telephone.IsNullOrEmptyWhiteSpace())
            {
                var valueTrimed = model.telephone.Trim(Commons.trimChars);
                data = data.Where(r => r.telephone.Contains(valueTrimed)).AsQueryable();
            }
            if (!model.citizenId.IsNullOrEmptyWhiteSpace())
            {
                var valueTrimed = model.citizenId.Trim(Commons.trimChars);
                data = data.Where(r => r.citizenId.Contains(valueTrimed)).AsQueryable();
            }
            if (!model.passport.IsNullOrEmptyWhiteSpace())
            {
                var valueTrimed = model.passport.Trim(Commons.trimChars);
                data = data.Where(r => r.passport.Contains(valueTrimed)).AsQueryable();
            }

            return data;
        }


        public List<CompanyPersonResponse> GetCompanyPersonList(GetCustomerListFilterRequest model)
        {
            //var dataConfig = dbContext.MT_CONFIG_D.ToList();
            //var dataCompany = dbContext.MT_COMPANY.ToList();
            var data = (from a in dbContext.MT_PERSON
                        join b in dbContext.TR_COMPANY_PERSON on a.PERSON_CODE equals b.PERSON_CODE
                        join c in dbContext.MT_COMPANY on b.COMP_CODE equals c.COMP_CODE into cc
                        from c in cc.DefaultIfEmpty()
                        select new CompanyPersonResponse
                        {
                            //caId = b.CA_ID,
                            citizenId = a.CITIZEN_ID,
                            //compId = b.COMP_ID,
                            //compName = dataCompany.Any(r => r.COMP_ID == b.COMP_ID) ? dataCompany.Where(r => r.COMP_ID == b.COMP_ID).FirstOrDefault().COMP_NAME : null,
                            compPersonId = b.COMP_PERSON_ID,
                          //  contactTypeCfCode = b.CONTACT_TYPE_CODE,
                            //contactTypeCfName = dataConfig.Any(r => r.CONFIG_D_CODE == b.CONTACT_TYPE_CFCODE) ? dataConfig.Where(r => r.CONFIG_D_CODE == b.CONTACT_TYPE_CFCODE).FirstOrDefault().CONFIG_D_TNAME : null,
                           // department = b.DEPARTMENT,
                            email = b.EMAIL,
                            firstName = a.FIRST_NAME,
                            isActive = a.IS_ACTIVE,
                            isMain = b.IS_MAIN,
                          //  jobPosition = b.JOB_POSITION,
                            lastName = a.LAST_NAME,
                            lineId = a.LINE_ID,
                            mobile = a.MOBILE,
                            passport = a.PASSPORT,
                            personId = a.PERSON_ID,
                            //personSourceName = dataConfig.Any(r => r.CONFIG_D_ID == b.PERSON_SOURCE_CFID) ? dataConfig.Where(r => r.CONFIG_D_ID == b.PERSON_SOURCE_CFID).FirstOrDefault().CONFIG_D_TNAME : null,
                            telephone = a.TELEPHONE,
                         //   titleName = a.TITLE_NAME,
                            createBy = a.CREATE_BY,
                            createDate = a.CREATE_DATE,
                            updateBy = a.UPDATE_BY,
                            updateDate = a.UPDATE_DATE,
                            deleteBy = a.DELETE_BY,
                            deleteDate = a.DELETE_DATE,
                        }).AsQueryable();
            data = filterCompanyPersonList(data, model);
            return data.ToList();
        }
    }
}