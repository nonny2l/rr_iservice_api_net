﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request.Customer;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class MtSubService : AbRepository<MT_SUB_SERVICE>
    {
        public override MT_SUB_SERVICE GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.SUB_SERVICE_ID == idInt).FirstOrDefault();
        }

        public MT_SUB_SERVICE GetDataByServiceCode(string subServiceCode)
        {
            return this.FindBy(a => a.SUB_SERVICE_CODE == subServiceCode).FirstOrDefault();
        }

        public static string GetName(string code)
        {
            string result = string.Empty;
            using (var rRIServiceModelContext = new RRIServiceModelContext())
            {
                var data = rRIServiceModelContext.MT_SUB_SERVICE.Where(a => a.SUB_SERVICE_CODE == code).FirstOrDefault();
                if (data != null)
                    result = data.SUB_SERVICE_NAME;
            }
            return result;
        }

        public override List<MT_SUB_SERVICE> GetDatas(string id)
        {
            return this.FindBy(a => a.SERVICE_CODE == id).ToList();
        }
        public List<MT_SUB_SERVICE> GetSubServiceListFilter(GetSubServiceListRequest request)
        {
            List<MT_SUB_SERVICE> result = new List<MT_SUB_SERVICE>();

            result = this.GetAll().ToList();

            if (request != null)
            {
                if (!request.srvcCode.IsNullOrEmptyWhiteSpace())
                {
                    result = result.Where(a => a.SERVICE_CODE == request.srvcCode).ToList();
                }

                if (request.srvcSubId != null && request.srvcSubId != 0)
                {
                    result = result.Where(a => a.SUB_SERVICE_ID == request.srvcSubId).ToList();
                }

                if (!request.srvcSubName.IsNullOrEmptyWhiteSpace())
                {
                    result = result.Where(a => a.SUB_SERVICE_NAME.Contains(request.srvcSubName)).ToList();
                }

            }


            return result;
        }
    }
  
}