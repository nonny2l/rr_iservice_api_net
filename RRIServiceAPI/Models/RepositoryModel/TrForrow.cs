﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrForrow : AbRepository<TR_FOLLOW>
    {
        public override TR_FOLLOW GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.ID == idInt).FirstOrDefault();
        }

        public TrForrowDto BindDataDto(TR_FOLLOW dataBind)
        {
            TrForrowDto data = new TrForrowDto();

            data.id = dataBind.ID.ToString();
            data.customerId = dataBind.CUSTOMER_ID.ToString();
            data.addressSurvey = dataBind.ADDRESS_SURVEY;
            data.appointmentDate = dataBind.APPOINTMENT_DATE;
            data.venderCode = dataBind.VENDER_CODE;
            data.venderName = dataBind.VENDER_NAME;
            data.status = dataBind.STATUS.ToString();
            data.isActive = dataBind.IS_ACTIVE;
            data.createBy = dataBind.CREATE_BY;
            data.createDate = dataBind.CREATE_DATE;
            data.updateBy = dataBind.UPDATE_BY;
            data.updateDate = dataBind.UPDATE_DATE;

           
            return data;
        }

        public List<TR_FOLLOW> GetListByCustomerId(int customerId)
        {
            return this.FindBy(a => a.CUSTOMER_ID == customerId).ToList();
        }

        public override List<TR_FOLLOW> GetDatas(string id)
        {
            throw new NotImplementedException();
        }
    }
    public class TrForrowDto : MasterDto
    {
        public string id { get; set; }
        public string customerId { get; set; }
        public DateTime? appointmentDate { get; set; }
        public string addressSurvey { get; set; }
        public string venderName { get; set; }
        public string venderCode { get; set; }
        public string status { get; set; }
    }
}