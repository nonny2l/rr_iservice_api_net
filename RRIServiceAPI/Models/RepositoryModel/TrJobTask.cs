﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Request.Customer;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormAPI.Models.RepositoryModel.Master;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrJobTask : AbRepository<TR_JOB_TASK>
    {
        public override TR_JOB_TASK GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.JOB_TASK_ID == idInt).FirstOrDefault();
        }

        

        public override List<TR_JOB_TASK> GetDatas(string id)
        {
            throw new NotImplementedException();
        }
        public TR_JOB_TASK GetDataByJobTarkCode(string jobTarkCode)
        {
            return this.FindBy(a => a.JOB_TASK_CODE == jobTarkCode).FirstOrDefault();
        }

        public List<TR_JOB_TASK> GetDataByJobNo(string jobNo)
        {
            List<TR_JOB_TASK> result = new List<TR_JOB_TASK>();

            result = this.FindBy(a => a.JOB_NO == jobNo && a.IS_ACTIVE == "Y").ToList();
            return result;
        }

        public List<TR_JOB_TASK> GetJobList(string jobNo)
        {
            List<TR_JOB_TASK> result = new List<TR_JOB_TASK>();

            result = this.FindBy(a => a.JOB_NO == jobNo && a.IS_ACTIVE == "Y").ToList();
            return result;
        }

        public List<string> GetTeamList(GetTeamListRequest request)
        {
            List<string> result = new List<string>();
            IQueryable<TR_JOB_TASK> query = this.FindBy(a => a.IS_ACTIVE == "Y");

            if (!request.compCode.IsNullOrEmptyWhiteSpace())
                query = query.Where(a => a.COMP_CODE.Contains(request.compCode));

            if (!request.projectCode.IsNullOrEmptyWhiteSpace())
                query = query.Where(a => a.PROJECT_CODE.Contains(request.projectCode));

            if (!request.deptCode.IsNullOrEmptyWhiteSpace())
                query = query.Where(a => a.DEPT_CODE.Contains(request.deptCode));

            if (!request.jobNo.IsNullOrEmptyWhiteSpace())
                query = query.Where(a => a.JOB_NO.Contains(request.jobNo));

            result = query.Select(a => a.JOB_TASK_CODE).ToList();

            return result;
        }

        public TR_JOB_TASK AcceptJobTask(AcceptJobTaskRequest request)
        {
            TR_JOB_TASK result = new TR_JOB_TASK();
            IQueryable<TR_JOB_TASK> query = this.FindBy(a => a.IS_ACTIVE == "Y");

            if (!request.jobTaskCode.IsNullOrEmptyWhiteSpace())
            {
                query = query.Where(a => a.JOB_TASK_CODE.Contains(request.jobTaskCode));
            }
            //if (!request.compCode.IsNullOrEmptyWhiteSpace())
            //    query = query.Where(a => a.COMP_CODE.Contains(request.compCode));

            //if (!request.projectCode.IsNullOrEmptyWhiteSpace())
            //    query = query.Where(a => a.PROJECT_CODE.Contains(request.projectCode));

            //if (!request.deptCode.IsNullOrEmptyWhiteSpace())
            //    query = query.Where(a => a.DEPT_CODE.Contains(request.deptCode));

            //if (!request.jobNo.IsNullOrEmptyWhiteSpace())
            //    query = query.Where(a => a.JOB_NO.Contains(request.jobNo));

            result = query.FirstOrDefault();

            return result;
        }


        public TR_JOB_TASK GetDataCheckDupicate(int taskId, string deptNo, string jobNumber, string parentTaskCode, string taskGroupCode)
        {
            return this.FindBy(a => a.JOB_TASK_ID == taskId
                                 && a.DEPT_CODE == deptNo
                                 && a.JOB_NO == jobNumber
                                 && a.PARENT_TASK_CODE == parentTaskCode
                                 && a.TASK_GROUP_CODE == taskGroupCode
                                 ).FirstOrDefault();


        }
        public TR_JOB_TASK BindData(SaveJobTaskData trTaskData, TR_JOB_TASK data, UserAccess userAccess, ActionType actionType)
        {
            if (data == null)
                data = new TR_JOB_TASK();

            data.JOB_TASK_NAME = trTaskData.jobTaskName; 
            data.PROJECT_CODE = trTaskData.projectCode;
            data.COMP_CODE = trTaskData.compCode;
            data.JOB_NO = trTaskData.jobNo;
            data.NOTE = trTaskData.note;
            data.DEPT_CODE = trTaskData.deptCode;
            data.PLAN_START_DATE = trTaskData.planStartDate;
            data.PLAN_END_DATE = trTaskData.planEndDate;
            data.DUE_DATE = trTaskData.dueDate;
            data.PRIORITY_CODE = trTaskData.priorityCode;
            data.JOB_TASK_STATUS_CODE = trTaskData.jobTaskStatusCode;
            data.IS_ACTIVE = StatusYesNo.Yes.Value();
            data.TASK_GROUP_CODE = "T01";

            if (actionType == ActionType.Add)
            {
                TrDocRunning trDocRunning = new TrDocRunning();
                data.JOB_TASK_CODE = trDocRunning.GetDocumentRunning(string.Empty, trTaskData.deptCode, trTaskData.jobNo + trTaskData.deptCode, 0, 0, 3);
                data.CREATE_BY = userAccess.fullName;
                data.CREATE_DATE = DateTime.Now;
            }
            else
            {
                data.UPDATE_BY = userAccess.fullName;
                data.UPDATE_DATE = DateTime.Now;

            }

            return data;
        }

        public TR_JOB_TASK Save(SaveJobTaskData data, UserAccess userAccess, out bool resultStatus)
        {
            resultStatus = false;
            this.Data = this.GetDataByJobTarkCode(data.jobTaskCode);
            if (this.Data == null)
            {
                this.Data = this.BindData(data, this.Data, userAccess, ActionType.Add);
                this.Create(this.Data);
                resultStatus = true;
            }
            else
            {
                this.Data = this.BindData(data, this.Data, userAccess, ActionType.Edit);
                this.Edit("", this.Data);
                resultStatus = true;
            }

            return this.Data;
        }
    }
}