﻿using RRApiFramework;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Request.Customer;
using RRPlatFormAPI.Models.RepositoryModel.Master;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrTeamRole : AbRepository<TR_TEAM_ASSIGN>
    {
        public override TR_TEAM_ASSIGN GetData(string id)
        {
            return FindBy(r => r.TEAM_ASSIGN_ID.ToString() == id).FirstOrDefault();
        }

        public TR_TEAM_ASSIGN CheckDupicate(string teamCode ,string empNo)
        {
            return FindBy(r => r.TEAM_CODE == teamCode && r.EMP_NO == empNo ).FirstOrDefault();
        }

        public List<TR_TEAM_ASSIGN> GetDatasByEmpNo( string empNo)
        {
            return FindBy(r => r.EMP_NO == empNo && r.IS_ACTIVE == "Y").ToList();
        }

        public List<TR_TEAM_ASSIGN> GetDatasByTeamCode(List<string> teamCodeList)
        {
            if (teamCodeList.Count > 0)
            {
                return FindBy(r => teamCodeList.Contains(r.TEAM_CODE) && r.IS_ACTIVE == "Y").ToList();
            }
            return FindBy(r => r.IS_ACTIVE == "Y").ToList();
        }

        public override List<TR_TEAM_ASSIGN> GetDatas(string id)
        {
            return FindBy(r => r.TEAM_CODE == id &&  r.IS_ACTIVE == "Y").ToList();
        }

        public TR_TEAM_ASSIGN BindData(TeamRoleDatas teamData, TR_TEAM_ASSIGN data, UserAccess userAccess, ActionType actionType)
        {
            if (data == null)
                data = new TR_TEAM_ASSIGN();

            data.IS_MAIN = teamData.isMain;
            data.IS_ACTIVE = teamData.isActive;

            if (actionType == ActionType.Add)
            {
                data.EMP_NO = teamData.empNo;
                data.TEAM_CODE = teamData.teamCode;
                data.CREATE_BY = userAccess.fullName;
                data.CREATE_DATE = DateTime.Now;
            }
            else
            {
                data.UPDATE_BY = userAccess.fullName;
                data.UPDATE_DATE = DateTime.Now;

            }

            return data;
        }

        public TR_TEAM_ASSIGN Save(TeamRoleDatas data, UserAccess userAccess, out bool resultStatus)
        {
            resultStatus = false;
            this.Data = this.CheckDupicate(data.teamCode, data.empNo);
            if (this.Data == null)
            {
                this.Data = this.BindData(data, this.Data, userAccess, ActionType.Add);
                this.Create(this.Data);
                resultStatus = true;
            }
            else
            {
                this.Data = this.BindData(data, this.Data, userAccess, ActionType.Edit);
                this.Edit("", this.Data);
                resultStatus = true;
            }

            return this.Data;
        }

    }
  
}