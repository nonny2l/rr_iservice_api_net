﻿using RRApiFramework.Model;
using RRPlatFormModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel.Master
{
    public class MtTaskGroup : AbRepository<MT_TASK_GROUP>
    {
        public override MT_TASK_GROUP GetData(string id)
        {
            return FindBy(a => a.TASK_GROUP_ID.ToString() == id).FirstOrDefault();
        }

        public MT_TASK_GROUP GetDataByCode(string code)
        {
            return FindBy(a => a.TASK_GROUP_CODE == code).FirstOrDefault();

        }

        public override List<MT_TASK_GROUP> GetDatas(string id)
        {
            return FindBy(r => r.IS_ACTIVE == "Y" ).ToList();
        }
       
    }
}