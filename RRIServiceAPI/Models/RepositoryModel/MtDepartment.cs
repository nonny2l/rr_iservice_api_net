﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class MtDepartment : AbRepository<MT_DEPARTMENT>
    {
        public override MT_DEPARTMENT GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.DEPT_ID == idInt).FirstOrDefault();
        }
        public  MT_DEPARTMENT GetDataByCode(string deptCode)
        {
            return this.FindBy(a => a.DEPT_CODE == deptCode && a.IS_ACTIVE == "Y").FirstOrDefault();
        }
        public static string GetName(string deptCode)
        {
            string result = string.Empty;
            using (var rRIServiceModelContext = new RRIServiceModelContext())
            {
                var data = rRIServiceModelContext.MT_DEPARTMENT.Where(a => a.DEPT_CODE == deptCode).FirstOrDefault();
                if (data != null)
                    result = data.DEPT_NAME;
            }
            return result;
        }
        public override List<MT_DEPARTMENT> GetDatas(string id)
        {
            throw new NotImplementedException();
        }
    }
  
}