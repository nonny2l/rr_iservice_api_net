﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrStaffTeam : AbRepository<TR_STAFF_TEAM>
    {
        public override TR_STAFF_TEAM GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.STAFF_TEAM_ID == idInt).FirstOrDefault();
        }

        public List<string> GetUserIdList(int userId)
        {
            List<string> userIdList = new List<string>();

            List<TR_STAFF_TEAM> allData = this.GetAll().ToList();

            List<TR_STAFF_TEAM> userIdListResult = allData.Where(c => c.USER_ID == userId && c.IS_ACTIVE == "Y")
                    .Select(c => new TR_STAFF_TEAM
                    {
                        STAFF_TEAM_ID = c.STAFF_TEAM_ID,
                        PARENT_ID = c.PARENT_ID,
                        USER_ID = c.USER_ID,
                        USER_NAME = c.USER_NAME,
                        IS_ACTIVE = c.IS_ACTIVE,
                    })
                    .ToList();

            foreach (var userIdResult in userIdListResult)
            {
                GetChildren(userIdResult, allData, userIdList,(int)userIdResult.USER_ID);
            }

            return userIdList;
        }

        public List<string> GetChildren(TR_STAFF_TEAM userData, List<TR_STAFF_TEAM> allData, List<string> userIdList, int parentId)
        {
            List<TR_STAFF_TEAM> userIdListResult = allData.Where(c => c.PARENT_ID == parentId && c.IS_ACTIVE == "Y")
                     .Select(c => new TR_STAFF_TEAM
                     {
                         STAFF_TEAM_ID = c.STAFF_TEAM_ID,
                         PARENT_ID = c.PARENT_ID,
                         USER_ID = c.USER_ID,
                         USER_NAME = c.USER_NAME,
                         IS_ACTIVE = c.IS_ACTIVE,
                     })
                     .ToList();

            foreach (var userIdResult in userIdListResult)
            {
                userIdList.Add(userIdResult.USER_NAME);
                var resultIn = GetChildren(userIdResult, allData, userIdList,(int)userIdResult.USER_ID);
                
            }

            return userIdList;
        }

        public override List<TR_STAFF_TEAM> GetDatas(string id)
        {
            return this.FindBy(a => a.USER_ID.ToString() == id && a.IS_ACTIVE == "Y").ToList();
        }
    }
    
}