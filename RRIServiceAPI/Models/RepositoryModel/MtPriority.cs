﻿using RRApiFramework.Model;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel.Master
{
    public class MtPriority : AbRepository<MT_PRIORITY>
    {
        public override MT_PRIORITY GetData(string id)
        {
            return this.FindBy(a => a.PRIORITY_ID.ToString() == id).FirstOrDefault();
        }

        public  MT_PRIORITY GetDataByCode(string priorityCode)
        {
            return this.FindBy(a => a.PRIORITY_CODE == priorityCode).FirstOrDefault();
        }

        public override List<MT_PRIORITY> GetDatas(string id)
        {
            return this.FindBy(a => a.IS_ACTIVE == "Y").ToList();
        }

    }
}