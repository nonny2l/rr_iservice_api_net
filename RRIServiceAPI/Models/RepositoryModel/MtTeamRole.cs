﻿using RRApiFramework;
using RRApiFramework.Model;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel.Master
{
    public class MtTeamRole : AbRepository<MT_TEAM_ROLE>
    {
        public override MT_TEAM_ROLE GetData(string id)
        {
            return FindBy(a => a.TEAM_ROLE_ID.ToString() == id).FirstOrDefault();
        }

        public override List<MT_TEAM_ROLE> GetDatas(string id)
        {
            return FindBy(r => r.IS_ACTIVE == "Y" && r.EMP_NO.ToString() == id).ToList();
        }



        public List<TeamRoleData> GetUserIdList(string empNo)
        {
            List<TeamRoleData> teamRoleList = new List<TeamRoleData>();

            List<MT_TEAM_ROLE> allData = this.FindBy(r => r.IS_ACTIVE == "Y").ToList();

            List<MT_TEAM_ROLE> userIdListResult = allData.Where(c => c.EMP_NO == empNo && c.IS_ACTIVE == "Y")
                    .Select(c => new MT_TEAM_ROLE
                    {
                        TEAM_ROLE_ID = c.TEAM_ROLE_ID,
                        PARENT_EMP_NO = c.PARENT_EMP_NO,
                        EMP_NO = c.EMP_NO,
                        ROLE_NAME = c.ROLE_NAME,
                        DEPT_CODE = c.DEPT_CODE,
                        IS_ACTIVE = c.IS_ACTIVE,
                    })
                    .ToList();

            foreach (var userIdResult in userIdListResult)
            {
                TeamRoleData teamRoleData = new TeamRoleData();
                teamRoleData.deptCode = userIdResult.DEPT_CODE;
                teamRoleData.deptName = MtDepartment.GetName(userIdResult.DEPT_CODE);
                teamRoleData.empNo = userIdResult.EMP_NO;
                teamRoleData.empName = MtEmployee.GetName(userIdResult.EMP_NO);
                teamRoleList.Add(teamRoleData);
                GetChildren(userIdResult, allData, teamRoleList, userIdResult.EMP_NO);
            }

            return teamRoleList;
        }

        public List<TeamRoleData> GetChildren(MT_TEAM_ROLE userData, List<MT_TEAM_ROLE> allData, List<TeamRoleData> teamRoleDatas, string parentId)
        {
            List<MT_TEAM_ROLE> userIdListResult = allData.Where(c => c.PARENT_EMP_NO == parentId && c.IS_ACTIVE == "Y")
                     .Select(c => new MT_TEAM_ROLE
                     {
                         TEAM_ROLE_ID = c.TEAM_ROLE_ID,
                         PARENT_EMP_NO = c.PARENT_EMP_NO,
                         EMP_NO = c.EMP_NO,
                         DEPT_CODE = c.DEPT_CODE,
                         ROLE_NAME = c.ROLE_NAME,
                         IS_ACTIVE = c.IS_ACTIVE,
                     })
                     .ToList();

            foreach (var userIdResult in userIdListResult)
            {
                TeamRoleData teamRoleData = new TeamRoleData();
                teamRoleData.deptCode = userIdResult.DEPT_CODE;
                teamRoleData.deptName = MtDepartment.GetName(userIdResult.DEPT_CODE);
                teamRoleData.empNo = userIdResult.EMP_NO;
                teamRoleData.empName = MtEmployee.GetName(userIdResult.EMP_NO);
                teamRoleDatas.Add(teamRoleData);

                var resultIn = GetChildren(userIdResult, allData, teamRoleDatas, userIdResult.EMP_NO);
            }

            return teamRoleDatas;
        }

        public List<MT_TEAM_ROLE> GetTeamRoleByEmpNo(string empNo)
        {
            return this.FindBy(r => r.EMP_NO == empNo && r.IS_ACTIVE == "Y").ToList();
        }

    }
}