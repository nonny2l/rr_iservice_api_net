﻿using RRApiFramework.Model;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel.Master
{
    public class MtSubDistrict : AbRepository<MT_SUBDISTRICT>
    {
        public override MT_SUBDISTRICT GetData(string id)
        {
            return FindBy(a => a.SUB_DISTRICT_CODE == id).FirstOrDefault();
        }

        public override List<MT_SUBDISTRICT> GetDatas(string id)
        {
            return dbContext.MT_SUBDISTRICT.Where(r => r.IS_ACTIVE == "Y" || (r.SUB_DISTRICT_CODE == "" || r.SUB_DISTRICT_CODE == id)).ToList();
        }

        public List<MT_SUBDISTRICT> GetDatasByDistrictCode(string districtCode)
        {
            return dbContext.MT_SUBDISTRICT.Where(r => r.IS_ACTIVE == "Y" && r.REF_DISTRICT_CODE == districtCode).ToList();
        }
    }
}