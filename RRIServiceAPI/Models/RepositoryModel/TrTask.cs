﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request.Customer;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormAPI.Models.RepositoryModel.Master;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrTask : AbRepository<TR_TASK>
    {
        public override TR_TASK GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.TASK_ID == idInt).FirstOrDefault();
        }
        public  TR_TASK GetDataByTaskCode(string taskCode)
        {
            return this.FindBy(a => a.TASK_CODE == taskCode).FirstOrDefault();
        }
        public List<TR_TASK> GetDataByJobNo(string jobNo)
        {
            return this.FindBy(a => a.JOB_NO == jobNo && a.IS_ACTIVE == "Y").ToList();
        }

        public List<TR_TASK> GetDataByJobNo(List<string> jobNo)
        {
            return this.FindBy(a => jobNo.Contains(a.JOB_NO) && a.IS_ACTIVE == "Y").ToList();
        }

        public List<TR_TASK> GetDataByProjectCodeList(List<string> procjectCodeList)
        {
            List<TR_TASK> result = new List<TR_TASK>();
          
            if (procjectCodeList.Count != 0)
            {
                result = this.FindBy(a => procjectCodeList.Contains(a.PROJECT_CODE) && a.IS_ACTIVE == "Y").ToList();
            }
            else
            {
                result = this.FindBy(a =>  a.IS_ACTIVE == "Y").ToList();

            }
            return result;
        }

        public override List<TR_TASK> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public List<TR_TASK> GetDataByStartEndDate(DateTime? startDate ,DateTime? endDate,List<string> userList,List<string> projectCodeList) 
        {
            List<TR_TASK> result = new List<TR_TASK>();

            //if (startDate != null && endDate != null)
            //{
            //    result = this.FindBy(a => a.Start == jobNo && a.IS_ACTIVE == "Y").ToList();
            //}
            //else
            //{

            //}

            return result;
        }

        public TR_TASK GetDataCheckDupicate(int taskId, string taskCode, string jobNumber, string parentTaskCode, string taskGroupCode)
        {
            return this.FindBy(a => a.TASK_ID == taskId
                                 && a.TASK_CODE == taskCode
                                 && a.JOB_NO == jobNumber
                                 && a.TASK_GROUP_CODE == taskGroupCode
                                 ).FirstOrDefault();


        }
        public TR_TASK BindData(SaveTaskRequest trTaskData, TR_TASK data, UserAccess userAccess, ActionType actionType)
        {
            if (data == null)
                data = new TR_TASK();

            TrDocRunning trDocRunning = new TrDocRunning();

            data.TASK_NAME = trTaskData.taskName;
            data.JOB_NO = trTaskData.jobNo;
            data.TASK_GROUP_CODE = trTaskData.taskGroupCode;
            data.PRIORITY_CODE = trTaskData.priorityCode;
            data.PROJECT_CODE = trTaskData.projectCode;
            data.PLAN_START_DATE = trTaskData.planStartDate;
            data.PLAN_END_DATE = trTaskData.planEndDate;
            data.DUE_DATE = trTaskData.dueDate;
            data.NOTE = trTaskData.note;
            data.COMP_CODE = trTaskData.compCode;
            data.TASK_STATUS_CODE = trTaskData.taskStatusCode;
            data.ESTIMATED = trTaskData.estimated;

            //data.PROGRESS_STATUS_CODE = trTaskData.progressStatusCo;
            //data.HASHTAG = trTaskData.hashtag;
            //data.IS_ACTIVE = trTaskData.isActive;
            //data.ACTUAL_START_DATE = trTaskData.actualStartDate;
            //data.ACTUAL_END_DATE = trTaskData.actualEndDate;
            //data.PARENT_TASK_CODE = trTaskData.parentTaskCode;

            if (actionType == ActionType.Add)
            {
                data.TASK_CODE = trDocRunning.GetDocumentRunning(trTaskData.compCode, "", "T" + trTaskData.projectCode, DateTime.Now.Year, DateTime.Now.Month,3);
                data.CREATE_BY = userAccess.fullName;
                data.CREATE_DATE = DateTime.Now;
                data.IS_ACTIVE = StatusYesNo.Yes.Value(); 
            }
            else
            {
                data.UPDATE_BY = userAccess.fullName;
                data.UPDATE_DATE = DateTime.Now;
            }

            return data;
        }

        public TR_TASK Save(SaveTaskRequest data, UserAccess userAccess, out bool resultStatus)
        {
            resultStatus = false;
            this.Data = this.GetDataByTaskCode(data.taskCode);
            if (this.Data == null)
            {
                this.Data = this.BindData(data, this.Data, userAccess, ActionType.Add);
                this.Create(this.Data);
                resultStatus = true;
            }
            else
            {
                this.Data = this.BindData(data, this.Data, userAccess, ActionType.Edit);
                this.Edit("", this.Data);
                resultStatus = true;
            }

            return this.Data;
        }
    }
}