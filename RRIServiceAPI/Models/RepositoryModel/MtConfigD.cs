﻿using RRApiFramework.Model;
using RRPlatFormModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel.Master
{
    public class MtConfigD : AbRepository<MT_CONFIG_D>
    {
        public override MT_CONFIG_D GetData(string id)
        {
            return FindBy(a => a.CONFIG_D_ID.ToString() == id).FirstOrDefault();
        }

        public override List<MT_CONFIG_D> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public MT_CONFIG_D GetDataByConfigDCode(string configDCode)
        {
            return FindBy(r => r.CONFIG_D_CODE == configDCode && r.IS_ACTIVE == "Y").FirstOrDefault();
        }

        public static string GetName(string configDCode)
        {
            string result = string.Empty;
            using (var rRIServiceModelContext = new RRIServiceModelContext())
            {
                var data = rRIServiceModelContext.MT_CONFIG_D.Where(a => a.CONFIG_D_CODE == configDCode).FirstOrDefault();
                if (data != null)
                    result = data.CONFIG_D_ENAME;
            }
            return result;
        }

        public List<MT_CONFIG_D> GetDatasActiveByConfigHCode(string configHCode)
        {
            return dbContext.MT_CONFIG_D.Where(r =>  r.CONFIG_H_CODE == configHCode && r.IS_ACTIVE == "Y").ToList();
        }
    }
}