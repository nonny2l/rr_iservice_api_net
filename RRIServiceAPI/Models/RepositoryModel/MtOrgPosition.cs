﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class MtOrgPosition : AbRepository<MT_ORG_POSITION>
    {
        public override MT_ORG_POSITION GetData(string id)
        {
            return this.FindBy(a => a.ORG_POSI_ID.ToString() == id).FirstOrDefault();
        }
        public MT_ORG_POSITION GetDataByCode(string positionCode)
        {
            return this.FindBy(a => a.POSITION_CODE == positionCode && a.IS_ACTIVE == "Y").FirstOrDefault();
        }
        public override List<MT_ORG_POSITION> GetDatas(string deptCode)
        {
            return this.FindBy(a => a.DEPT_CODE == deptCode && a.IS_ACTIVE == "Y").ToList();
        }

        public List<MT_ORG_POSITION> GetDatasByDeptCode(List<string> deptCode)
        {
            return this.FindBy(a => deptCode.Contains(a.DEPT_CODE) && a.IS_ACTIVE == "Y").ToList();
        }
                    
        public static string GetName(string positionCode)
        {
            string result = string.Empty;
            using (var rRIServiceModelContext = new RRIServiceModelContext())
            {
                var data = rRIServiceModelContext.MT_ORG_POSITION.Where(a => a.POSITION_CODE == positionCode).FirstOrDefault();
                if (data != null)
                    result = data.POSITION_CODE;
            }
            return result;
        }
    }
  
}