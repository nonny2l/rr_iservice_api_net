﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request.Customer;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class MtProjectH : AbRepository<MT_PROJECT>
    {
        public override MT_PROJECT GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.PROJECT_ID == idInt).FirstOrDefault();
        }

        public List<MT_PROJECT> GetProjectList(GetProjectListRequest request)
        {
            List<MT_PROJECT> reponseList = new List<MT_PROJECT>();
            IQueryable<MT_PROJECT> query = this.FindBy(a => a.IS_ACTIVE == "Y");

            if (request.compCodeList.Count != 0)
            {
                query = query.Where(a => request.compCodeList.Contains(a.COMP_CODE));
            }

            reponseList = query.ToList();

            return reponseList;
        }

        public  MT_PROJECT GetDataByProjectCode(string projectCode)
        {
            return this.FindBy(a => a.PROJECT_CODE == projectCode).FirstOrDefault();
        }

        public MT_PROJECT GetDataByProjectCodeAndCompCode(string projectCode,string compCode)
        {
            return this.FindBy(a => a.PROJECT_CODE == projectCode && a.COMP_CODE == compCode).FirstOrDefault();
        }

        public List<MT_PROJECT> GetDataByProjectCodeAndCompCodeList(List<string> projectCodes, List<string> compCodes)
        {
            return this.FindBy(a => projectCodes.Contains( a.PROJECT_CODE) && compCodes.Contains(a.COMP_CODE)).ToList();
        }

        public override List<MT_PROJECT> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public static string GetName(string serviceCode)
        {
            string result = string.Empty;
            using (var rRIServiceModelContext = new RRIServiceModelContext())
            {
                var data = rRIServiceModelContext.MT_PROJECT.Where(a => a.PROJECT_CODE == serviceCode).FirstOrDefault();
                if (data != null)
                    result = data.PROJECT_NAME;
            }
            return result;
        }
        public static string GetName(string serviceCode, string compCode)
        {
            string result = string.Empty;
            using (var rRIServiceModelContext = new RRIServiceModelContext())
            {
                var data = rRIServiceModelContext.MT_PROJECT.Where(a => a.PROJECT_CODE == serviceCode && a.COMP_CODE == compCode).FirstOrDefault();
                if (data != null)
                    result = data.PROJECT_NAME;
            }
            return result;
        }

        public List<string> GetJobListFilter(GetJobListRequest request)
        {
            List<string> result = new List<string>();

            List<MT_PROJECT> dataList = this.FindBy(a => a.IS_ACTIVE == "Y").ToList();

         

          

            result = dataList.Select(a => a.PROJECT_CODE).Distinct().ToList();

            return result;
        }
    }
  
}