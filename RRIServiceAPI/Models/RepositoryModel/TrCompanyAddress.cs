﻿using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRPlatFormAPI.HTTP.Request.Customer;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel.Customer
{
    public class TrCompanyAddress : AbRepository<TR_COMPANY_ADDRESS>
    {
        public override TR_COMPANY_ADDRESS GetData(string id)
        {
            return FindBy(r => r.COMP_ADDR_ID.ToString() == id).FirstOrDefault();
        }

        public override List<TR_COMPANY_ADDRESS> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public List<TR_COMPANY_ADDRESS> GetDatasActiveByCompId(string id)
        {
            return dbContext.TR_COMPANY_ADDRESS.Where(r => r.IS_ACTIVE == "Y").ToList();
        }

        public TR_COMPANY_ADDRESS Save(TrCompanyAddressRequest request)
        {
            this.Data = this.GetData(request.compId.ToString());
            if (this.Data == null)
            {
                this.Data = this.BindData(request, this.Data, ActionType.Add);
                this.Create(this.Data);
            }
            else
            {
                this.Data = this.BindData(request, this.Data, ActionType.Edit);
                this.Edit(request.compId.ToString(), this.Data);
            }

            return this.Data;
        }

        internal TR_COMPANY_ADDRESS BindData(TrCompanyAddressRequest model, TR_COMPANY_ADDRESS data, ActionType actionType)
        {
            if (data == null)
                data = new TR_COMPANY_ADDRESS();

            data.IS_ACTIVE = model.isActive;
            data.ADDRESS = model.address;
            data.COMP_ADDR_ID = model.compId;
            data.DISTRICT_CODE = model.districtCode;
            data.IS_MAIN = model.isMain;
            data.PROV_CODE = model.provCode;
            data.SUB_DISTRICT_CODE = model.subDistrictCode;
            data.ZIPCODE = model.zipcode;
            data.ZONE = model.zone;

            if (actionType == ActionType.Add)
            {
                data.CREATE_BY = model.createBy;
                data.CREATE_DATE = DateTime.Now;
            }
            else
            {
                data.UPDATE_BY = model.updateBy;
                data.CREATE_DATE = DateTime.Now;
            }
            return data;
        }

        public List<TrCompanyAddressResponse> GetCustomerAddressByCompIdList(string id)
        {
            var data = (from a in dbContext.TR_COMPANY_ADDRESS
                        join b in dbContext.MT_PROVINCE on a.PROV_CODE equals b.PROV_CODE into bb
                        from b in bb.DefaultIfEmpty()
                        join c in dbContext.MT_DISTRICT on a.DISTRICT_CODE equals c.DISTRICT_CODE into cc
                        from c in cc.DefaultIfEmpty()
                        join d in dbContext.MT_SUBDISTRICT on a.SUB_DISTRICT_CODE equals d.SUB_DISTRICT_CODE into dd
                        from d in dd.DefaultIfEmpty()
                        where (a.COMP_ADDR_ID.ToString() == id || id == "") && a.IS_ACTIVE == "Y"
                        select new TrCompanyAddressResponse
                        {
                            address = a.ADDRESS,
                            compAddrId = a.COMP_ADDR_ID,
                            districtCode = a.DISTRICT_CODE,
                            subDistrictCode = a.SUB_DISTRICT_CODE,
                            provCode = a.PROV_CODE,
                            districtName = c.DISTRICT_TNAME,
                            isActive = a.IS_ACTIVE,
                            isMain = a.IS_MAIN,
                            provName = b.PROV_TNAME,
                            subDistrictName = d.SUB_DISTRICT_TNAME,
                            zipcode = a.ZIPCODE,
                            zone = a.ZONE,
                            createBy = a.CREATE_BY,
                            createDate = a.CREATE_DATE,
                            updateBy = a.UPDATE_BY,
                            updateDate = a.UPDATE_DATE,
                            deleteBy = a.DELETE_BY,
                            deleteDate = a.DELETE_DATE,
                        }).ToList();
            return data;
        }
    }
}