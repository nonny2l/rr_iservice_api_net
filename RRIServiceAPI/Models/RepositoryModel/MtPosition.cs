﻿using RRApiFramework.Model;
using RRApiFramework.Utility;
using RRPlatFormModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class MtPosition : AbRepository<MT_POSITION>
    {
        public override MT_POSITION GetData(string positionCode)
        {
            return this.FindBy(a => a.POSITION_CODE == positionCode).FirstOrDefault();
        }

        public override List<MT_POSITION> GetDatas(string positionCode)
        {
            return this.FindBy(a => a.POSITION_CODE == positionCode).ToList();
        }

        public static string GetName(string positionCode)
        {
            string result = string.Empty;
            using (var rRIServiceModelContext = new RRIServiceModelContext())
            {
                var data = rRIServiceModelContext.MT_POSITION.Where(a => a.POSITION_CODE == positionCode).FirstOrDefault();
                if (data != null)
                    result = data.POSITION_NAME;
            }
            return result;
        }

    }
}