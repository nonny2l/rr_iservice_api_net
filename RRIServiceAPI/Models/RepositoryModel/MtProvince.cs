﻿using RRApiFramework.Model;
using RRPlatFormAPI.HTTP.Response.Master;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel.Master
{
    public class MtProvince : AbRepository<MT_PROVINCE>
    {
        public List<ProvinceResponse> GetAllGrouped()
        {
            return DbContext.MT_PROVINCE.Where(r => r.IS_ACTIVE == "Y").GroupBy(a => new { a.PROV_CODE, a.PROV_REGION })
                 .Select(a => new ProvinceResponse
                 {
                     provCode = a.Key.PROV_CODE,
                     provTName = a.Select(b => b.PROV_TNAME).FirstOrDefault(),
                     provEName = a.Select(b => b.PROV_ENAME).FirstOrDefault(),
                     isActive = a.Select(b => b.IS_ACTIVE).FirstOrDefault(),
                     provRegion = a.Key.PROV_REGION,
                 }).OrderBy(a => a.provTName).ToList();
        }

        public override MT_PROVINCE GetData(string id)
        {
            return FindBy(a => a.PROV_CODE == id).FirstOrDefault();
        }

        public override List<MT_PROVINCE> GetDatas(string id)
        {
            return dbContext.MT_PROVINCE.Where(a => a.IS_ACTIVE == "Y" || (a.PROV_CODE == "" || a.PROV_CODE == id)).ToList();
        }
    }
}