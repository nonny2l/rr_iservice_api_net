﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class MtUserMenuPermission : AbRepository<MT_USER_MENU_PERMISSION>
    {
        public MtUserMenuPermission()
        {
            this.Data = new MT_USER_MENU_PERMISSION();
            this.Datas = new List<MT_USER_MENU_PERMISSION>();
        }

        public override MT_USER_MENU_PERMISSION GetData(string id)
        {
            int idInt = Convert.ToInt32(id);
            var data = (from d in DbContext.MT_USER_MENU_PERMISSION
                        where d.ID == idInt
                                 select d).FirstOrDefault();

            return data;
        }
        public MT_USER_MENU_PERMISSION BindDataSave(int userId, string active, int menuId,string userName, MT_USER_MENU_PERMISSION data, ActionType action)
        {
            if (data == null)
                data = new MT_USER_MENU_PERMISSION();

            data.MENU_ID = menuId;
            data.USER_ID = userId;
            data.IS_ACTIVE = active;

            if (action == ActionType.Add)
            {
                data.CREATE_BY = userName;
                data.CREATE_DATE = DateTime.Now;
            }
            else
            {
                data.UPDATE_BY = userName;
                data.UPDATE_DATE = DateTime.Now;
            }

            return data;
        }

        public override List<MT_USER_MENU_PERMISSION> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public bool SaveById(int userId, int menuId,string active,string userName)
        {
            bool result = false;
            this.Data = this.FindBy(a => a.MENU_ID == menuId && a.USER_ID == userId).FirstOrDefault();
            if (this.Data == null)
            {
                this.Data = this.BindDataSave(userId, active, menuId, userName, this.Data, ActionType.Add);
                this.Create(this.Data);
            }
            else
            {
                this.Data = this.BindDataSave(userId, active, menuId, userName, this.Data, ActionType.Edit);
                this.Edit("",this.Data);
            }


            return result;
        }
   
    }
}