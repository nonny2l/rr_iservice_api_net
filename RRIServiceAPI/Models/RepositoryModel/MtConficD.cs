﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class MtConficD : AbRepository<MT_CONFIG_D>
    {
        public override MT_CONFIG_D GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.CONFIG_D_ID == idInt).FirstOrDefault();
        }
        public MT_CONFIG_D GetDataByDCode(string configDCode)
        {
            return this.FindBy(a => a.CONFIG_D_CODE == configDCode && a.IS_ACTIVE == "Y").FirstOrDefault();
        }

        public List<MT_CONFIG_D> GetDatasByHCode(string configHCode)
        {
            return this.FindBy(a => a.CONFIG_H_CODE == configHCode && a.IS_ACTIVE == "Y").OrderBy(a => a.CONFIG_D_SEQ).ToList();
        }

        public static string GetEName(string code)
        {
            string result = string.Empty;
            using (var rRIServiceModelContext = new RRIServiceModelContext())
            {
                var data = rRIServiceModelContext.MT_CONFIG_D.Where(a => a.CONFIG_D_CODE == code).FirstOrDefault();
                if (data != null)
                    result = data.CONFIG_D_ENAME;
            }
            return result;
        }
        public static string GetEName(string code, string masterCode)
        {
            string result = string.Empty;
            using (var rRIServiceModelContext = new RRIServiceModelContext())
            {
                var data = rRIServiceModelContext.MT_CONFIG_D.Where(a => a.CONFIG_D_CODE == code && a.CONFIG_H_CODE == masterCode).FirstOrDefault();
                if (data != null)
                    result = data.CONFIG_D_ENAME;
            }
            return result;
        }
        public static string GetTName(string code)
        {
            string result = string.Empty;
            using (var rRIServiceModelContext = new RRIServiceModelContext())
            {
                var data = rRIServiceModelContext.MT_CONFIG_D.Where(a => a.CONFIG_D_CODE == code).FirstOrDefault();
                if (data != null)
                    result = data.CONFIG_D_TNAME;
            }
            return result;
        }

        public MtConficDDto BindDataDto(MT_CONFIG_D dataBind)
        {
            MtConficDDto data = new MtConficDDto();
            data.mtConfigDId = dataBind.CONFIG_D_ID.ToString();
            data.mtConfigDCode = dataBind.CONFIG_D_CODE;
            data.mtConfigHCode = dataBind.CONFIG_H_CODE;
            data.mtConfigDSeq = dataBind.CONFIG_D_SEQ;
            data.mtConfigDTName = dataBind.CONFIG_D_TNAME;
            data.mtConfigDEName = dataBind.CONFIG_D_ENAME;
            data.parentDId = dataBind.PARENT_D_ID.ToString();
            data.isActive = dataBind.IS_ACTIVE;
            data.createBy = dataBind.CREATE_BY;
            data.createDate = dataBind.CREATE_DATE;
            data.updateBy = dataBind.UPDATE_BY;
            data.updateDate = dataBind.UPDATE_DATE;

            return data;
        }

      
        public override List<MT_CONFIG_D> GetDatas(string id)
        {
            return this.FindBy(a => a.CONFIG_H_CODE == id && a.IS_ACTIVE == "Y").OrderBy(a => a.CONFIG_D_SEQ).ToList();
        }

        public List<GetMasterListResponse> GetChildParent(List<GetMasterListResponse> listResponse,string parentDId)
        {
            return listResponse.Where(c => c.parentDId == parentDId && c.isActive == "Y")
                    .Select(dataBind => new GetMasterListResponse
                    {
                        mtConfigDId = dataBind.mtConfigDId,
                        mtConfigDCode = dataBind.mtConfigDCode,
                        mtConfigHCode = dataBind.mtConfigHCode,
                        mtConfigDSeq = dataBind.mtConfigDSeq,
                        mtConfigDTName = dataBind.mtConfigDTName,
                        mtConfigDEName = dataBind.mtConfigDEName,
                        parentDId = dataBind.parentDId,
                        isActive = dataBind.isActive,
                        createBy = dataBind.createBy,
                        createDate = dataBind.createDate,
                        updateBy = dataBind.updateBy,
                        updateDate = dataBind.updateDate,
                        child = GetChildParent(listResponse, dataBind.mtConfigDId)

                    }).OrderBy(a => a.mtConfigDSeq).ToList();
        }
    }
    public class MtConficDDto : MasterDto
    {
        public string mtConfigDId { get; set; }
        public string mtConfigDCode { get; set; }
        public string mtConfigHCode { get; set; }
        public int mtConfigDSeq { get; set; }
        public string mtConfigDTName { get; set; }
        public string mtConfigDEName { get; set; }
        public string parentDId { get; set; }

    }
}