﻿using RRApiFramework.Model;
using RRApiFramework.Utility;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrCompanyHeadCs : AbRepository<TR_COMPANY_HEAD_CS>
    {
        public override TR_COMPANY_HEAD_CS GetData(string id)
        {
            throw new NotImplementedException();
        }

        public override List<TR_COMPANY_HEAD_CS> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public List<TR_COMPANY_HEAD_CS> GetDatasActive() => FindBy(r => r.IS_ACTIVE == "Y").ToList();
        public List<TR_COMPANY_HEAD_CS> GetDatasByCompCode(List<string> compCodeList)
        {
            if (compCodeList.Count > 0)
            {
                return FindBy(r => compCodeList.Contains(r.COMP_CODE) && r.IS_ACTIVE == "Y").ToList();
            }
            return GetDatasActive();
        }
        public List<TR_COMPANY_HEAD_CS> GetDatasByCompCode(string compCode)
        {
            if (!compCode.IsNullOrEmptyWhiteSpace())
            {
                return FindBy(r => r.COMP_CODE == compCode && r.IS_ACTIVE == "Y").ToList();
            }
            return GetDatasActive();
        }
    }
}