﻿using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel.Master
{
    public class TrDocRunning : AbRepository<TR_DOC_RUNNING>
    {
        public override TR_DOC_RUNNING GetData(string id)
        {
            return this.FindBy(a => a.DOCUMENT_RUNNING_ID.ToString() == id).FirstOrDefault();
        }

        public override List<TR_DOC_RUNNING> GetDatas(string id)
        {
            return this.FindBy(a => a.IS_ACTIVE == "Y").ToList();
        }

        public string GetDocumentRunning(string comCode ,string deptCode ,string prefixCode ,int year ,int month,int runningDigit )
        {
            string result = string.Empty;
            TR_DOC_RUNNING data = new TR_DOC_RUNNING();

            string runningFormatDigit = "0";
            for (int i = 1; i < runningDigit; i++)
            {
                runningFormatDigit = runningFormatDigit + "0";
            }

            

            var docRunningDataList = this.FindBy(a => 
                                                 a.COM_CODE == comCode
                                              && a.DEPT_CODE == deptCode
                                              && a.PREFIX_CODE == prefixCode
                                              && a.PREFIX_YEAR == year
                                              && a.PREFIX_MONTH == month).ToList();

            if (docRunningDataList.Count == 0)
            {
                data.COM_CODE = comCode;
                data.DEPT_CODE = deptCode;
                data.PREFIX_CODE = prefixCode;
                data.PREFIX_YEAR = year;
                data.PREFIX_MONTH = month;
                data.DOCUMENT_RUNNING = 1;
                data.IS_ACTIVE = StatusYesNo.Yes.Value();
                data.CREATE_BY = "admin";
                data.CREATE_DATE = DateTime.Now;
                this.Create(data);

                result = data.PREFIX_CODE + data.PREFIX_YEAR.ToString() + data.PREFIX_MONTH.ToString("00") + data.DOCUMENT_RUNNING.ToString(runningFormatDigit);
            }
            else
            {
                data = this.FindBy(a => a.COM_CODE == comCode
                                                      && a.DEPT_CODE == deptCode
                                                      && a.PREFIX_CODE == prefixCode
                                                      && a.PREFIX_YEAR == year
                                                      && a.PREFIX_MONTH == month).OrderByDescending(a => a.DOCUMENT_RUNNING).FirstOrDefault();

                int docRunning = data.DOCUMENT_RUNNING + 1;
                result = data.PREFIX_CODE + data.PREFIX_YEAR.ToString() + data.PREFIX_MONTH.ToString("00") + docRunning.ToString(runningFormatDigit);

                data.DOCUMENT_RUNNING = docRunning;
                this.Edit("", data);
            }

            return result;
        }


    }
}