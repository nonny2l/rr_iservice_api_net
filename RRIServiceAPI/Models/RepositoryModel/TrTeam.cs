﻿using RRApiFramework;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Request.Customer;
using RRPlatFormAPI.Models.RepositoryModel.Master;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrTeam : AbRepository<TR_TEAM>
    {
        public override TR_TEAM GetData(string id)
        {
            return FindBy(r => r.TEAM_ID.ToString() == id).FirstOrDefault();
        }

        public override List<TR_TEAM> GetDatas(string id)
        {
            return FindBy(r =>  r.IS_ACTIVE == "Y").ToList();
        }

        public TR_TEAM GetDataByCode(string teamCode)
        {
            return FindBy(r => r.TEAM_CODE == teamCode && r.IS_ACTIVE == "Y").FirstOrDefault();
        }

     

        public TR_TEAM GetDataByJobNoAndDeptCode(string jobNo,string deptCode)
        {
            return FindBy(r => r.JOB_NO == jobNo && r.DEPT_CODE == deptCode &&  r.IS_ACTIVE == "Y").FirstOrDefault();
        }
        public TR_TEAM GetDataByJobTaskCode(string taskCode)
        {
            return FindBy(r => r.JOB_TASK_CODE == taskCode && r.IS_ACTIVE == "Y").FirstOrDefault();
        }

        public TR_TEAM GetDataByTeamCode(string teamCode)
        {
            return FindBy(r => r.TEAM_CODE == teamCode && r.IS_ACTIVE == "Y").FirstOrDefault();
        }

        public List<TR_TEAM> GetDataByJobNo(List<string> jobNoList)
        {
            if (jobNoList.Count > 0)
            {
                return FindBy(r => jobNoList.Contains(r.JOB_NO) && r.IS_ACTIVE == "Y").ToList();
            }
            return GetDatas(string.Empty);
        }

        public List<TR_TEAM> GetDataByJobNoAndDeptCodeList(string jobNo, List<string> deptCodeList)
        {
            return FindBy(r => r.JOB_NO == jobNo && deptCodeList.Contains(r.DEPT_CODE) && r.IS_ACTIVE == "Y").ToList();
        }




        public TR_TEAM BindData(TeamData teamData, TR_TEAM data, UserAccess userAccess, ActionType actionType)
        {
            if (data == null)
                data = new TR_TEAM();

            data.TEAM_NAME = teamData.teamName;
            data.JOB_NO = teamData.jobNo;
            data.DEPT_CODE = teamData.deptCode;
            data.JOB_TASK_CODE = teamData.jobTaskCode;

            if (actionType == ActionType.Add)
            {
                TrDocRunning trDocRunning = new TrDocRunning(); 
                

                data.TEAM_CODE = trDocRunning.GetDocumentRunning(teamData.deptCode, string.Empty, teamData.deptCode, 0, 0, 3);
                data.IS_ACTIVE = StatusYesNo.Yes.Value();
                data.CREATE_BY = userAccess.fullName;
                data.CREATE_DATE = DateTime.Now;
            }
            else
            {
                data.UPDATE_BY = userAccess.fullName;
                data.UPDATE_DATE = DateTime.Now;

            }

            return data;
        }

        public TR_TEAM Save(TeamData data, UserAccess userAccess, out bool resultStatus)
        {
            resultStatus = false;
            this.Data = this.GetDataByJobTaskCode(data.jobTaskCode);
            if (this.Data == null)
            {
                this.Data = this.BindData(data, this.Data, userAccess, ActionType.Add);
                this.Create(this.Data);
                resultStatus = true;
            }
            else
            {
                this.Data = this.BindData(data, this.Data, userAccess, ActionType.Edit);
                this.Edit("", this.Data);
                resultStatus = true;
            }

            return this.Data;
        }

        public bool SaveTeamJobAssign(List<string> deptCodeList,string jobNo, UserAccess userAccess, string jobTaskCode)
        {
            bool result = false;
            MtEmployeePosition mtEmployeePosition = new MtEmployeePosition();
            TrTeamRole trTeamRole = new TrTeamRole();
            foreach (var deptCode in deptCodeList)
            {
                TeamData teamData = new TeamData();
                
                teamData.deptCode = deptCode;
                teamData.jobNo = jobNo;
                teamData.jobTaskCode = jobTaskCode;
                bool resultTrTeam = false;
                var teamDataSave = this.Save(teamData, userAccess, out resultTrTeam);
                if (resultTrTeam)
                {
                    mtEmployeePosition.Datas = mtEmployeePosition.FindBy(a => a.DEPT_CODE == deptCode && a.POSITION_CODE == "DH").ToList();
                    foreach (var mtEmployeePositionData in mtEmployeePosition.Datas)
                    {
                        TeamRoleDatas teamRoleDatas = new TeamRoleDatas();
                        teamRoleDatas.teamCode = teamDataSave.TEAM_CODE;
                        teamRoleDatas.empNo = mtEmployeePositionData.EMP_NO;
                        teamRoleDatas.isMain = StatusYesNo.Yes.Value();
                        teamRoleDatas.isActive = StatusYesNo.Yes.Value();
                        bool resultTrTeamRole = false;
                        trTeamRole.Save(teamRoleDatas, userAccess,out resultTrTeamRole);
                    }
                }
            }
            return result;
        }





    }

}