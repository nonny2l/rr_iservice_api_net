﻿using RRApiFramework.Model;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel.Master
{
    public class MtConfigH : AbRepository<MT_CONFIG_H>
    {
        public override MT_CONFIG_H GetData(string id)
        {
            throw new NotImplementedException();
        }

        public override List<MT_CONFIG_H> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public List<MT_CONFIG_H> GetDatasActive()
        {
            return dbContext.MT_CONFIG_H.Where(r => r.IS_ACTIVE == "Y").ToList();
        }
    }
}