﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrNotifyUserRole : AbRepository<TR_NOTIFY_USER_ROLE>
    {
        public override TR_NOTIFY_USER_ROLE GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.ID == idInt).FirstOrDefault();
        }

        public List<TR_NOTIFY_USER_ROLE> GetDatasByEmployee(string id)
        {
            return this.FindBy(a => a.EMP_NO == id && a.IS_ACTIVE == "Y").ToList();
        }
        public override List<TR_NOTIFY_USER_ROLE> GetDatas(string id)
        {
            return this.FindBy(a => a.NOTIFY_MSG_CODE == id && a.IS_ACTIVE == "Y").ToList();
        }

   
    }
   
}