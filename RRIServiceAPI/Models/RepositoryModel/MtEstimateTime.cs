﻿using RRApiFramework.Model;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel.Master
{
    public class MtEstimateTime : AbRepository<MT_ESTIMATED_TIME>
    {
        public override MT_ESTIMATED_TIME GetData(string id)
        {
            return this.FindBy(a => a.ESTIMATED_ID.ToString() == id).FirstOrDefault();
        }

        public  MT_ESTIMATED_TIME GetDataByCode(string estimateCode)
        {
            return this.FindBy(a => a.ESTIMATED_CODE == estimateCode && a.IS_ACTIVE == "Y").FirstOrDefault();
        }

        public override List<MT_ESTIMATED_TIME> GetDatas(string id)
        {
            return this.FindBy(a => a.IS_ACTIVE == "Y").ToList();
        }

    }
}