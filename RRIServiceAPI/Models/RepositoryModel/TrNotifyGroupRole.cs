﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrNotifyGroupRole : AbRepository<TR_NOTIFY_GROUP_ROLE>
    {
        public override TR_NOTIFY_GROUP_ROLE GetData(string id)
        {
            return this.FindBy(a => a.ID.ToString() == id).FirstOrDefault();
        }

        public List<TR_NOTIFY_GROUP_ROLE> GetDatasByGroupCode(string id)
        {
            return this.FindBy(a => a.NOTIFY_GROUP_CODE == id && a.IS_ACTIVE == "Y").ToList();
        }
        public override List<TR_NOTIFY_GROUP_ROLE> GetDatas(string id)
        {
            return this.FindBy(a => a.NOTIFY_MSG_CODE == id && a.IS_ACTIVE == "Y").ToList();
        }

   
    }
   
}