﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrTaskDocument : AbRepository<TR_TASK_DOCUMENT>
    {
        public override TR_TASK_DOCUMENT GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.DOCUMENT_ID == idInt).FirstOrDefault();
        }

    
        public override List<TR_TASK_DOCUMENT> GetDatas(string id)
        {
            return this.FindBy(a => a.TASK_CODE == id && a.IS_ACTIVE == "Y").ToList();
        }

        public TR_TASK_DOCUMENT BindData(TaskDocumentDto trTaskData, TR_TASK_DOCUMENT data, UserAccess userAccess, ActionType actionType)
        {
            if (data == null)
                data = new TR_TASK_DOCUMENT();

            data.TASK_CODE = trTaskData.taskCode;
            data.DOCUMENT_ID = trTaskData.documentId;
            data.DOCUMENT_NAME = trTaskData.documentName;
            data.DOCUMENT_URL = trTaskData.documentUrl;
            data.IS_ACTIVE = trTaskData.isActive;

            if (actionType == ActionType.Add)
            {
                data.CREATE_BY = userAccess.fullName;
                data.CREATE_DATE = DateTime.Now;
            }
            else
            {
                data.UPDATE_BY = userAccess.fullName;
                data.UPDATE_DATE = DateTime.Now;

            }

            return data;
        }

        public TR_TASK_DOCUMENT Save(TaskDocumentDto data, UserAccess userAccess, out bool resultStatus)
        {
            resultStatus = false;
            this.Data = this.GetData(data.documentId.ToString());
            if (this.Data == null)
            {
                this.Data = this.BindData(data, this.Data, userAccess, ActionType.Add);
                this.Create(this.Data);
                resultStatus = true;
            }
            else
            {
                this.Data = this.BindData(data, this.Data, userAccess, ActionType.Edit);
                this.Edit("", this.Data);
                resultStatus = true;
            }

            return this.Data;
        }

    }
    public class TaskDocumentDto
    {

        public int documentId { get; set; }
        public string documentName { get; set; }
        public string documentUrl { get; set; }
        public string taskCode { get; set; }
        public string isActive { get; set; }
    }

}