﻿using RRApiFramework;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRPlatFormAPI.HTTP.Request.Customer;
using RRPlatFormAPI.Models.RepositoryModel.Master;
using RRPlatFormModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel.Customer
{
    public class MtPerson : AbRepository<MT_PERSON>
    {
        public override MT_PERSON GetData(string id)
        {
            return FindBy(R => R.PERSON_ID.ToString() == id).FirstOrDefault();
        }

        public override List<MT_PERSON> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public static string GetName(string code)
        {
            string result = string.Empty;
            using (var rRIServiceModelContext = new RRIServiceModelContext())
            {
                var data = rRIServiceModelContext.MT_PERSON.Where(a => a.PERSON_CODE == code).FirstOrDefault();
                if (data != null)
                    result = data.FIRST_NAME + " " + data.LAST_NAME ;
            }
            return result;
        }

        public MT_PERSON Save(SaveContactRequest request,UserAccess userAccess ,out bool statusResult)
        {
            this.Data = this.GetData(request.personId.ToString());
            if (this.Data == null)
            {
                this.Data = this.BindData(request, this.Data, userAccess,ActionType.Add);
                this.Create(this.Data);
                statusResult = true;
            }
            else
            {
                this.Data = this.BindData(request, this.Data, userAccess, ActionType.Edit);
                this.Edit(request.personId.ToString(), this.Data);
                statusResult = true;
            }

            return this.Data;
        }

        internal MT_PERSON BindData(SaveContactRequest model, MT_PERSON data,UserAccess userAccess,ActionType actionType)
        {
            if (data == null)
                data = new MT_PERSON();

            data.IS_ACTIVE = model.isActive;
            data.CITIZEN_ID = model.citizenId;
            data.PRE_NAME_CODE = model.preNameCode;
            data.FIRST_NAME = model.firstName;
            data.LAST_NAME = model.lastName;
            data.LINE_ID = model.lineId;
            data.MOBILE = model.mobile;
            data.PASSPORT = model.passport;
            data.TELEPHONE = model.telephone;
            data.EMAIL = model.email;
        
            if (actionType == ActionType.Add)
            {
                TrDocRunning trDocRunning = new TrDocRunning();
                data.PERSON_CODE = trDocRunning.GetDocumentRunning(string.Empty, string.Empty, "PS" , 0, 0, 5);
                data.CREATE_BY = userAccess.fullName;
                data.CREATE_DATE = DateTime.Now;
            }
            else
            {
                data.UPDATE_BY = userAccess.fullName;
                data.CREATE_DATE = DateTime.Now;
            }
            return data;
        }
    }
}