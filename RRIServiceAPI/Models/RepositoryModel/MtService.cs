﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class MtService : AbRepository<MT_SERVICE>
    {
        public override MT_SERVICE GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.SERVICE_ID == idInt).FirstOrDefault();
        }

        public MT_SERVICE GetDataByServiceCode(string serviceCode)
        {
            return this.FindBy(a => a.SERVICE_CODE == serviceCode).FirstOrDefault();
        }

        public static string GetName(string serviceCode)
        {
            string result = string.Empty;
            using (var rRIServiceModelContext = new RRIServiceModelContext())
            {
                var data = rRIServiceModelContext.MT_SERVICE.Where(a => a.SERVICE_CODE == serviceCode).FirstOrDefault();
                if (data != null)
                    result = data.SERVICE_NAME;
            }
            return result;
        }

        public override List<MT_SERVICE> GetDatas(string id)
        {
            throw new NotImplementedException();
        }
    }
  
}