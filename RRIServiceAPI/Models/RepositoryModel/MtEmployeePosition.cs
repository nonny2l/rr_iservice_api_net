﻿using RRApiFramework;
using RRApiFramework.Model;
using RRApiFramework.Utility;
using RRPlatFormModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel.Master
{

    public class MtEmployeePosition : AbRepository<MT_EMPLOYEE_POSITION>
    {
        public override MT_EMPLOYEE_POSITION GetData(string id)
        {
            int idInt = id.ToInt32();
            return FindBy(a => a.EMP_POSITION_ID == idInt).FirstOrDefault();
        }
        public override List<MT_EMPLOYEE_POSITION> GetDatas(string id)
        {
            int idInt = id.ToInt32();
            return FindBy(a => a.EMP_POSITION_ID == idInt).ToList();
        }

        public List<MT_EMPLOYEE_POSITION> GetDatasByEmpNo(string EMP_NO)
        {
            return this.DbContext.MT_EMPLOYEE_POSITION.Where(a => a.EMP_NO == EMP_NO).ToList();
        }

        public List<MT_EMPLOYEE_POSITION> GetDatasByDeptCode(string DEPT_CODE)
        {
            return this.DbContext.MT_EMPLOYEE_POSITION.Where(a => a.DEPT_CODE == DEPT_CODE && a.IS_ACTIVE == "Y").ToList();
        }
        public List<MT_EMPLOYEE_POSITION> GetDatasByDeptCode(string DEPT_CODE, string POSITION_CODE)
        {
            return this.DbContext.MT_EMPLOYEE_POSITION.Where(a => a.DEPT_CODE == DEPT_CODE && a.POSITION_CODE == POSITION_CODE).ToList();
        }

        public List<MT_EMPLOYEE_POSITION> GetDatasByDeptCodeListPositioCodeList(List<string> deptCodeList, List<string> positionCodeList)
        {
            return this.DbContext.MT_EMPLOYEE_POSITION.Where(a => deptCodeList.Contains(a.DEPT_CODE) && positionCodeList.Contains(a.POSITION_CODE) && a.IS_ACTIVE == "Y").ToList();
        }

        public List<MT_EMPLOYEE_POSITION> GetDatasByDeptCodeList(List<string> deptCodeList)
        {
            if (deptCodeList.Count > 0)
            {
                return this.DbContext.MT_EMPLOYEE_POSITION.Where(a => deptCodeList.Contains(a.DEPT_CODE) && a.IS_ACTIVE == "Y").ToList();
            }
            return this.DbContext.MT_EMPLOYEE_POSITION.Where(a => a.IS_ACTIVE == "Y").ToList();
        }
    }

}