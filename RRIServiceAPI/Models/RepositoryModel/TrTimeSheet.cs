﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Request.Customer;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class TrTimeSheet : AbRepository<TR_TIME_SHEET>
    {
        private string timeSheetStatusSuccess = TimeSheetStatus.Success.Value();
        private string timeSheetStatusManual = TimeSheetStatus.Manual.Value();

        public override TR_TIME_SHEET GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.TIME_SHEET_ID == idInt).FirstOrDefault();
        }
        public List<TR_TIME_SHEET> GetDataByTaskCode(string taskCode, string empNo)
        {
            return this.FindBy(a => a.EMP_NO == empNo && a.TASK_CODE == taskCode && a.IS_ACTIVE == "Y").ToList();
        }
        public TR_TIME_SHEET GetDataActive(string empNo)
        {
            return this.FindBy(a => a.EMP_NO == empNo && a.END_DATE == null && a.IS_ACTIVE == "Y").FirstOrDefault();
        }
        public override List<TR_TIME_SHEET> GetDatas(string id)
        {
            return this.FindBy(a => a.EMP_NO == id && a.IS_ACTIVE == "Y").ToList();
        }
        public List<SummaryTime> GetDatasByTaskCode(string taskCode)
        {
            List<SummaryTime> responseList = new List<SummaryTime>();
            List<TR_TIME_SHEET> result = new List<TR_TIME_SHEET>();
            result = this.FindBy(a => a.TASK_CODE == taskCode && a.IS_ACTIVE == "Y").ToList();

            List<DateTime> resDatetimeDistinc = result.Select(a => a.START_DATE.Date).Distinct().ToList();
            foreach (var resDatetime in resDatetimeDistinc)
            {
                SummaryTime summaryTime = new SummaryTime();
                summaryTime.Date = resDatetime;

                summaryTime.workTime = result.Where(a => a.START_DATE.Date == resDatetime
                                                    && (a.TIME_SHEET_ACTIVE == TimeSheetStatus.Manual.Value() || a.TIME_SHEET_ACTIVE == TimeSheetStatus.Success.Value()))
                                               .Sum(a => a.WORK_TIME);

                responseList.Add(summaryTime);
            }
            return responseList;
        }

        public List<SummaryTime> GetDatasByTaskCode(string taskCode, DateTime? startDate, DateTime? endDate, string empNo)
        {
            List<SummaryTime> responseList = new List<SummaryTime>();
            List<TR_TIME_SHEET> result = new List<TR_TIME_SHEET>();

            result = this.FindBy(a => a.TASK_CODE == taskCode
                                  && a.EMP_NO == empNo
                                  && (a.TIME_SHEET_ACTIVE == timeSheetStatusSuccess || a.TIME_SHEET_ACTIVE == timeSheetStatusManual)
                                  && a.IS_ACTIVE == "Y").ToList();

            if (startDate != null)
            {
                result = result.Where(a => a.START_DATE.Date >= ((DateTime)startDate).Date).ToList();
            }

            if (endDate != null)
            {
                result = result.Where(a => ((DateTime)a.END_DATE).Date <= ((DateTime)endDate).Date).ToList();
            }

            List<DateTime> resDatetimeDistinc = result.Select(a => a.START_DATE.Date).Distinct().ToList();
            foreach (var resDatetime in resDatetimeDistinc)
            {
                SummaryTime summaryTime = new SummaryTime();
                summaryTime.Date = resDatetime;

                summaryTime.workTime = result.Where(a => a.START_DATE.Date == resDatetime)
                                            .Sum(a => a.WORK_TIME);


                responseList.Add(summaryTime);
            }



            return responseList;
        }

        public List<ReportSummaryWorkTimeModel> GetDatasByWorkTime(DateTime? startDate, DateTime? endDate)
        {
            List<ReportSummaryWorkTimeModel> responseList = new List<ReportSummaryWorkTimeModel>();
            List<TR_TIME_SHEET> result = new List<TR_TIME_SHEET>();

            result = this.FindBy(a => (a.TIME_SHEET_ACTIVE == timeSheetStatusSuccess || a.TIME_SHEET_ACTIVE == timeSheetStatusManual)
                                  && a.IS_ACTIVE == "Y").ToList();

            if (startDate != null)
            {
                result = result.Where(a => a.START_DATE.Date >= ((DateTime)startDate).Date).ToList();
            }

            if (endDate != null)
            {
                result = result.Where(a => ((DateTime)a.END_DATE).Date <= ((DateTime)endDate).Date).ToList();
            }

            responseList = (from a in result
                            group a by new { a.EMP_NO, a.START_DATE.Date } into g
                            select new ReportSummaryWorkTimeModel
                            {
                                Date = g.Key.Date,
                                empNo = g.Key.EMP_NO,
                                workTime = g.Sum(r => r.WORK_TIME)
                            }).OrderBy(r => r.Date).ThenBy(r => r.empNo).ToList();

            //List<DateTime> resDatetimeDistinc = result.Select(a => a.START_DATE.Date).Distinct().ToList();
            //foreach (var resDatetime in resDatetimeDistinc)
            //{
            //    ReportSummaryWorkTimeModel summaryTime = new ReportSummaryWorkTimeModel();
            //    summaryTime = result.Select(r => new ReportSummaryWorkTimeModel
            //    {
            //        Date = r.START_DATE.Date,
            //        empNo = r.EMP_NO,
            //        workTime = result.Where(a => a.START_DATE.Date == resDatetime && a.EMP_NO == r.EMP_NO).Sum(a => a.WORK_TIME)
            //    }).FirstOrDefault();
            //    responseList.Add(summaryTime);
            //}
            return responseList;
        }

        public decimal SummaryHour(List<SummaryTime> summaryTimesList)
        {
            decimal minuteSummary = 0;
            decimal hourSummary = 0;
            decimal resultSummary = 0;

            foreach (var summaryTimes in summaryTimesList)
            {
                int hour = (int)Math.Floor(summaryTimes.workTime);
                decimal minute = summaryTimes.workTime - hour;

                minuteSummary = minuteSummary + minute;
                hourSummary = hourSummary + hour;
            }

            if (minuteSummary > 0.6M)
            {
                string[] minuteArray = minuteSummary.ToString().Split('.');
                string minuteString = minuteArray[1];
                minuteSummary = minuteString.ToInt32();
                var timeSpan = TimeSpan.FromMinutes((double)minuteSummary);
                int hh = timeSpan.Hours;
                int mm = timeSpan.Minutes;

                string resultSummaryString = (hourSummary + hh).ToString() + "." + mm;
                resultSummary = resultSummaryString.ToDecimal();
            }
            else
            {
                resultSummary = hourSummary + minuteSummary;
            }


            return resultSummary;
        }


        public decimal SummaryHourList(string taskCode)
        {
            decimal summaryTime = 0;

            var result = this.FindBy(a => a.TASK_CODE == taskCode && a.END_DATE != null && a.IS_ACTIVE == "Y").ToList();

            var timeSpan = TimeSpan.FromHours((double)result.Sum(a => a.WORK_TIME));

            int hh = timeSpan.Hours;
            int mm = timeSpan.Minutes;
            summaryTime = ((hh + "." + mm).ToString()).ToDecimal();


            return summaryTime;
        }


        public TR_TIME_SHEET GetDatasByTaskCodeActive(string taskCode, string empNo)
        {
            string activeSatus = TimeSheetStatus.Active.Value();

            return this.FindBy(a => a.TASK_CODE == taskCode
                                 && a.EMP_NO == empNo
                                 && a.TIME_SHEET_ACTIVE == activeSatus
                                 && a.IS_ACTIVE == "Y").FirstOrDefault();
        }

        public List<TR_TIME_SHEET> GetDatasByTaskCodeActive(List<string> taskCodes)
        {
            string activeSatus = TimeSheetStatus.Active.Value();

            return this.FindBy(a => taskCodes.Contains(a.TASK_CODE)
                                 && a.TIME_SHEET_ACTIVE == activeSatus
                                 && a.IS_ACTIVE == "Y").ToList();
        }

        public TR_TIME_SHEET BindData(SaveTimeSheetRequest trTimSheetData, TR_TIME_SHEET data, UserAccess userAccess, ActionType actionType)
        {
            if (data == null)
                data = new TR_TIME_SHEET();

            data.EMP_NO = userAccess.empNo;
            data.TASK_CODE = trTimSheetData.taskCode;
            data.WORK_TIME = trTimSheetData.workTime;
            data.START_DATE = trTimSheetData.startDate;
            data.END_DATE = trTimSheetData.endDate;
            data.TIME_SHEET_ACTIVE = trTimSheetData.timeSheetActive;
            if (actionType == ActionType.Add)
            {
                data.IS_ACTIVE = StatusYesNo.Yes.Value();
                data.CREATE_BY = userAccess.fullName;
                data.CREATE_DATE = DateTime.Now;
            }
            else
            {
                data.UPDATE_BY = userAccess.fullName;
                data.UPDATE_DATE = DateTime.Now;
            }

            return data;
        }

        public TR_TIME_SHEET Save(SaveTimeSheetRequest data, UserAccess userAccess, out bool resultStatus)
        {
            resultStatus = false;
            this.Data = this.GetData(data.timeSheetId.ToString());
            if (this.Data == null)
            {
                this.Data = this.BindData(data, this.Data, userAccess, ActionType.Add);
                this.Create(this.Data);
                resultStatus = true;
            }
            else
            {
                this.Data = this.BindData(data, this.Data, userAccess, ActionType.Edit);
                this.Edit("", this.Data);
                resultStatus = true;
            }

            return this.Data;
        }


        public IEnumerable<TR_TIME_SHEET> GetDataByTaskCodesAsync(List<string> taskCodeList)
        {
            var active = new List<string> { "S", "M" };
            var data = dbContext.TR_TIME_SHEET.Where(r => taskCodeList.Contains(r.TASK_CODE) && active.Contains(r.TIME_SHEET_ACTIVE)).AsEnumerable();
            return data.ToList();
        }

    }

}