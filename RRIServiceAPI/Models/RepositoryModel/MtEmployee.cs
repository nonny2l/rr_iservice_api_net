﻿using RRApiFramework;
using RRApiFramework.HTTP.Response;
using RRApiFramework.Model;
using RRApiFramework.Model.Enum;
using RRApiFramework.Security;
using RRApiFramework.Utility;
using RRPlatFormAPI.HTTP.Response;
using RRPlatFormAPI.Models.RepositoryModel.Master;
using RRPlatFormModel;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public class MtEmployee : AbRepository<MT_EMPLOYEE>
    {
        public override MT_EMPLOYEE GetData(string id)
        {
            int idInt = id.ToInt32();
            return this.FindBy(a => a.EMP_ID == idInt).FirstOrDefault();
        }

        public static string GetName(string empNo)
        {
            string result = string.Empty;
            using (var rRIServiceModelContext = new RRIServiceModelContext())
            {
                var data = rRIServiceModelContext.MT_EMPLOYEE.Where(a => a.EMP_NO == empNo).FirstOrDefault();
                if (data != null)
                    result = data.FIRST_NAME +" "+ data.LAST_NAME;
            }
            return result;
        }

        public List<MT_EMPLOYEE> GetDataByUserId(string empno)
        {
            MtTeamRole mtTeamRole = new MtTeamRole();
            MtUser mtUser = new MtUser();
            List<string> empNoList = new List<string>();
            empNoList.Add(empno);

            var empNoListSet = mtTeamRole.GetUserIdList(empno).Select(a => a.empNo).ToList();
            empNoList.AddRange(empNoListSet);

            return this.FindBy(a => empNoList.Contains(a.EMP_NO)).ToList();
        }

        public override List<MT_EMPLOYEE> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public List<MT_EMPLOYEE> GetDatasByEmpNoList(List<string> empNoList)
        {
            return this.FindBy(a => empNoList.Contains(a.EMP_NO)).ToList();
        }
    }
  
}