﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public enum ApiMethod
    {
        [DefaultValue("GET")]
        GET = 0,

        [DefaultValue("POST")]
        POST = 1,

        [DefaultValue("PUT")]
        PUT = 2,

        [DefaultValue("DELETE")]
        DELETE = 4,
    }
}