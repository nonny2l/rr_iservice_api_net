﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.Enum
{

    public enum ActivityHeaderStatus
    {
        

        [DefaultValue("การเชิญเข้าร่วมโครงการ")]
        SendEDM = 2,

        [DefaultValue("ลงทะเบียนแล้ว")]
        Register = 6,

        //[DefaultValue("สำรวจ")]
        //Survey = 4,

        //[DefaultValue("ติดตามตามเอกสาร")]
        //FollowUpDocument = 5,

        [DefaultValue("เสนอ")]
        ProposeProposal = 8,

        //[DefaultValue("เซ็นสัญญา")]
        //FollowUpContract = 7,

        //[DefaultValue("การติดตั้ง")]
        //Installation = 8,

        //[DefaultValue("ส่งมอบงาน")]
        //Deliverable = 8,

        //[DefaultValue("ยกเลิก")]
        //Cancel = 9,
    }
}