﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public enum TimeSheetStatus
    {

        [Description("ยกเลิก")]
        [DefaultValue("C")]
        Cancel = 0,

        [Description("กำลังดำเนินการ")]
        [DefaultValue("A")]
        Active = 1,

        [Description("เรียบร้อยแล้ว")]
        [DefaultValue("S")]
        Success = 2,

        [Description("กำหนดเอง")]
        [DefaultValue("M")]
        Manual = 4,

    }

    


}