﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Models.RepositoryModel
{
    public enum ApiContentType
    {
        [DefaultValue("text/plain")]
        TextPlain = 0,

        [DefaultValue("text/html")]
        TextHtml = 1,

        [DefaultValue("application/pdf")]
        ApplicationPdf = 2,

        [DefaultValue("application/octet-stream")]
        ApplicationOctetStream = 4,

        [DefaultValue("application/msword")]
        ApplicationMsword = 8,

        [DefaultValue("application/json")]
        ApplicationJson = 16,

        [DefaultValue("application/json;charset=utf-8")]
        ApplicationJsonUTF8 = 32,

        [DefaultValue("html")]
        Html = 64,



    }
}