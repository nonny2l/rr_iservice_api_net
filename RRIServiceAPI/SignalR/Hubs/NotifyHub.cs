﻿using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace RRPlatFormAPI.SignalR.Hubs
{
    [HubName("notify")]
    public class NotifyHub : MyHub
    {
        public void Send(string name, string message)
        {
            Clients.All.clientMessage(name, message);
        }
        [HubMethodName("broadcast")]
        public async Task broadcast(string message)
        {
            await Clients.All.broadcaseMessage(message);
        }

        [HubMethodName("caller")]
        public async Task caller(string message)
        {
            await Clients.Caller.clientMessage(message);
        }
        [HubMethodName("onNotify")]
        public void onNotify(string message)
        {
        }
    }
}