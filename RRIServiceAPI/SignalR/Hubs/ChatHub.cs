﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Threading.Tasks;

namespace RRPlatFormAPI.SignalR.Hubs
{
    public class ChatHub : MyHub
    {
        public void Send(string name, string message)
        {
            Clients.All.clientMessage(name, message);
        }

    }
    [HubName("broadcast")]
    public class BroadcastHub : MyHub
    {
        public void Send(string name, string message)
        {
            Clients.All.broadcastMessage(name, message);
        }
    }
}