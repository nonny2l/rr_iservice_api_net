﻿using INCENTIVE_API.Models;
using INCENTIVE_API.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using DB_MODEL;

namespace INCENTIVE_API.MessageHandlers
{
    public class AuthHandler: DelegatingHandler
    {

        string _userName = "";
        string _role = "";
        //Method to validate credentials from Authorization
        //header value
        private bool ValidateCredentials(AuthenticationHeaderValue authenticationHeaderVal)
        {
            try
            {
                if (authenticationHeaderVal != null
                    && !String.IsNullOrEmpty(authenticationHeaderVal.Parameter))
                {
                    string[] decodedCredentials
                    = Encoding.ASCII.GetString(Convert.FromBase64String(
                    authenticationHeaderVal.Parameter))
                    .Split(new[] { ':' });

                    //now decodedCredentials[0] will contain
                    //username and decodedCredentials[1] will
                    //contain password.

                    using (var context = new ModelContext())
                    {
                        var _user = decodedCredentials[0].ToString();
                        var _pass = decodedCredentials[1].ToString();
                       
                        var query = context.AppAuths.Where(s => s.APP_USERNAME.Equals(_user) && s.APP_PASSWORD.Equals(_pass))
                                           .FirstOrDefault<AppAuth>();
                        if (query != null)
                        {
                            _userName = query.APP_NAME;
                            _role = "Admin";
                            return true;//request authenticated.;
                        }
                        else { return false; }
                    }
                }
                return false;//request not authenticated.
            }
            catch(Exception ex)
            {
                log4.log.Info(ex);
                return false;
            }
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            //if the credentials are validated,
            //set CurrentPrincipal and Current.User
            if (ValidateCredentials(request.Headers.Authorization))
            {
                Thread.CurrentPrincipal = new APIPrincipal(_userName, _role);
                HttpContext.Current.User = new APIPrincipal(_userName, _role);
            }
            //Execute base.SendAsync to execute default
            //actions and once it is completed,
            //capture the response object and add
            //WWW-Authenticate header if the request
            //was marked as unauthorized.

            //Allow the request to process further down the pipeline
            var response = await base.SendAsync(request, cancellationToken);

            if (response.StatusCode == HttpStatusCode.Unauthorized
                && !response.Headers.Contains("WwwAuthenticate"))
            {
                response.Headers.Add("WwwAuthenticate", "Basic");
            }

            return response;
        }


    }
}