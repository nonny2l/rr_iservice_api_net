﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP
{
    public class HttpCommonsModel
    {
        [Description("Y=ใช้งาน/N=ไม่ใช่งาน")]
        public string isActive { get; set; }
        [Description("สร้างข้อมูลโดย")]
        public string createBy { get; set; }
        [Description("วันที่สร้างข้อมูล")]
        public DateTime? createDate { get; set; }
        [Description("แก้ไขข้อมูลโดย")]
        public string updateBy { get; set; }
        [Description("วันที่แก้ไขข้อมูล")]
        public DateTime? updateDate { get; set; }
        [Description("ลบข้อมูลโดย")]
        public string deleteBy { get; set; }
        [Description("วันที่ลบข้อมูล")]
        public DateTime? deleteDate { get; set; }
    }
}