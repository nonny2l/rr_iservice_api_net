﻿using RRPlatFormAPI.Models.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Response
{

    public class MenuItem
    {
        public MenuItem()
        {
            children = new List<Children>();
        }
        public int id { get; set; }
        public string name { get; set; }
        public string icon { get; set; }
        public string path { get; set; }
        public string isUse { get; set; }
        public int? sort { get; set; }
        public List<Children> children { get; set; }
    }

    public class MenuItemResponse
    {
        public MenuItemResponse()
        {
            children = new List<MenuItemResponse>();
        }
        public int id { get; set; }
        public int parentId { get; set; }
        public string name { get; set; }
        public string icon { get; set; }
        public string path { get; set; }
        public string isUse { get; set; }
        public int? sort { get; set; }
        public List<MenuItemResponse> children { get; set; }
    }

    public class Children
    {
        public int id { get; set; }
        public string name { get; set; }
        public string icon { get; set; }
        public string path { get; set; }
        public string isUse { get; set; }
        public int? sort { get; set; }

    }

    public class MenuDataListResponse
    {
        public string button_id { get; set; }
        public string button_name { get; set; }
        public string url { get; set; }
    }

    public class MenuDataShort
    {
        public string menuId { get; set; }
        public string name { get; set; }
        public string isUse { get; set; }
        public int? parrentId { get; set; }
        public int? countChildren { get; set; }

    }

    public class GetMenuAllResponse
    {
        public List<MenuItemResponse> menuList { get; set; }
        public List<MenuDataShort> userMenuList { get; set; }
        public UserTypeProfile userTypeProfile { get; set; }
        public List<MenuDataShort> userTypeMenuList { get; set; }

    }
   
    public class MenuGroupButtonResponse
    {
        public string menuId { get; set; }
        public string name { get; set; }
        public string isUse { get; set; }

        public List<MenuButtonResponse> children { get; set; }
    }

    public class MenuButtonResponse
    {
        public string menuId { get; set; }
        public string name { get; set; }
        public string menuGroupId { get; set; }
        public string url { get; set; }
        public string isUse { get; set; }
    }


    public class GetMenuButtomAllResponse
    {
        public List<MenuGroupButtonResponse> menuButtonList { get; set; }
        public List<MenuButtonResponse> userMenuButtonList { get; set; }
        public UserTypeProfile userTypeProfile { get; set; }
        public List<MenuButtonResponse> userTypeMenuButtonList { get; set; }

    }
    public class UserTypeProfile
    {
        public string userTypeId { get; set; }
        public string userTypeName { get; set; }
        public string isActive { get; set; }
    }

    public class outputPermission
    {

        public string perId { get; set; } = "";
        public string perName { get; set; } = "";
        public string flg { get; set; } = "0";

    }
    public class MtMenuModel
    {
        public int menuId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string url { get; set; }
        public string icon { get; set; }
        public string active { get; set; }
        public string createBy { get; set; }
        public DateTime? createDate { get; set; }
        public string updateBy { get; set; }
        public DateTime? updateDate { get; set; }
        public int? sort { get; set; }
        public int? parrentId { get; set; }
        public int? menuTypeId { get; set; }
        public string isUse { get; set; }

    }


}