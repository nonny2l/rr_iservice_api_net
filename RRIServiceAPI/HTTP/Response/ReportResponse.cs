﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Response
{
    public class ReportResponse
    {
    }

    public class ReportSummaryWorktimeTemp
    {
        [JsonIgnore]
        public string deptCode { get; set; }
        [JsonIgnore]
        public string deptName { get; set; }
        [JsonIgnore]
        public string empNo { get; set; }
        [JsonIgnore]
        public string empName { get; set; }
        public DateTime? Date { get; set; }
        public decimal workTime { get; set; }
    }

    public class ReportSummaryWorkTimeModel
    {
        public string empNo { get; set; }
        public DateTime? Date { get; set; }
        public decimal workTime { get; set; }
        public decimal totalTime { get; set; }
    }

    public class ReportSummaryWorkTimeResponse
    {
        public List<ReportSummaryWorktimeTemp> worktimeList { get; set; }
        public string deptCode { get; set; }
        public string deptName { get; set; }
        public string empNo { get; set; }
        public string empName { get; set; }
        public decimal totalTime { get; set; }
    }
}