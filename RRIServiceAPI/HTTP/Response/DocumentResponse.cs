﻿using RRPlatFormAPI.HTTP.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Response
{
    public class GetDocumentResponse : DocumentHResponse
    {
    }
    public class DocumentHResponse : DocumentHRequest
    {
        public List<DocumentDResponse> ducomentD { get; set; }
    }
    public class DocumentDResponse : DocumentDRequest
    {
        public string docTypeName { get; set; }
    }
}