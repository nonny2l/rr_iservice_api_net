﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Response.Master
{
    public class SubDistrictResponse
    {
        [Description("รหัสแขวง/ตำบล")]
        public string subDistrictCode { get; set; }
        [Description("ชื่อแขวง/ตำบล ภาษาไทย")]
        public string subDistrictTName { get; set; }
        [Description("ชื่อแขวง/ตำบล ภาษาอังกฤษ")]
        public string subDdistrictEName { get; set; }
        [Description("รหัสเขต/อำเภอ")]
        public string reffDistrictCode { get; set; }
        [Description("รหัสไปรษณีย์")]
        public string zipcode { get; set; }
        [Description("Y=ใช้งาน/N=ไม่ใช้งาน")]
        public string isActive { get; set; }
    }
}