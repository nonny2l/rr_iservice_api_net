﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Response.Master
{
    public class DistrictResponse
    {
        [Description("รหัสเขต/อำเภอ")]
        public string districtCode { get; set; }
        [Description("ชื่อเขต/อำเภอ ภาษาไทย")]
        public string districtTName { get; set; }
        [Description("ชื่อเขต/อำเภอ ภาษาอังกฤษ")]
        public string districtEName { get; set; }
        [Description("รหัสจังหวัด")]
        public string reffProvCode { get; set; }
        [Description("Y=ใช้งาน/N=ไม่ใช้งาน")]
        public string isActive { get; set; }
    }
}