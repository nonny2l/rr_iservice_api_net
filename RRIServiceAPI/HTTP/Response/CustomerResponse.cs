﻿using RRPlatFormAPI.HTTP.Request.Customer;
using RRPlatFormAPI.Models.RepositoryModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Response
{


    public class FollowList
    {
        public string customerId { get; set; }
        public string customerNo { get; set; }
        public string caNo { get; set; }
        public string estimateArea { get; set; }
        public string layout { get; set; }
        public string planRoofTop { get; set; }
        public string billing { get; set; }
        public string singelLine { get; set; }
        public string loadTOU { get; set; }
        public string statusSurveyID { get; set; }
    }

    public class AppointmentList
    {
        public string customerId { get; set; }
        public string customerNo { get; set; }
        public string appointmentDate { get; set; }
        public string addressSurvey { get; set; }
        public string venderName { get; set; }
        public string venderCode { get; set; }
        public string status { get; set; }

    }

    public class TrCompanyResponse : MtCompanyRequest
    {
        [Description("ช่องทางของการได้มาของผู้ติดต่อ")]
        public string companySourceName { get; set; }
    }

    public class TrCompanyAddressResponse : TrCompanyAddressRequest
    {
        [Description("ชื่อเขต/อำเภอ")]
        public string districtName { get; set; }
        [Description("ชื่อแขวง/ตำบล")]
        public string subDistrictName { get; set; }
        [Description("ชื่อจังหวัด")]
        public string provName { get; set; }
    }

    public class CompanyPersonResponse : MtPersonRequest
    {
        [Description("รหัส compPersonId Auto Increment")]
        public int? compPersonId { get; set; }
        [Description("รหัสผู้ใช้ไฟ")]
        public int caId { get; set; }
        [Description("รหัสบริษัท")]
        public int compId { get; set; }
        [Description("ชื่อบริษัท")]
        public string compName { get; set; }
        [Description("ตำแหน่ง")]
        public string jobPosition { get; set; }
        [Description("แผนก")]
        public string department { get; set; }
        [Description("อีเมล์")]
        public string email { get; set; }
        [Description("รหัสช่องทางของการได้มาของผู้ติดต่อ")]
        public int personSourceId { get; set; }
        [Description("ช่องทางของการได้มาของผู้ติดต่อ")]
        public string personSourceName { get; set; }
        [Description("รหัสประเภทผู้ติดต่อ")]
        public string contactTypeCfCode { get; set; }
        [Description("ชือประเภทผู้ติดต่อ")]
        public string contactTypeCfName { get; set; }
        [Description("Y=ผู้ติดต่อหลัก/N=ไม่ใช่ผู้ติดต่อหลัก")]
        public string isMain { get; set; }
    }
    public class GetCustomerListResponse
    {
        [Description("อ็อบเจ็คบริษัท")]
        public TrCompanyResponse company { get; set; }
        [Description("รายการอ็อบเจ็คผู้ติดต่อ")]
        public List<CompanyPersonResponse> contactList { get; set; }
        [Description("รายการอ็อบเจ็คที่อยู่")]
        public List<TrCompanyAddressResponse> address { get; set; }
    }
    public class GetCustomerResponse
    {
        [Description("อ็อบเจ็คบริษัท")]
        public TrCompanyResponse company { get; set; }
        [Description("รายการอ็อบเจ็คผู้ติดต่อ")]
        public List<CompanyPersonResponse> contactList { get; set; }
        [Description("รายการอ็อบเจ็คที่อยู่")]
        public List<TrCompanyAddressResponse> address { get; set; }
    }

    public class GetSummaryCompanyCaStatusReponse
    {
        public int? refStatusId { get; set; }
        public string refStatusName { get; set; }
        public string configDCode { get; set; }
        public int caCount { get; set; }
    }

    public class GetClientListReponse
    {
        public string compCode { get; set; }
        public string compName { get; set; }
        public int businessTypeId { get; set; }
        public string businessTypeName { get; set; }
        public int subBusinessTypeId { get; set; }
        public string subBusinessTypeName { get; set; }
        public DateTime? stratDate { get; set; }
        public DateTime? endDate { get; set; }
        public string taxId { get; set; }
        public string telNo { get; set; }
        public string faxNo { get; set; }
        public int termOfPayment { get; set; }
        public int? agencyFee { get; set; }
        public string remark { get; set; }
        public string fackbookId { get; set; }
        public string website { get; set; }
        public string isCompany { get; set; }
        public string isActive { get; set; }
    }

    public class GetProjectListResponse
    {
        public int projectHId { get; set; }
        public string projectCode { get; set; }
        public string compCode { get; set; }
        public string projectName { get; set; }
        public string projectDesc { get; set; }
        public int budgetValue { get; set; }
        public string budgetUnitTypeCode { get; set; }
        public string budgetUnitTypeName { get; set; }
        public string isActive { get; set; }
    }

    public class GetServiceListResponse
    {
        public int srvcId { get; set; }
        public string srvcName { get; set; }
        public string srvcCode { get; set; }
        public string isActive { get; set; }
        public int seq { get; set; }
    }

    public class GetStatusJobListResponse
    {
        public int statusId { get; set; }
        public string statusCode { get; set; }
        public string statusName { get; set; }
        public int seq { get; set; }

        public string isActive { get; set; }
    }

    public class GetSubServiceListResponse
    {
        public string srvcCode { get; set; }
        public string srvcName { get; set; }
        public int srvcSubId { get; set; }
        public string srvcSubCode { get; set; }
        public string srvcSubName { get; set; }
        public string isActive { get; set; }

    }
    public class GetEmployeeListResponse
    {
        public int empId { get; set; }
        public string empNo { get; set; }
        public string title { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public string deptCode { get; set; }
        public string posnCode { get; set; }
        public string isActive { get; set; }
        public string userId { get; set; }
        public string avatar { get; set; }
        public string email { get; set; }
    }

    public class GetEmployeeByTeamListResponse
    {
        public int empId { get; set; }
        public string empNo { get; set; }
        public string title { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public string deptCode { get; set; }
        public string posnCode { get; set; }
        public string isActive { get; set; }
        public string userId { get; set; }
        public string avatar { get; set; }
        public string email { get; set; }
        public string jobNo { get; set; }
    }

    

    public class ContactData
    {
        public int compPersonId { get; set; }
        public string compCode { get; set; }
        public string compName { get; set; }
        public string jobPositionCode { get; set; }
        public string jobPositionName { get; set; }
        public string deptCode { get; set; }
        public string deptName { get; set; }
        public string email { get; set; }
        public string isMain { get; set; }
    }

    public class GetContactListResponse
    {
        public int personId { get; set; }
        public string personCode { get; set; }
        public string preNameCode { get; set; }
        public string preNameName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public string telephone { get; set; }
        public string citizenId { get; set; }
        public string passport { get; set; }
        public string isMain { get; set; }
        public string lineId { get; set; }
        public ContactData contact { get; set; }
    }



    public class GetJobListResponse
    {
        public List<MtProjectDDTO> mtProjectD { get; set; }

    }
    public class MtProjectDDTO
    {
        public string compCode { get; set; }
        public string compName { get; set; }
        public int projectDId { get; set; }
        public string projectCode { get; set; }
        public string projectName { get; set; }
        public string jobNo { get; set; }
        public string jobYear { get; set; }
        public string jobName { get; set; }
        public DateTime statusDate { get; set; }
        public string srvcCode { get; set; }
        public string subSrvcCode { get; set; }
        public string prodTypeCode { get; set; }
        public string personCode { get; set; }
        public string jobDesc { get; set; }
        public string cancelReason { get; set; }
        public string peRemk { get; set; }
        public int? peVatPct { get; set; }
        public string peVatFlag { get; set; }
        public int workBudget { get; set; }
        public DateTime? dueDate { get; set; }
        public DateTime startDate { get; set; }
        public DateTime? finishDate { get; set; }
        public DateTime? actlStartDate { get; set; }
        public DateTime? actlFinishDate { get; set; }
        public string issueBy { get; set; }
        public string issueByName { get; set; }
        public DateTime? issueDate { get; set; }
        public string ackBy { get; set; }
        public DateTime? ackDate { get; set; }
        public string appvBy { get; set; }
        public DateTime? appvDate { get; set; }
        public int? assignTo { get; set; }
        public DateTime? assignDate { get; set; }
        public string freezeFlag { get; set; }
        public string freezeYymm { get; set; }
        public string clientAppvBy { get; set; }
        public DateTime? clientAppvDate { get; set; }
        public int? peAgcyFeePct { get; set; }
        public int? lastVerNo { get; set; }
        public DateTime? lastVerDate { get; set; }
        public string verLock { get; set; }
        public int? peAgcyFeeAmt { get; set; }
        public string remark { get; set; }
        public string jobStatusCode { get; set; }
        public string jobStatusName { get; set; }
        public string srvcName { get; set; }
        public string subSrvcName { get; set; }
        public string isActive { get; set; }
        public string isAssigned { get; set; }
        public DateTime briefDate { get; set; }
        public List<TrJobTaskDto> trJobTask { get; set; }
        public List<SaveJobAttactFileData> jobAttactList { get; set; }
    }

    public class TrTaskDTO
    {
        public int taskId { get; set; }
        public string taskCode { get; set; }
        public string taskName { get; set; }
        public string jobNo { get; set; }
        public string projectCode { get; set; }
        public string parentTaskCode { get; set; }
        public int taskGroupId { get; set; }
        public string priorityCode { get; set; }
        public string deptNo { get; set; }
        public DateTime? actualStartDate { get; set; }
        public DateTime? actualEndDate { get; set; }
        public DateTime? planStartDate { get; set; }
        public DateTime? planEndDate { get; set; }
        public DateTime? dueDate { get; set; }
        public string note { get; set; }
        public int progressStatusId { get; set; }
        public string hashtag { get; set; }
        public string isActive { get; set; }
        public decimal estimated { get; set; }

    }

    public class TrJobTaskDto
    {
        public int jobTaskId { get; set; }
        public string jobTaskName { get; set; }
        public string jobTaskCode { get; set; }
        public string note { get; set; }
        public string jobNo { get; set; }
        public string deptCode { get; set; }
        public string deptName { get; set; }
        public DateTime? planStartDate { get; set; }
        public DateTime? planEndDate { get; set; }
        public DateTime? dueDate { get; set; }
        public string priorityCode { get; set; }
        public string priorityName { get; set; }
        public string jobTaskStatusCode { get; set; }
        public string jobTaskStatusName { get; set; }
        public TeamDatas team { get; set; }
    }

    public class TeamRoleDatas
    {
        public string empNo { get; set; }
        public string empName { get; set; }
        public List<string> positionCode { get; set; }
        public string isMain { get; set; }
        public string isActive { get; set; }
    }

    public class TeamDatas
    {
        public string teamCode { get; set; }
        public string jobTaskCode { get; set; }
        public string teamName { get; set; }
        public List<TeamRoleDatas> teamRoleList { get; set; }
    }

    public class GetDataTimeSheetResponse
    {
        public int assingToId { get; set; }
        public string empNo { get; set; }
        public string userFullName { get; set; }
        public string email { get; set; }
        public int taskId { get; set; }
        public string taskCode { get; set; }
        public string jobNo { get; set; }
        public string taskName { get; set; }
        public string projectCode { get; set; }
        public string projectName { get; set; }
        public string taskGroupCode { get; set; }
        public string taskGroupName { get; set; }
        public string priorityCode { get; set; }
        public string priorityName { get; set; }
        public DateTime? actualStartDate { get; set; }
        public DateTime? actualEndDate { get; set; }
        public DateTime? planStartDate { get; set; }
        public DateTime? planEndDate { get; set; }
        public DateTime? dueDate { get; set; }
        public string note { get; set; }
        public string progressStatusCode { get; set; }
        public string progressStatusName { get; set; }
        public decimal totalDuration { get; set; }
        public TaskActiveOntime taskActiveOnTime { get; set; }
        public List<SummaryTime> summaryTimeList { get; set; }
        public string compCode { get; set; }
        public string taskStatusCode { get; set; }
        public string taskStatusName { get; set; }
        public decimal estimated { get; set; }
        public DateTime? createDate { get; set; }
        public JobDatas jobData { get; set; }

    }

    public class JobDatas
    {
        public string jobNo { get; set; }
        public string jobName { get; set; }
        public string serviceName { get; set; }
        public string subServiceName { get; set; }
        public DateTime? briefDate { get; set; }
        public DateTime? dueDate { get; set; }
        public string issueByName { get; set; }
        public DateTime? issueDate { get; set; }
        public string contactName { get; set; }
    }

    public class TaskActiveOntime
    {
        public int timeSheetId { get; set; }
        public string empNo { get; set; }
        public DateTime startDate { get; set; }
        public DateTime? endDate { get; set; }
        public decimal workTime { get; set; }
        public string isActive { get; set; }
    }

    public class SummaryTime
    {
        public DateTime? Date { get; set; }
        public decimal workTime { get; set; }
    }
    public class GetTimeSheetActiveByUserIdResponse
    {
        public int timeSheetId { get; set; }
        public string empNo { get; set; }
        public string taskCode { get; set; }
        public DateTime startDate { get; set; }
        public DateTime? endDate { get; set; }
        public decimal workTime { get; set; }

        public string timeSheetActiveStatusCode { get; set; }
        public string timeSheetActiveStatusName { get; set; }

        public DateTime? createDate { get; set; }

    }
    public class GetTimeSheetActiveByTaskCodeResponse
    {
        public List<GetTimeSheetActiveByUserIdResponse> timeSheetList { get; set; }
    }
    public class GetTaskCerrentTimeSheetResponse
    {

        public int timeSheetId { get; set; }
        public string compCode { get; set; }
        public string compName { get; set; }
        public string projectCode { get; set; }
        public string projectName { get; set; }
        public string jobCode { get; set; }
        public string jobName { get; set; }
        public string taskCode { get; set; }
        public string taskName { get; set; }
        public string priorityCode { get; set; }
        public string priorityName { get; set; }
        public DateTime startDate { get; set; }
        public DateTime? endDate { get; set; }
        public decimal totalWorkingTime { get; set; }
            
    }
    public class GetTeamListResponse
    {
        public string teamCode { get; set; }
        public string teamName { get; set; }
        public string jobTarkCode { get; set; }
        public List<TeamRoleList> teamRoleList { get; set; }
    }

    public class TeamRoleList
    {
        public string empNo { get; set; }
        public string isMain { get; set; }
        public string isActive { get; set; }
    }

    public class GetSummaryTaskListResponse
    {
        public string compCode { get; set; }
        public string compName { get; set; }
        public string projectCode { get; set; }
        public string projectName { get; set; }
        public string jobNo { get; set; }
        public string jobName { get; set; }
        public string jobYear { get; set; }
        public string jobStatusCode { get; set; }
        public string jobStatusName { get; set; }
        public string taskCode { get; set; }
        public string taskName { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public decimal? totalWorkingTime { get; set; }
        public string taskStatusCode { get; set; }
        public string taskStatusName { get; set; }
        public decimal? estimated { get; set; }
        public string empNo { get; set; }
        public string empFullName { get; set; }
        public List<string> empNoList { get; set; }
        public List<string> empFullNameList { get; set; }
    }

    public class GetClientCareListResponse
    {
        public string empNo { get; set; }
        public string empName { get; set; }
        public string deptCode { get; set; }
        public string deptName { get; set; }
        public string positionCode { get; set; }
        public string positionName { get; set; }
        public List<GetClientList> companyList { get; set; }
    }

    public class GetClientList
    {
        public string compCode { get; set; }
        public string compName { get; set; }
    }

    public class TrCompanyHeadCsGroupEmp
    {
        public string empNo { get; set; }
        public List<string> companyCodeList { get; set; }
    }

}