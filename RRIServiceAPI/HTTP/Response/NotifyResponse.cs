﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Response
{
    public class GetNotifyListResponse
    {
        public string notifyMsgCode { get; set; }
        public string notifyHeader { get; set; }
        public string notifyDesc { get; set; }
        public string imgUrl { get; set; }
        public string url { get; set; }
        public DateTime? createDate { get; set; }
        public string readStatus { get; set; }
    }

    public class GetReadNotifyListResponse
    {
        public string notifyMsgCode { get; set; }
      
    }

    

}