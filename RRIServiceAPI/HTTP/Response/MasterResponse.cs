﻿using RRPlatFormAPI.Models.RepositoryModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Response
{
   
    public class GetMasterListResponse : MtConficDDto
    {
        public List<GetMasterListResponse> child { get; set; }
    }

    public class GeDepartmentListResponse
    {
        public int deptId { get; set; }
        public string deptCode { get; set; }
        public string deptName { get; set; }
        public string jobHNDLFlag { get; set; }
        public string isActive { get; set; }
    }

    public class GetEstimationTimeListResponse
    {
        public int estimatedId { get; set; }
        public string estimatedCode { get; set; }
        public string estimatedName { get; set; }
        public string remark { get; set; }
        public string isActive { get; set; }
    }

    public class GetPriorityListResponse
    {
        public int priorityId { get; set; }
        public string priorityCode { get; set; }
        public string priorityName { get; set; }
        public string remark { get; set; }
        public string isActive { get; set; }
    }

    public class GetTaskGroupListesponse
    {
        public int taskGroupId { get; set; }
        public string taskGroupName { get; set; }
        public string taskGroupCode { get; set; }
        public string isActive { get; set; }
        public int seq { get; set; }
    }

 

    public class GetMasterTaskStatusListResponse
    {
        public int taskStatusId { get; set; }
        public string taskStatusName { get; set; }
        public string taskStatusCode { get; set; }
        public string isActive { get; set; }
        public int seq { get; set; }
    }

    public class GetOrgPositionListResponse
    {
        public string deptCode { get; set; }
        public string deptName { get; set; }
        public string positionCode { get; set; }
        public string positionName { get; set; }
        public int seq { get; set; }
    }

    public class GetMasterConfigListResponse
    {
        public string HeaderCode { get; set; }
        public string HeaderName { get; set; }
        public string EName { get; set; }
        public string TName { get; set; }
        public string Code { get; set; }
        public string Seq { get; set; }

    }




}




