﻿using Newtonsoft.Json;
using RRApiFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Request
{
   

    public class UserRequest
    {
        public int provinceCode { get; set; }
        public int districtCode { get; set; }
        public int subDistrictCode { get; set; }
    }

    public class LoginRequest
    {
        [Description("ชื่อผู้ใช้งาน")]
        public string userName { get; set; }

        [Description("รหัสผ่าน")]
        public string passWord { get; set; }

        [Description("กลุ่มผู้ใช้งาน")]
        public int userGroupId { get; set; }

    }

    public class GetUserListRequest
    {
        public string deptNo { get; set; }
    
    }

    public class GetUserProfileRequest : UserAccess
    {
      
    }

    public class SavePasswordRequest
    {
        [Required]
        public string currentPassword { get; set; }
        [Required]
        public string newPassword { get; set; }
        [JsonIgnore]
        public string fullName { get; set; }
    }

}