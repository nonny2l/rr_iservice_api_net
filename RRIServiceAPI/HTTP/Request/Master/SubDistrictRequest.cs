﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Request.Master
{
    public class SubDistrictRequest
    {
        [Description("รหัสจังหวัด")]
        public string provCode { get; set; }
        [Description("รหัสเขต/อำเภอ")]
        public string districtCode { get; set; }
    }
}