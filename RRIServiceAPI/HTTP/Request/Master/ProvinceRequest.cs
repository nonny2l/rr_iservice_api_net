﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Request.Master
{
    public class ProvinceRequest
    {
        [Description("รหัสจังหวัด")]
        public string provCode { get; set; }
        [Description("ชื่อจังหวัด ภาษาไทย")]
        public string provTName { get; set; }
        [Description("ชื่อจังหวัด ภาษาอังกฤษ")]
        public string provEName { get; set; }
        [Description("ภาค")]
        public string provRegion { get; set; }
        [Description("เขตพื้นที่การดูแลของ PEA")]
        public string zonePEA { get; set; }
        [Description("Y=ใช้งาน/N=ไม่ใช่งาน")]
        public string isActive { get; set; }
    }
}