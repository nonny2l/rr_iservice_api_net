﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Request.Master
{
    public class VendorRequest : HttpCommonsModel
    {
        [Required]
        [Description("รหัส Vendor")]
        public int vendorId { get; set; }
        [Required]
        [Description("ชื่อ Vendor")]
        public string vendorName { get; set; }
        [Required]
        [Description("ลำดับ")]
        public int seq { get; set; }
        [Column(TypeName = "text")]
        [Description("หมายเหตุ")]
        public string remark { get; set; }
    }
    public class GetVendorRequest
    {
        [Description("รหัส Vendor")]
        public int? vendorId { get; set; }
        [Description("ชื่อ Vendor")]
        public string vendorName { get; set; }
    }
}