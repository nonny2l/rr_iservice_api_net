﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Request
{
    public class DucomentRequest
    {
        public DocumentHRequest documentH { get; set; }
        public List<DocumentDRequest> documentD { get; set; }
    }

    public class DocumentHRequest : HttpCommonsModel
    {
        [Required]
        [Description("รหัสเอกสาร")]
        public int docHId { get; set; }
        [Required]
        [Description("รหัสผู้ใช้ไฟ")]
        public int caId { get; set; }
        [Description("หมายเหตุ")]
        [Column(TypeName = "text")]
        public string remark { get; set; }
    }
    public class DocumentDRequest : HttpCommonsModel
    {
        [Required]
        [Description("รหัสรายละเอียดเอกสาร running")]
        public int docDId { get; set; }
        [Required]
        [Description("รหัสเอกสาร")]
        public int docHId { get; set; }
        [Required]
        [Description("รหัส Reference เอกสาร")]
        public int docTypeId { get; set; }
        [Required]
        [Description("วันที่ได้รับเอกสาร")]
        public DateTime docDate { get; set; }
        
    }

    public class GetDocomentRequest
    {
        [Description("รหัสผู้ใช้ไฟ")]
        public int? caId { get; set; }
        [Description("รหัสเอกสาร")]
        public int? docHId { get; set; }

        [Description("รหัส Reference เอกสาร")]
        public int? docTypeId { get; set; }
    }
}