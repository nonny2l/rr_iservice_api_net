﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Request
{
    public class GetNotifyListRequest
    {
    }
    public class GetReadNotifyListRequest
    {
    }
    public class ReadNotifyRequest
    {
        public string notifyMsgCode { get; set; }
    }

    public class ReadNotifyListRequest
    {
        public List<ReadNotifyRequest> notifyMsgCodeList { get; set; }
    }
    public class SaveNotiMsgRequest : HttpCommonsModel
    {
        public string empNo { get; set; }
        public DateTime? endDate { get; set; }
        public string imgUrl { get; set; }
        public string notifyDesc { get; set; }
        public string notifyGroupCode { get; set; }
        public string notifyHeader { get; set; }
        public string notifyMsgCode { get; set; }
        public int notifyMsgId { get; set; }
        public string notifyTopicCode { get; set; }
        public DateTime? startDate { get; set; }
        public string url { get; set; }
        public string refKey { get; set; }
        public string refTable { get; set; }
        public string refValue { get; set; }
    }

}