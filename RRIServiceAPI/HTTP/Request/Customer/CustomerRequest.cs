﻿using RRPlatFormAPI.HTTP.Response;
using RRPlatFormAPI.Models.RepositoryModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Request.Customer
{
    public class CustomerRequest
    {
    }

    public class GetCustomerListFilterRequest
    {
        // where TR_COMPANY_CA
        [Description("รหัส statusMainId")]
        public int? statusMainId { get; set; }
        [Description("รหัส statusSubMainId")]
        public int? statusSubMainId { get; set; }

        // where TR_COMPANY
        [Description("รหัสบริษัท")]
        public int? compId { get; set; }
        [Description("รหัสช่องทางของการได้มาของบริษัท")]
        public int? companySourceId { get; set; }
        [Description("ชื่อบริษัท")]
        public string compName { get; set; }
        [Description("ประเภทธุรกิจ")]
        public string businessType { get; set; }
        [Description("ประเภทธุรกิจย่อย")]
        public string subBusinessType { get; set; }


        [Description("CA NO กรณีทีมี CA NO / กรณีที่ไม่มี CA NO ก็ generate เลขใหม่/หรือเป็นเลขของนครหลวง")]
        public string refCaNo { get; set; }
        [Description("ประเภทของ CA (PEA or MEA)")]
        public string caType { get; set; }
        [Description("รหัสผู้ใช้ไฟ")]
        public int? caId { get; set; }


        // where TR_COMPANY_PERSON

        [Description("รหัสบุคคล")]
        public int? personId { get; set; }
        [Description("ชื่อบุคคล")]
        public string firstName { get; set; }
        [Description("นามสกุล")]
        public string lastName { get; set; }
        [Description("เบอร์โทรศัพท์ ทั้งมือถือ และบ้าน")]
        public string telephone { get; set; }
        [Description("เบอร์มือถือ")]
        public string mobile { get; set; }
        [Description("อีเมลล์")]
        public string email { get; set; }
        [Description("รหัสประเภทผู้ติดต่อ")]
        public string contactTypeCfCode { get; set; }
        [Description("หมายเลขบัตรประชาชน")]
        public string citizenId { get; set; }
        [Description("หมายเลข Passport")]
        public string passport { get; set; }
        public string compCode { get; set; }
    }

    public class SaveCompanyContactRequest
    {
        [Description("อ็อบเจ็คผู้ติดต่อของบริษัท")]
        public TrCompanyPersonRequest companyPerson { get; set; }
        [Description("อ็อบเจ็คบุคคล")]
        public MtPersonRequest person { get; set; }
    }

    public class UpdateStatusCompanyCaRequest
    {
        [Description("รหัสผู้ใช้ไฟ")]
        [Required]
        public int caId { get; set; }
        [Required]
        public List<ColumnValueCompanyCa> fieldData { get; set; }
        [Description("รหัสสถานะหลัก")]
        public int? statusMainId { get; set; }
        [Description("รหัสสถานะรอง")]
        public int? statusSubMainId { get; set; }
        [Description("โค้ดตัวย่อของสถานะ")]
        public string statusCode { get; set; }
        [Description("แก้ไขข้อมูลโดย")]
        public string updateBy { get; set; }
    }

    public class ColumnValueCompanyCa
    {
        [Description("คอลัมน์ที่ต้องการ update")]
        [Required]
        public string column { get; set; }
        [Description("ค่าของคอลัมน์ที่ต้องการ update")]
        public string value { get; set; }
    }

    public class GetCompanyCaStatusHistoryRequest
    {
        [Description("รหัสผู้ใช้ไฟ")]
        [Required]
        public int? caId { get; set; }
    }
    public class GenerateUrlCaRequest
    {
        [Description("สถานะการแปลงข้อมมูล Y = แปลงข้อมูลทั้งหมด , N = แปลงข้อมูลเฉพาะเลข  ca ที่กำหนด")]
        [Required]
        public string generateAll { get; set; }

        [Description("ชุดเลขผู้ใช้ไฟ")]
        [Required]
        public List<int> caIdList { get; set; }

        [Description("ประเภทข้อมูลของงาน EDM")]
        [Required]
        public string urlType { get; set; }

        [Description("ประเภทข้อมูลที่ต้องการแปลง")]
        [Required]
        public string generateType { get; set; }
    }

    public class GetClientListRequest
    {

    }

    public class GetProjectListRequest
    {
        public List<string> compCodeList { get; set; }
    }

    public class GetServiceListRequest
    {

    }
    public class GetSubServiceListRequest
    {
        public int srvcId { get; set; }
        public string srvcCode { get; set; }
        public int srvcSubId { get; set; }
        public string srvcSubName { get; set; }
    }
    public class GetStatusJobListRequest
    {

    }

    public class GetEmployeeListRequest
    {
        public string deptCode { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string empNo { get; set; }
        public string posnCode { get; set; }

    }

    public class GetEmployeeByTeamListRequest
    {
        [Required]
        public string jobNo { get; set; }
    }

    public class GetEmployeeAllListRequest
    {

    }

    public class GetContactListRequest
    {
       [Required]
       public List<string> compCodeList { get; set; }
    }



    public class SaveContactDataRequest
    {
        public int compPersonId { get; set; }
        public string compCode { get; set; }
        public string compName { get; set; }
        public string jobPositionCode { get; set; }
        public string jobPositionName { get; set; }
        public string deptCode { get; set; }
        public string deptName { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string isMain { get; set; }
        public string isActive { get; set; }

        public string personCode { get; set; }

    }

    public class SaveContactRequest
    {
        public int personId { get; set; }
        public string personCode { get; set; }
        public string preNameCode { get; set; }
        public string preNameName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public string telephone { get; set; }
        public string citizenId { get; set; }
        public string passport { get; set; }
        public string isMain { get; set; }
        public string lineId { get; set; }
        public string isActive { get; set; }
        public SaveContactDataRequest contact { get; set; }
    }


    public class GetJobListRequest
    {

        public List<string> compCodeList { get; set; }
        public List<string> projectCodeList { get; set; }
        public List<string> jobStatusCodeList { get; set; }
        public List<string> deptCodeList { get; set; }
        public string jobNo { get; set; }
        public DateTime? briefDateFrom { get; set; }
        public DateTime? briefDateTo { get; set; }
        public string jobYear { get; set; }


    }

    public class GetJobDetailRequest
    {
        public string jobNo { get; set; }
    }

    public class GetDataTimeSheetRequest
    {
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public List<string> empNoList { get; set; }
        public List<string> projectCodeList { get; set; }
        public List<string> compCodeList { get; set; }
    }

    public class GetTaskDetailsRequest
    {
        public string taskCode { get; set; }
    }

   


    public class GetTaskCerrentTimeSheetRequest
    {
    }

    public class SaveJobRequest
    {
        public string compCode { get; set; }
        public string projectCode { get; set; }
        public string jobNo { get; set; }
        public string jobYear { get; set; }
        public string jobName { get; set; }
        public string personCode { get; set; }
        public string srvcCode { get; set; }
        public string subSrvcCode { get; set; }
        public string jobDesc { get; set; }
        public DateTime? jobDate { get; set; }
        public string jobStatusCode { get; set; }
        public int workBudget { get; set; }
        [Required]
        public DateTime briefDate { get; set; }
        [Required]
        public DateTime dueDate { get; set; }
        public string issueBy { get; set; }
        public DateTime? issueDate { get; set; }
        public List<SaveJobAttactFileData> jobAttactList { get; set; }
        public List<SaveJobTaskData> jobTaskList { get; set; }
    }

    public class SaveJobAttactFileData
    {
        public int jobAttachId { get; set; }
        public int? seq { get; set; }
        public string jobNo { get; set; }
        public string fileName { get; set; }
        public string oldFileName { get; set; }
        public string fileType { get; set; }
        public string filePath { get; set; }
        public string isActive { get; set; }
    }

    public class SaveJobTaskData
    {
        public int jobTaskId { get; set; }

        public string jobTaskCode { get; set; }
        public string jobTaskName { get; set; }
        public string note { get; set; }
        public string compCode { get; set; }
        public string projectCode { get; set; }
        public string jobNo { get; set; }
        public string deptCode { get; set; }
        public DateTime? planStartDate { get; set; }
        public DateTime? planEndDate { get; set; }
        public DateTime? dueDate { get; set; }
        public string priorityCode { get; set; }
        public string jobTaskStatusCode { get; set; }

    }


    public class SaveJobTarkRequest : SaveJobTaskData
    {
        public List<TeamData> teamList { get; set; }
    }

    public class TeamRoleDatas
    {
        public string empNo { get; set; }
        public string teamCode { get; set; }
        public string isMain { get; set; }
        public string isActive { get; set; }
        public string isSendNotify { get; set; }
    }

    public class TeamData
    {
        public string jobTaskCode { get; set; }
        public string teamCode { get; set; }
        public string teamName { get; set; }
        public string deptCode { get; set; }
        public string jobNo { get; set; }

        public IList<TeamRoleDatas> teamRoleList { get; set; }
    }

    public class SaveTimeSheetRequest
    {
        public int timeSheetId { get; set; }
        public string taskCode { get; set; }
        public DateTime startDate { get; set; }
        public DateTime? endDate { get; set; }
        public decimal workTime { get; set; }
        public string timeSheetActive { get; set; }

    }

    public class GetTimeSheetActiveByUserIdRequest
    {

    }

    public class GetTimeSheetHistoryByTaskCode
    {
        [Required]
        public string taskCode { get; set; }
    }
    
    public class SaveTaskRequest
    {
        [Required]
        public string compCode { get; set; }
        [Required]
        public string projectCode { get; set; }
        [Required]
        public string jobNo { get; set; }
        public string taskCode { get; set; }
        [Required]
        public string taskName { get; set; }
        [Required]
        public string taskGroupCode { get; set; }
        public string note { get; set; }
        [Required]
        public string priorityCode { get; set; }
        [Required]
        public DateTime? planStartDate { get; set; }
        [Required]
        public DateTime? planEndDate { get; set; }
        [Required]
        public DateTime? dueDate { get; set; }
        public string taskStatusCode { get; set; }
        [Required]
        public int estimated { get; set; }
        [Required]
        public List<SaveTaskAssignDatas> assignList { get; set; }

    }
    public class SaveTaskAssignDatas
    {
        [Required]
        public string empNo { get; set; }
        public string taskCode { get; set; }
        [Required]
        public string isActive { get; set; }

        [Required]
        public string isSendNotify { get; set; }
    }

    public class GetSummaryTaskListRequest
    {
        public string compCode { get; set; }
        public string projectCode { get; set; }
        [Required]
        public List<string> jobStatusCodeList { get; set; }
        public string jobNo { get; set; }
        [Required]
        public string year { get; set; }
    }

    public class GetClientCareListRequest
    {
        public List<string> deptCodeList { get; set; }
    }
}

