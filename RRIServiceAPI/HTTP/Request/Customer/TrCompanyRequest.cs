﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Request.Customer
{
    public class GetCustomerRequest
    {
        [Required]
        [Description("รหัสบริษัท")]
        public int compId { get; set; }
    }
    public class MtCompanyRequest : HttpCommonsModel
    {
        [Required]
        [Description("รหัสบริษัท")]
        public int compId { get; set; }
        [Required]
        [Description("ชื่อบริษัท")]
        public string compName { get; set; }
        [Required]
        [Description("ประเภทธุรกิจ")]
        public string businessType { get; set; }
        [Description("ประเภทธุรกิจย่อย")]
        public string subBusinessType { get; set; }
        [Description("เบอร์โทรศัพท์บริษัท")]
        public string telephone { get; set; }
        [Description("เบอร์มือถือบริษัท")]
        public string mobile { get; set; }
        [Description("Email บริษัท")]
        public string email { get; set; }
        [Description("website บริษัท")]
        public string website { get; set; }
        [Description("ตัวย่อ project เช่น PEA")]
        public string projectCode { get; set; }
        [Required]
        [Description("รหัสช่องทางของการได้มาของบริษัท")]
        public int companySourceId { get; set; }
        [Required]
        [Description("Y=Company/N=Person")]
        public string isCompany { get; set; }
    }


    public class MtPersonRequest : HttpCommonsModel
    {
        [Description("รหัสบุคคล")]
        [Required]
        public int personId { get; set; }
        [Required]
        [Description("คำนำหน้าชื่อ")]
        public string titleName { get; set; }
        [Description("ชื่อ")]
        [Required]
        public string firstName { get; set; }
        [Description("นามสกุล")]
        public string lastName { get; set; }
        [Description("เบอร์โทรศัพท์ ทั้งมือถือ และบ้าน")]
        public string telephone { get; set; }
        [Description("เบอร์มือถือ")]
        public string mobile { get; set; }
        [Description("หมายเลขบัตรประชาชน")]
        public string citizenId { get; set; }
        [Description("หมายเลข Passport")]
        public string passport { get; set; }
        [Description("Line ID ")]
        public string lineId { get; set; }
    }
    public class TrCompanyPersonRequest : HttpCommonsModel
    {
        [Description("รหัส compPersonId Auto Increment")]
        public int? compPersonId { get; set; }
        [Description("รหัสผู้ใช้ไฟ")]
        [Required]
        public int caId { get; set; }
        [Description("รหัสบุคคล")]
        [Required]
        public int personId { get; set; }
        [Description("รหัสบริษัท")]
        [Required]
        public int compId { get; set; }
        [Description("ตำแหน่ง")]
        public string jobPosition { get; set; }
        [Description("แผนก")]
        public string department { get; set; }
        [Description("อีเมล์")]
        public string email { get; set; }
        [Required]
        [Description("ช่องทางของการได้มาของผู้ติดต่อ")]
        public int personSourceId { get; set; }
        [Required]
        [Description("ประเภทผู้ติดต่อ")]
        public string contactTypeCfCode { get; set; }
        [Required]
        [Description("Y=ผู้ติดต่อหลัก/N=ไม่ใช่ผู้ติดต่อหลัก")]
        public string isMain { get; set; }
    }

    public class TrCompanyAddressRequest : HttpCommonsModel
    {
        [Required]
        [Description("ที่อยู่บริษัท")]
        public int compAddrId { get; set; }
        [Required]
        [Description("รหัสบริษัท")]
        public int compId { get; set; }
        [Description("ที่อยู่บริษัท")]
        public string address { get; set; }
        [Description("รหัสเขต/อำเภอ")]
        public string districtCode { get; set; }
        [Description("รหัสแขวง/ตำบล")]
        public string subDistrictCode { get; set; }
        [Description("รหัสจังหวัด")]
        public string provCode { get; set; }
        [Description("รหัสไปรษณีย์")]
        public string zipcode { get; set; }
        [Description("พื้นที่รับผิดชอบ 12 เขตของการไฟฟ้า")]
        public string zone { get; set; }
        [Required]
        [Description("Y = เป็นที่อยู่หลัก N = เป็นที่อยู่รอง")]
        public string isMain { get; set; }
    }
    
    public class GetCompanyPersonRequestList
    {
        [Description("CA NO กรณีทีมี CA NO / กรณีที่ไม่มี CA NO ก็ generate เลขใหม่/หรือเป็นเลขของนครหลวง")]
        public string refCaNo { get; set; }
        [Description("รหัสผู้ใช้ไฟ")]
        public int? caId { get; set; }

        [Description("รหัสบุคคล")]
        public int? personId { get; set; }
        [Description("รหัสบริษัท")]
        public int? compId { get; set; }

        [Description("ชื่อ")]
        public string firstName { get; set; }
        [Description("นามสกุล")]
        public string lastName { get; set; }
        [Description("เบอร์โทรศัพท์ ทั้งมือถือ และบ้าน")]
        public string telephone { get; set; }
        [Description("เบอร์มือถือ")]
        public string mobile { get; set; }
        [Description("อีเมลล์")]
        public string email { get; set; }
        [Description("รหัสประเภทผู้ติดต่อ")]
        public string contactTypeCfCode { get; set; }
        [Description("หมายเลขบัตรประชาชน")]
        public string citizenId { get; set; }
        [Description("หมายเลข Passport")]
        public string passport { get; set; }
    }
    
}