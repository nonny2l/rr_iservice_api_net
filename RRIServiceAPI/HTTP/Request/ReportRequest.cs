﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Request
{
    public class ReportRequest
    {
    }

    public class ReportSummaryWorktimeRequest
    {
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public List<string> deptCodeList { get; set; }
        public string empName { get; set; }
    }
}