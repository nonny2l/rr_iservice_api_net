﻿using RRPlatFormAPI.Controllers.ExternalApiCaller;
using RRPlatFormAPI.HTTP.Request.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Request
{
    public class GetTeamListRequest
    {
        public string compCode { get; set; }
        public string projectCode { get; set; }
        public string jobNo { get; set; }
        public string deptCode { get; set; }
    }

  

    public class AcceptJobTaskRequest
    {
        public string jobTaskCode { get; set; }
        public string compCode { get; set; }
        public string projectCode { get; set; }
        public string jobNo { get; set; }
        public string deptCode { get; set; }
        public string acceptStatus { get; set; }
        public DateTime? planStartDate { get; set; }
        public DateTime? planEndDate { get; set; }
        public DateTime? dueDate { get; set; }
        public TeamData team { get; set; }
    }




}