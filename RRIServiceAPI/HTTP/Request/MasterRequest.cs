﻿using RRPlatFormAPI.Models.RepositoryModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Request
{

    public class GetMasterListRequest
    {
        [Description("Code Master Data ใช้สำหรับบงบอกว่าเป็นประเภทอะไร")]
        public string configHCode { get; set; }
    }

    public class GetSummaryCompanyCaStatusRequest
    {
        [Description("Code Master Data ใช้สำหรับบงบอกว่าเป็นประเภทอะไร")]
        public string configHCode { get; set; }

        GetSummaryCompanyCaStatusRequest()
        {
            configHCode = "STATUS_MAIN_CODE";
        }
    }

    public class GeDepartmentListRequest
    {
        [Description("Code Master Data ใช้สำหรับบงบอกว่าเป็นประเภทอะไร")]
        public string configHCode { get; set; }
    }

    public class GetEstimationTimeListRequest
    {
    }

    public class GetPriorityListRequest
    {

    }

    public class GetTaskGroupListRequest
    {

    }

    public class GetMasterTaskStatusListRequest
    {

    }

    public class GetOrgPositionListRequest
    {
        public List<string> deptCodeList { get; set; }
    }

    public class GetMasterConfigListRequest
    {
        public string configHCode { get; set; }
    }

    





}




