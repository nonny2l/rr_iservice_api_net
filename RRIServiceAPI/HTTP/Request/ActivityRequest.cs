﻿using RRPlatFormAPI.HTTP.Request.Customer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Request
{
    public class ActivityRequest : HttpCommonsModel
    {
       
        [Required]
        [Description("รหัสกิจกรรม running")]
        public int activityId { get; set; }
        [Required]
        [Description("สถานะของกิจกรรม")]
        public string activityStatus { get; set; }
        [Description("วันที่ลงกิจกรรม")]
        public DateTime activityDate { get; set; }
        [Description("รหัสประเภทกิจกรรม")]
        public int activityTypeId { get; set; }
        [Description("วันที่ยกเลิก")]
        public DateTime? cancelDate { get; set; }
        [Description("รหัสผู้ใช้ไฟ")]
        public int caId { get; set; }
        [Description("รหัสบันทึกการโทรหลัก")]
        public int? refCallHId { get; set; }
        [Description("รหัส Vendor")]
        public int? refVendorId { get; set; }
        [Description("หมายเหตุ")]
        public string remark { get; set; }
    }

    public class SaveActivityRequest
    {
        [Description("อ็อบเจ็คบันทึกกิจกรรม")]
        public ActivityRequest activity { get; set; }
        [Description("อ็อบเจ็คสถานะของ Company Ca")]
        public UpdateStatusCompanyCaRequest dataCompanyCaStatus { get; set; }
    }

    public class GetActivityRequest
    {
        [Description("รหัสกิจกรรม running")]
        public int? activityId { get; set; }
        [Description("รหัสผู้ใช้ไฟ")]
        public int? caId { get; set; }
        [Description("รหัสบันทึกการโทรหลัก")]
        public int? refCallHId { get; set; }
        [Description("รหัสประเภทกิจกรรม")]
        public int? activityTypeId { get; set; }
    }
}