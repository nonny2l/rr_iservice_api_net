﻿using RRPlatFormAPI.Controllers.ExternalApiCaller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Request
{

    public class UserInput 
    {
    }


    public class PermittionListRequestInput 
    {
        public int permiition_type { get; set; }

    }


    public class PermittionBottonPerPageRequestInput
    {
        public string url { get; set; }
    }

    public class GetMenuAllRequest 
    {
        public string userid { get; set; }
        public string usertypeId { get; set; }
    }

    public class SaveMtUserTypeRequest 
    {
        public string userTypeId { get; set; }
        public string userTypeName { get; set; }
        public string isActive { get; set; }
        public List<SaveMtUserTypeDepartment> deptNoList { get; set; }

    }

    public class SaveMtUserTypeDepartment
    {
        public string deptNo { get; set; }
        public string isActive { get; set; }

    }

    public class SetPermissionMenuRequest 
    {
        public string userid { get; set; }
        public string usertypeId { get; set; }
        public List<SetMenuActiveData> menuList { get; set; }
    }

    public class SetMenuActiveData
    {
        public int menuId { get; set; }
        public string active { get; set; }
    }



}