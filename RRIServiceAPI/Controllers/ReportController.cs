﻿using RRApiFramework.Controller;
using RRPlatFormAPI.BussinessLogic;
using RRPlatFormAPI.HTTP.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace RRPlatFormAPI.Controllers
{
    public class ReportController : AbApiController
    {
        public override string ModuleName
        {
            get
            {
                return "Report";
            }
        }

        [Route("api/reportSummaryWorktime")] //
        [HttpPost]
        public async Task<IHttpActionResult> ReportSummaryWorktime([FromBody] ReportSummaryWorktimeRequest model)
        {
            ReportBL reportBl = new ReportBL();
            reportBl.userAccess = this.UserAccessApi;
            this.MethodName = "getMtProvinceList";
            var responseData = await reportBl.ReportSummaryWorktime(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);

        }
    }
}