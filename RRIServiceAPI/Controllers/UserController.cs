﻿using RRPlatFormAPI.BussinessLogic;
using RRPlatFormAPI.HTTP.Request;
using RRApiFramework.Controller;
using RRApiFramework.HTTP.Response;
using System.Web.Http;

namespace RRPlatFormAPI.Controllers
{
    public class UserController : AbApiController
    {
        public override string ModuleName
        {
            get
            {
                return "User";
            }
        }

        [Route("api/login")]
        [HttpPost]
        public IHttpActionResult Login([FromBody]LoginRequest model)
        {
            
            this.MethodName = "login";
            UserBL userBL = new UserBL();
            ActionResultStatus responseData = userBL.Login(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/logout")]
        [HttpPost]
        public IHttpActionResult LogOut()
        {
            this.MethodName = "logout";
            UserBL userBL = new UserBL();
            ActionResultStatus responseData = userBL.LogOut(this.UserAccessApi);
            return this.SendResponse(responseData);
        }

        [Authorize]
        [Route("api/getUserProfile")]
        [HttpPost]
        public IHttpActionResult getUserProfile()
        {
            this.MethodName = "getUserProfile";
            UserBL userBL = new UserBL();
            ActionResultStatus responseData = userBL.GetUserProfile(this.UserAccessApi);
            return this.SendResponse(responseData);
        }

        [Route("api/savePassword")]
        [HttpPost]
        public IHttpActionResult SavePassword([FromBody]SavePasswordRequest model)
        {

            this.MethodName = "savePassword";
            UserBL userBL = new UserBL();
            userBL.userAccess = this.UserAccessApi;
            ActionResultStatus responseData = userBL.SavePassword(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }
    }
}
