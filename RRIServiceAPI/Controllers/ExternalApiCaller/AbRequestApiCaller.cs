﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RRApiFramework;
using System.IO;
using System.Net;
using System.Text;

namespace RRPlatFormAPI.Controllers.ExternalApiCaller
{
    public abstract class AbRequestApiCaller<T>
    {
        private string endpointRefer;
        private string host;
        private string contentType;
        private long contentLength;
        private string connection;
        private string accept;
        private string method;
        private string userAgent;
        private AuthorizationApiCaller authorizationApiCaller;
        private object datasInput;
        private string methodName;
        private bool enableAuthorization;



        public abstract string RequestChannel { get; }

        public string MethodName
        {
            get { return this.methodName; }
            set { this.methodName = value; }
        }
        public string EndpointRefer
        {
            get { return this.endpointRefer; }
            set { this.endpointRefer = value; }
        }
        public string Host
        {
            get { return this.host; }
            set { this.host = value; }
        }

        public string Connection
        {
            get { return this.connection; }
            set { this.connection = value; }
        }
        public string ContentType
        {
            get { return this.contentType; }
            set { this.contentType = value; }
        }
        public long ContentLength
        {
            get { return this.contentLength; }
            set { this.contentLength = value; }
        }

        public string Accept
        {
            get { return this.accept; }
            set { this.accept = value; }
        }
        public string Method
        {
            get { return this.method; }
            set { this.method = value; }
        }
        public string UserAgent
        {
            get { return this.userAgent; }
            set { this.userAgent = value; }
        }
        public bool EnableAuthorization
        {
            get { return this.enableAuthorization; }
            set { this.enableAuthorization = value; }
        }
        public AuthorizationApiCaller Authorization
        {
            get { return this.authorizationApiCaller; }
            set { this.authorizationApiCaller = value; }
        }

        public object DataInput
        {
            get { return this.datasInput; }
            set { this.datasInput = value; }
        }

        public virtual T SendRequest()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(this.endpointRefer);

            if (!string.IsNullOrEmpty(this.host))
                httpWebRequest.Host = this.host;

            if (!string.IsNullOrEmpty(this.accept))
                httpWebRequest.Accept = this.accept;

            if (!string.IsNullOrEmpty(this.contentType))
                httpWebRequest.ContentType = this.contentType;

            if (!string.IsNullOrEmpty(this.method))
                httpWebRequest.Method = this.method;

            if (!string.IsNullOrEmpty(this.userAgent))
                httpWebRequest.UserAgent = this.userAgent;


            if (this.enableAuthorization)
                httpWebRequest.Headers.Add(authorizationApiCaller.authorityName + " " + authorizationApiCaller.authorityValue);

            try
            {
              
                byte[] requestBytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(this.datasInput));
                httpWebRequest.ContentLength = requestBytes.Length;

                using (Stream requestStream  = httpWebRequest.GetRequestStream())
                {
                   
                    requestStream.Write(requestBytes, 0, requestBytes.Length);
                    requestStream.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {

                    var response = JsonConvert.DeserializeObject<T>(streamReader.ReadToEnd());
                    SaveLog.SaveLogObject(response, JObject.Parse(JsonConvert.SerializeObject(httpWebRequest)), "", methodName);//save log output            }
                    return response;
                }
            }
            catch (WebException ex)
            {
                SaveLog.SaveLogObject(ex, JObject.Parse(JsonConvert.SerializeObject(httpWebRequest)), ex.Message.ToString(), methodName);//save log output            }
                var response = JsonConvert.DeserializeObject<T>(ex.ToString());
                return response;
                
            }


        }
    }
    public class AuthorizationApiCaller
    {
        public string authorityName { get; set; }
        public string authorityValue { get; set; }
    }
}