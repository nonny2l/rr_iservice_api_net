﻿using Newtonsoft.Json;
using RRApiFramework;
using RRApiFramework.Controller;
using RRApiFramework.HTTP.Response;
using RRPlatFormAPI.BussinessLogic;
using RRPlatFormAPI.HTTP.Request;
using RRPlatFormAPI.HTTP.Request.Customer;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

namespace RRPlatFormAPI.Controllers
{
    public class TarkController : AbApiController
    {
        public override string ModuleName
        {
            get
            {
                return "Tark";
            }
        }

        [Route("api/getTeamList")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetTeamList([FromBody]GetTeamListRequest model)
        {
            TarkBL tarkBL = new TarkBL();
            tarkBL.userAccess = this.UserAccessApi;
            this.MethodName = "getTeamList";
            var responseData = await tarkBL.GetTeamList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/acceptJobTask")] //
        [HttpPost]
        public async Task<IHttpActionResult> AcceptJobTask([FromBody]AcceptJobTaskRequest model)
        {
            TarkBL tarkBL = new TarkBL();
            tarkBL.userAccess = this.UserAccessApi;
            this.MethodName = "acceptJobTask";
            var responseData = await tarkBL.AcceptJobTask(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/saveTaskList")] //
        [HttpPost]
        public async Task<IHttpActionResult> SaveTask([FromBody] List<SaveTaskRequest> model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "saveTaskList";
            var responseData = await customerBl.SaveTaskList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }
    }
}
