﻿using RRPlatFormAPI.BussinessLogic;
using RRPlatFormAPI.HTTP.Request;
using RRApiFramework.Controller;
using RRApiFramework.HTTP.Response;
using System.Web.Http;
using RRPlatFormAPI.HTTP.Request.Master;
using System.Threading.Tasks;

namespace RRPlatFormAPI.Controllers
{
    public class NotifyController : AbApiController
    {
        public override string ModuleName
        {
            get
            {
                return "Notify";
            }
        }

        [Route("api/getNotifyList")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetNotifyList([FromBody]GetNotifyListRequest model)
        {
            NotifyBL notifyBL = new NotifyBL();
            notifyBL.userAccess = this.UserAccessApi;
            this.MethodName = "getNotifyList";
            var responseData = await notifyBL.GetNotifyList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getReadNotifyList")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetReadNotifyList([FromBody]GetReadNotifyListRequest model)
        {
            NotifyBL notifyBL = new NotifyBL();
            notifyBL.userAccess = this.UserAccessApi;
            this.MethodName = "getReadNotifyList";
            var responseData = await notifyBL.GetReadNotifyList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/readNotify")] //
        [HttpPost]
        public async Task<IHttpActionResult> ReadNotify([FromBody]ReadNotifyRequest model)
        {
            NotifyBL notifyBL = new NotifyBL();
            notifyBL.userAccess = this.UserAccessApi;
            this.MethodName = "readNotify";
            var responseData = await notifyBL.ReadNotify(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/readNotifyList")] //
        [HttpPost]
        public async Task<IHttpActionResult> ReadNotifyList([FromBody]ReadNotifyListRequest model)
        {
            NotifyBL notifyBL = new NotifyBL();
            notifyBL.userAccess = this.UserAccessApi;
            this.MethodName = "readNotify";
            var responseData = await notifyBL.ReadNotifyList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }
    }
}
