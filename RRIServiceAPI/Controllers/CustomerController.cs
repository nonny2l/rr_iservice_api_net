﻿using RRPlatFormAPI.BussinessLogic;
using RRPlatFormAPI.HTTP.Request;
using RRApiFramework.Controller;
using RRApiFramework.HTTP.Response;
using System.Web.Http;
using System.Security.Claims;
using RRApiFramework;
using Newtonsoft.Json;
using System.Threading.Tasks;
using RRPlatFormAPI.HTTP.Request.Customer;

namespace RRPlatFormAPI.Controllers
{
    public class CustomerController : AbApiController
    {
        public override string ModuleName
        {
            get
            {
                return "Customer";
            }
        }

        [Route("api/getClientList")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetClientList([FromBody] GetClientListRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "getClientList";
            var responseData = await customerBl.GetClientList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getProjectList")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetProjectList([FromBody] GetProjectListRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "getProjectList";
            var responseData = await customerBl.GetProjectList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getServiceList")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetServiceList([FromBody] GetServiceListRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "getServiceList";
            var responseData = await customerBl.GetServiceList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getSubServiceList")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetSubServiceList([FromBody] GetSubServiceListRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "getSubServiceList";
            var responseData = await customerBl.GetSubServiceList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getStatusJobList")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetStatusJobList([FromBody] GetStatusJobListRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "getStatusJobList";
            var responseData = await customerBl.GetStatusJobList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getEmployeeList")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetEmployeeList([FromBody] GetEmployeeListRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "getEmployeeList";
            var responseData = await customerBl.GetEmployeeList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getEmployeeByTeamList")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetEmployeeByTeamList([FromBody] GetEmployeeByTeamListRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "getEmployeeByTeamList";
            var responseData = await customerBl.GetEmployeeByTeamList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        

        [Route("api/getEmployeeAllList")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetEmployeeAllList([FromBody] GetEmployeeAllListRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "getEmployeeAllList";
            var responseData = await customerBl.GetEmployeeAllList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getContactList")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetContactList([FromBody]GetContactListRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "getContactList";
            var responseData = await customerBl.GetContactList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/saveContact")] //
        [HttpPost]
        public async Task<IHttpActionResult> SaveContact([FromBody]SaveContactRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "saveContact";
            var responseData = await customerBl.SaveContact(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getJobList")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetJobList([FromBody]GetJobListRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "getJobList";
            var responseData = await customerBl.GetJobList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getJobDetail")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetJobDetail([FromBody]GetJobDetailRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "getJobDetail";
            var responseData = await customerBl.GetJobDetail(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        

        [Route("api/saveJob")] //
        [HttpPost]
        public async Task<IHttpActionResult> SaveJob([FromBody]SaveJobRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "saveJob";
            var responseData = await customerBl.SaveJob(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/saveJobTark")] //
        [HttpPost]
        public async Task<IHttpActionResult> SaveJobTark([FromBody]SaveJobTarkRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "saveJobTark";
            var responseData = await customerBl.SaveJobTask(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        

        [Route("api/getDataTimeSheet")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetDataTimeSheet([FromBody]GetDataTimeSheetRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "getDataTimeSheet";
            var responseData = await customerBl.GetDataTimeSheet(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }


        [Route("api/getTaskCerrentTimeSheet")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetTaskCerrentTimeSheet([FromBody]GetTaskCerrentTimeSheetRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "getTaskCerrentTimeSheet";
            var responseData = await customerBl.GetTaskCerrentTimeSheet(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/saveTimeSheet")] //
        [HttpPost]
        public async Task<IHttpActionResult> SaveTimeSheet([FromBody]SaveTimeSheetRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "saveTimeSheet";
            var responseData = await customerBl.SaveTimeSheet(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getTimeSheetHistoryByTaskCode")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetTimeSheetHistoryByTaskCode([FromBody]GetTimeSheetHistoryByTaskCode model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "getTimeSheetHistoryByTaskCode";
            var responseData = await customerBl.GetTimeSheetHistoryByTaskCode(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getTimeSheetActiveByUserId")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetTimeSheetActiveByUserId([FromBody]GetTimeSheetActiveByUserIdRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "getTimeSheetActiveByUserId";
            var responseData = await customerBl.GetTimeSheetActiveByUserId();
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/saveTask")] //
        [HttpPost]
        public async Task<IHttpActionResult> SaveTask([FromBody]SaveTaskRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "saveTask";
            var responseData = await customerBl.SaveTask(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getTaskDetails")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetTaskDetails([FromBody]GetTaskDetailsRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "getTaskDetails";
            var responseData = await customerBl.GetTaskDetails(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getSummaryTaskList")] //
        [HttpPost]
        public async Task<IHttpActionResult> GetSummaryTaskList([FromBody]GetSummaryTaskListRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "getSummaryTaskList";
            var responseData = await customerBl.GetSummaryTaskList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getClientCareList")] 
        [HttpPost]
        public async Task<IHttpActionResult> GetClientCareList([FromBody]GetClientCareListRequest model)
        {
            CustomerBL customerBl = new CustomerBL();
            customerBl.userAccess = this.UserAccessApi;
            this.MethodName = "getClientCareList";
            var responseData = await customerBl.GetClientCareList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }
    }
}
