﻿using RRPlatFormAPI.BussinessLogic;
using RRPlatFormAPI.HTTP.Request;
using RRApiFramework.Controller;
using RRApiFramework.HTTP.Response;
using System.Web.Http;
using System.Web;

namespace RRPlatFormAPI.Controllers
{
    public class UploadController : AbApiController
    {
        public override string ModuleName
        {
            get
            {
                return "Upload";
            }
        }

        [Route("api/uploadFile")]
        [HttpPost]
        public IHttpActionResult UploadProfileImage()
        {
            this.MethodName = "uploadFile";
            var httpRequest = HttpContext.Current.Request;
            this.EnableResponseApiSpect = true;
            UploadBL uploadBL = new UploadBL();
            uploadBL.userAccess = this.UserAccessApi;
            this.InputRequest(httpRequest.Files);
            return this.SendResponse(uploadBL.UploadProfileImage(httpRequest.Files));
        }
    }
}
