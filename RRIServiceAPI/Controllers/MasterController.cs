﻿using RRPlatFormAPI.BussinessLogic;
using RRPlatFormAPI.HTTP.Request;
using RRApiFramework.Controller;
using RRApiFramework.HTTP.Response;
using System.Web.Http;
using RRPlatFormAPI.HTTP.Request.Master;

namespace RRPlatFormAPI.Controllers
{
    public class MasterController : AbApiController
    {
        public override string ModuleName
        {
            get
            {
                return "Master";
            }
        }

        [Route("api/getMasterList")] //
        [HttpPost]
        public IHttpActionResult GetMasterList([FromBody]GetMasterListRequest model)
        {
            this.MethodName = "getMasterList";
            MasterBL masterBL = new MasterBL();
            this.InputRequest(model);
            return this.SendResponse(masterBL.GetMasterList(model));
        }

        [Route("api/getMtProvinceList")] //
        [HttpPost]
        public IHttpActionResult getMtProvinceList([FromBody] ProvinceRequest model)
        {
            MasterBL masterBl = new MasterBL();
            masterBl.userAccess = this.UserAccessApi;
            this.MethodName = "getMtProvinceList";
            var responseData = masterBl.GetMstProvinceList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);

        }

        [Route("api/getMtProvinceGroupRegionList")] //
        [HttpPost]
        public IHttpActionResult getMtProvinceGroupRegionList([FromBody] ProvinceRequest model)
        {
            MasterBL masterBl = new MasterBL();
            masterBl.userAccess = this.UserAccessApi;
            this.MethodName = "getMtProvinceGroupRegionList";
            var responseData = masterBl.GetMstProvinceGroupRegionList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getMtDistrictList")] //
        [HttpPost]
        public IHttpActionResult getMtDistrictList([FromBody] DistrictRequest model)
        {
            MasterBL masterBl = new MasterBL();
            masterBl.userAccess = this.UserAccessApi;
            this.MethodName = "getMtDistrictList";
            var responseData = masterBl.GetMstDistrictList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getMtSubDistrictList")] //
        [HttpPost]
        public IHttpActionResult getMtSubDistrictList([FromBody] SubDistrictRequest model)
        {
            MasterBL masterBl = new MasterBL();
            masterBl.userAccess = this.UserAccessApi;
            this.MethodName = "getMtSubDistrictList";
            var responseData = masterBl.GetMstSubDistrictList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getDepartmentList")] //
        [HttpPost]
        public IHttpActionResult GeDepartmentList([FromBody] GeDepartmentListRequest model)
        {
            MasterBL masterBl = new MasterBL();
            masterBl.userAccess = this.UserAccessApi;
            this.MethodName = "getDepartmentList";
            var responseData = masterBl.GeDepartmentList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getEstimationTimeList")] //
        [HttpPost]
        public IHttpActionResult GetEstimationTimeList([FromBody] GetEstimationTimeListRequest model)
        {
            MasterBL masterBl = new MasterBL();
            masterBl.userAccess = this.UserAccessApi;
            this.MethodName = "getEstimationTimeList";
            var responseData = masterBl.GetEstimationTimeList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getPriorityList")] //
        [HttpPost]
        public IHttpActionResult GetPriorityList([FromBody] GetPriorityListRequest model)
        {
            MasterBL masterBl = new MasterBL();
            masterBl.userAccess = this.UserAccessApi;
            this.MethodName = "getPriorityList";
            var responseData = masterBl.GetPriorityList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getTaskGroupList")] //
        [HttpPost]
        public IHttpActionResult GetTaskGroupList([FromBody] GetTaskGroupListRequest model)
        {
            MasterBL masterBl = new MasterBL();
            masterBl.userAccess = this.UserAccessApi;
            this.MethodName = "getTaskGroupList";
            var responseData = masterBl.GetTaskGroupList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getMasterTaskStatusList")]
        [HttpPost]
        public IHttpActionResult GetMasterTaskStatusList([FromBody]GetMasterTaskStatusListRequest model)
        {
            MasterBL masterBl = new MasterBL();
            masterBl.userAccess = this.UserAccessApi;
            this.MethodName = "getMasterTaskStatusList";
            var responseData = masterBl.GetMasterTaskStatusList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getOrgPositionList")]
        [HttpPost]
        public IHttpActionResult GetOrgPositionList([FromBody]GetOrgPositionListRequest model)
        {
            MasterBL masterBl = new MasterBL();
            masterBl.userAccess = this.UserAccessApi;
            this.MethodName = "getOrgPositionList";
            var responseData = masterBl.GetOrgPositionList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getMasterConfigList")]
        [HttpPost]
        public IHttpActionResult GetMasterConfigList([FromBody]GetMasterConfigListRequest model)
        {
            MasterBL masterBl = new MasterBL();
            masterBl.userAccess = this.UserAccessApi;
            this.MethodName = "getMasterConfigList";
            var responseData = masterBl.GetMasterConfigList(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        


    }
}
