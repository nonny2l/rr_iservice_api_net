﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.HTTP.Request
{
    public class MasterPaginationRequest
    {
        public MasterPaginationRequest()
        {
            pagination = new PaginationRequest();
            sort = new SortableRequest();
        }

        public PaginationRequest pagination { get; set; }
        public SortableRequest sort { get; set; }

    }
    public class SortableRequest
    {
        public string column { get; set; }
        public string action { get; set; }
    }
    public class PaginationRequest
    {
        public int currentPage { get; set; }
        public int limitPerPage { get; set; }

        public PaginationRequest()
        {
            currentPage = 0;
            limitPerPage = 10;
        }
    }
}