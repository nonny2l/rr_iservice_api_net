﻿using RRApiFramework.HTTP.Response;
using RRApiFramework.Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ModelBinding;

namespace RRPlatFormAPI.Utility
{
    public static class ErrorMessage
    {
        public static ActionResultStatus GetRequestModelError(ModelStateDictionary modelState)
        {
            List<object> listErr = new List<object>();
            foreach (ModelState vals in modelState.Values)
            {
                foreach (var item in vals.Errors)
                {
                    listErr.Add(item.Exception);
                }
            }
            ActionResultStatus actionResultStatus = new ActionResultStatus
            {
                success = false,
                code = (int)StatusCode.Exception,
                data = modelState,
                message = $"ทำรายาการไม่สำเร็จ",
                description = "Data model Error"
            };

            return actionResultStatus;
        }
    }
}