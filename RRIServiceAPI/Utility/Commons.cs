﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Utility
{
    public static class Commons
    {
        public static CultureInfo cultureInfo = new CultureInfo(GetAppSettingValue("cultureInfo"));
        public static string formatDate = GetAppSettingValue("RESPONSE_DATE");
        public static string formatTime = GetAppSettingValue("RESPONSE_TIME");
        public static string formatDateTime = GetAppSettingValue("RESPONSE_DATETIME");
        public static string strDecimal = GetAppSettingValue("RESPONSE_DECIMAL");
        public static char[] trimChars = { '*', '@', ' ', '\t' };
        public static string physicalServerPathFile = GetAppSettingValue("PhysicalServerPathFile");
        public static string physicalServerPath = GetAppSettingValue("PhysicalServerPath");
        public static string physicalServerPrefixRoute = GetAppSettingValue("PhysicalServerPrefixRoute");

        public static string GetAppSettingValue(string key)
        {
            try
            {
                NameValueCollection appSettings = ConfigurationManager.AppSettings;
                string value = appSettings.GetValues(key).FirstOrDefault();
                return value;
            }
            catch (ConfigurationErrorsException e)
            {
                return e.ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
    }
}