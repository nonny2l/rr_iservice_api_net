﻿using RRApiFramework.HTTP.Response;
using RRApiFramework.Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRPlatFormAPI.Utility
{
    public class MessageUtility
    {
        public static ActionResultStatus ThrowErrorMessage(object model, string msg, string desc = "")
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus
            {
                success = false,
                code = (int)StatusCode.NotFoundData,
                message = msg,
                description = desc,
                otherData = new
                {
                    input = model
                }
            };
            return actionResultStatus;
        }
    }
}